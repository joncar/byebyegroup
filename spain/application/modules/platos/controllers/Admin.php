<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function categorias_platos(){
            $crud = $this->crud_function('',''); 
            $crud->set_subject('Categoria de platos');
            $crud->set_field_upload('fondo','img/platos');
            $crud->columns('fondo','nombre','Platos añadidos');
            $crud->callback_column('Platos añadidos',function($val,$row){
                return '<a href="'.base_url('platos/admin/platos/'.$row->id).'">'.get_instance()->db->get_where('platos',array('categorias_platos_id'=>$row->id))->num_rows().' <i class="fa fa-plus"></i></a>';
            });
            $output = $crud->render();
            $output->title = 'Categoria de platos';
            $this->loadView($output);
        }
        
        function platos($x = ''){
            $crud = $this->crud_function('','');
            $crud->where('categorias_platos_id',$x);
            $crud->field_type('categorias_platos_id','hidden',$x);
            $crud->set_field_upload('foto','img/platos')
                 ->set_field_upload('foto_especialidad','img/platos')
                 ->set_field_upload('portada','img/platos');
            $crud->display_as('foto_especialidad','Foto de especialidad (482x400)')
                 ->display_as('foto','Foto de carta (120x120)');
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function menu_dia($x = ''){
            $crud = $this->crud_function('','');
            if($crud->getParameters()=='edit'){
                $crud->required_fields('descripcion');
            }
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function ajustes($x = ''){
            $crud = $this->crud_function('','');
            $crud->required_fields('precio_menu_dia');
            $output = $crud->render();
            $this->loadView($output);
        }
    }
?>
