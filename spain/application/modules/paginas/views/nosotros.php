<div id="wrapper-content">
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <section class="page-banner about-us-page">
            <div class="patterndestino"></div>
            <div class="container">
                <div class="page-title-wrapper">
                    <div class="page-title-content">
                        <ol class="breadcrumb">
                            <li><a href="index.html" class="link home">Home</a></li>
                            <li class="active"><a href="#" class="link">nosotros</a></li>
                        </ol>
                        <div class="clearfix"></div>
                        <h2 class="captions">¿Quiénes somos?</h2></div>
                </div>
            </div>
        </section>
        <section class="about-us layout-2 padding-top padding-bottom about-us-4">
            <div class="container">
                <div class="row">
                    <div class="wrapper-contact-style">
                        <div class="col-lg-8 col-md-8">
                            <h3 class="title-style-2">¿Quién es Bye Bye Group?</h3>
                            <div class="about-us-wrapper">
                                <p class="text">Un placer presentarnos, somos la Agencia de Viajes elegida por excelencia entre los Centros Educativos y Asociaciones de Madres y Padres de Alumnos de su localidad.
                                    Estamos convencidos que la educación que reciben los estudiantes en las aulas y hogares, es complementaria a la formación didáctica y pedagógica en la que se orientan nuestros circuitos y programas escolares.
                                    Por ello, apostamos por destinos que combinan una parte cultural, con visitas a los lugares más emblemáticos de cada región, y una parte lúdica, con divertidas actividades de aventura que encantarán a los estudiantes.
                                    Aportamos soluciones adecuadas a las propuestas recibidas por los centros, coordinándonos con el equipo docente, a efectos de garantizar la satisfacción de un aprendizaje completo durante el transcurso de las actividades y visitas planificadas.

                                    A continuación os presentamos nuestra oferta de viajes de fin de curso nacionales e internacionales dónde podréis ver los itinerarios de cada destino y los servicios que están incluidos.
                                    Si preferís hacer vuestro viaje a medida también tenéis una amplia referencia de actividades opcionales en cada viaje.

                                    Si lo deseáis, como director de la agencia, estaré encantado de poder hacer una visita a vuestro centro para explicaros, asesoraros y ampliar la información que necesitéis sin ningún compromiso.


                                </p>
                                <div class="group-list">
                                    <ul class="list-unstyled about-us-list">
                                        <li>
                                            <p class="text">Descubres nuevos destinos</p>
                                        </li>
                                        <li>
                                            <p class="text">Destinos de proximidad
                                                </p>
                                        </li>
                                        <li>
                                            <p class="text">Viaja por mar o aire</p>
                                        </li>
                                        <li>
                                            <p class="text">Combinamos ocio y cultura</p>
                                        </li>
                                    </ul>
                                    <ul class="list-unstyled about-us-list">
                                        <li>
                                            <p class="text">  Aprende idiomas</p>
                                        </li>
                                        <li>
                                            <p class="text">Accesibles para todos</p>
                                        </li>
                                        <li>
                                            <p class="text">Descubre sitios nuevos</p>
                                        </li>
                                        <li>
                                            <p class="text"> Nos entenderemos seguro!</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div data-wow-delay="0.4s" class="about-us-image wow zoomInRight"><img src="<?= base_url() ?>img/homepage/about-us-4.png" alt="" class="img-responsive"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="videos padding-top padding-bottom page-our-values" style=" padding-bottom: 87px; height: 920px;">
            <div class="container">
                <h3 class="title-style-2" style="color: white;">¿Por qué viajar con nosotros?</h3>
                <div class="row">
                    <div class="our-wrapper">
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/1.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">GARANTIA AAVV </p>
                                    <p class="text">Por la reconocida garantía y confianza de nuestra Agencia, porque somos especialistas en colectivos de estudiantes. Más de 25.000 alumnos, profesores, padres y madres ya han viajado con nosotros.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/2.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">AGENTE PERSONALIZADO</p>
                                    <p class="text">Asesoraremos al profesorado sobre todas las gestiones administrativas que conciernen la actividad seleccionada en relación con los medios de transporte, alojamiento, horarios, visitas, seguros, presupuestos, etc.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/3.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">TRANSPORTES </p>
                                    <p class="text">En nuestros viajes siempre viajaremos en autocar, cumpliendo siempre las normativas vigentes sobre las condiciones de seguridad en relación a transportes escolares y de menores RD 443/2001 y RD 561/2006.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/4.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">ALOJAMIENTOS</p>
                                    <p class="text">Para cada destino tenemos diferentes tipos de alojamientos, ya sean céntricos, o en la periferia, disponemos de esta diversidad según la necesidad de cada grupo, para asegurar un alojamiento confortable.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="our-wrapper">
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/10.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">SOPORTE TELEFÓNICO</p>
                                    <p class="text">El centro y el profesorado acompañante dispondrán de un listado telefónico durante el transcurso del viaje para realizar cualquier consulta o sugerencia de los servicios contratados.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/6.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title"> PROMOCIONES </p>
                                    <p class="text"> • Alumnado: Ponemos a vuestra disposición un talonario de forma gratuita para cada alumno, con el que podrán obtener hasta 200€. El importe de los boletines se destinará íntegramente a su viaje.
                                        En el caso de que algún alumno vendiera todo el talonario, podría solicitarnos otro para seguir recaudando dinero para su viaje.
                                        Además obsequiaremos a cada participante con una bolsa/mochila personalizada.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/9.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">SEGUROS</p>
                                    <p class="text"> Trabajamos con las compañías líderes del mercado en colectivos de jóvenes y adolescentes. Viajar seguro es importante y da tranquilidad.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/8.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">RESERVA ANTICIPADA Y TRATO DIRECTO</p>
                                    <p class="text">Anticípate y reserva tu viaje antes de diciembre y consigue todas nuestras promociones. Tenemos un trato directo con todos los alojamientos y servicios que componen los viajes, hecho que nos da mucho margen y rapidez para poder configurar el viaje a vuestra medida y sin intermediarios.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                        <!--<div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/11.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">TRACTO DIRECTO</p>
                                    <p class="text"></p>
                                </div>
                            </div>
                        </div>-->
                    </div>
                </div>            
        </section>
    <section class="our-expert padding-top padding-bottom-50" style="padding-bottom: 160px; background:url(<?= base_url() ?>img/background/bg-section-about_1.jpg)">
        <div class="container">
        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1466761151287"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="slz-shortcode block-title-141176022559947adb20239 ">
                        <h3 class="title-style-2">¿ComO NOS ORGANIZAMOS?</h3></div>
                    <div id="accordion-slzexploore_faq-94869926059947adb21205" class="slz-shortcode  wrapper-accordion panel-group ">
                        <div class="panel">
                            <div class="panel-heading">
                                <p class="text">


                                    Organizar un viaje de fin de curso puede resultar un trámite complicado y costoso. Por ello, nuestra agencia está a su disposición, para facilitar todas las gestiones. Nuestro agente de viajes le asesorará durante todo el proceso.</p>       <br>
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" href="#slzexploore_faq-94869926059947adb21205-collapse-1" aria-expanded="false" class="accordion-toggle collapsed">CALENDARIO DE PREPARACIÓN
                                    </a>
                                </h5>
                            </div>
                            <div id="slzexploore_faq-94869926059947adb21205-collapse-1" aria-expanded="false" class="panel-collapse collapse" role="tabpanel" style="height: 0px;">
                                <div class="panel-body"><p>el calendario de preparación de su viaje es el origen de una correcta planificación. Por nuestra experiencia, proponemos notificar el interés de hacer el viaje con la máxima antelación posible para trabajar con un margen de tiempo adecuado.
                                <BR>Llámanos y le asignaremos un gestor de proyectos para que establezca contacto personal con el equipo de profesorado para asesorarle y cerrar un presupuesto según sus necesidades. </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" href="#slzexploore_faq-94869926059947adb21205-collapse-2" aria-expanded="false" class="accordion-toggle collapsed">PRESUPUESTO Y CONTRATO</a>
                                </h5>
                            </div>
                            <div id="slzexploore_faq-94869926059947adb21205-collapse-2" aria-expanded="false" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body"><P> Cuando les visitamos en su centro, les llevaremos los presupuestos de los viajes que estén valorando acompañado de la siguiente documentación:<BR>
                                 • Itinerario detallado <br>
                                 • Información turística y actividades <br>
                                 • Contrato genérico de viaje combinado </ p>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" href="#slzexploore_faq-94869926059947adb21205-collapse-3" aria-expanded="false" class="accordion-toggle collapsed">CONFIRMACIÓN DE NÚMERO DE ALUMNOS Y PRIMER PAGO</a>
                                </h5>
                            </div>
                            <div id="slzexploore_faq-94869926059947adb21205-collapse-3" aria-expanded="false" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body"><p>Una vez seleccionado el destino de su viaje, es importante pedir una paga y señal para asegurarse del número de alumnos que vendrán al viaje.
                                                                                 <br> Si desea dar un precio final del viaje a los alumnos teniendo como referencia el presupuesto, que sea aproximado, nunca cerrado, porque este puede variar en función del número de alumnos que finalmente participen.
                                                                                 <br> Recomendamos que fije los siguientes importes de paga y señal así serán los mismos que nosotros necesitaremos para hacer el bloqueo de las plazas de los estudiantes por el viaje:<br><br>
                                        <b>Viajes nacionales e internacionales → Bloqueo gratuito del grupo, 20% al realizar la reserva y el resto 30 días antes.</b><br><br>
                                        En caso de utilizar avión, se debe pedir aproximadamente un anticipo de unos 150 € por alumno para poder comprar los billetes. Antes de comprarlos, nuestros agentes siempre confirman los precios de los billetes y si han subido y no le interesa o desea cambiar de viaje le devolvemos su dinero sin ningún inconveniente.
                                                                                 <br> Cuando finaliza este proceso, sabréis exactamente cuántos alumnos harán el viaje y le podrá confirmar a nuestro agente de viaje para poder hacer el pago de garantía de reserva.
                                                                                 <br> 30 días antes de la salida, se deberá realizar el pago final del grupo y enviar el listado de los alumnos y profesores con sus nombres, apellidos y DNI.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" href="#slzexploore_faq-94869926059947adb21205-collapse-4" aria-expanded="false" class="accordion-toggle collapsed">DOCUMENTACIÓN FINAL</a>
                                </h5>
                            </div>
                            <div id="slzexploore_faq-94869926059947adb21205-collapse-4" aria-expanded="false" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body"><p>Antes del inicio del viaje nuestro agente le hará entrega de la documentación importante de todo el viaje, así como el listado telefónico para cualquier imprevisto.
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
</div>

</section>
        <section class="our-expert padding-top padding-bottom-50">
            <div class="container">
                <h3 class="title-style-2">NUESTRA EXPERTA</h3>
                <div class="wrapper-expert">
                    <div class="item content-expert">
                        <a href="#" class="img-expert"><img src="<?= base_url() ?>img/homepage/about-5.jpg" alt="" class="img-responsive img"></a>
                        <div class="caption-expert"><a href="#" class="title">Anabel Mateos</a>
                            <p class="text">Controller administrativa de expansión Espanya</p>
                            <ul class="social list-inline">
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-facebook"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-twitter"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <!--<div class="item content-expert">
                        <a href="#" class="img-expert"><img src="<?/*= base_url() */?>img/homepage/about-6.jpg" alt="" class="img-responsive img"></a>
                        <div class="caption-expert"><a href="#" class="title">Anna Riba</a>
                            <p class="text">Controller administrativa</p>
                            <ul class="social list-inline">
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-facebook"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-twitter"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item content-expert">
                        <a href="#" class="img-expert"><img src="<?/*= base_url() */?>img/homepage/about-7.jpg" alt="" class="img-responsive img"></a>
                        <div class="caption-expert"><a href="#" class="title">Monica Martinez</a>
                            <p class="text">Directora comercial de ventas</p>
                            <ul class="social list-inline">
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-facebook"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-twitter"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item content-expert">
                        <a href="#" class="img-expert"><img src="<?/*= base_url() */?>img/homepage/about-8.jpg" alt="" class="img-responsive img"></a>
                        <div class="caption-expert"><a href="#" class="title">Emma Soler</a>
                            <p class="text">Controller administrativa</p>
                            <ul class="social list-inline">
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-facebook"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-twitter"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item content-expert">
                        <a href="#" class="img-expert"><img src="<?/*= base_url() */?>img/homepage/about-9.jpg" alt="" class="img-responsive img"></a>
                        <div class="caption-expert"><a href="#" class="title">Montse Raja</a>
                            <p class="text">Comercial de ventas</p>
                            <ul class="social list-inline">
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-facebook"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-twitter"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>-->
                   <!-- <div class="item content-expert">
                        <a href="#" class="img-expert"><img src="<?/*= base_url() */?>img/homepage/about-8.jpg" alt="" class="img-responsive img"></a>
                        <div class="caption-expert"><a href="#" class="title">Mark letto</a>
                            <p class="text">Manager Tour Guide</p>
                            <ul class="social list-inline">
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-facebook"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-twitter"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>-->
                </div>
            </div>
        </section>
        <div class="about-tours padding-top padding-bottom">
            <div class="container">
                <div class="wrapper-tours">
                    <div class="content-icon-tours">
                        <div class="content-tours"><i class="icon flaticon-people"></i>
                            <div class="wrapper-thin"><span class="wrapper-icon-thin"><i class="icon-thin fa fa-circle-thin"></i></span>
                                <div class="tours-title">3750</div>
                            </div>
                            <div class="text">Alumnos Felices</div>
                        </div>
                        <div class="content-tours"><i class="icon flaticon-suitcase"></i>
                            <div class="wrapper-thin"><span class="wrapper-icon-thin"><i class="icon-thin fa fa-circle-thin"></i></span>
                                <div class="tours-title">7740</div>
                            </div>
                            <div class="text">Destinos</div>
                        </div>
                        <div class="content-tours"><i class="icon flaticon-two"></i>
                            <div class="wrapper-thin"><span class="wrapper-icon-thin"><i class="icon-thin fa fa-circle-thin"></i></span>
                                <div class="tours-title">850</div>
                            </div>
                            <div class="text">Hoteles</div>
                        </div>
                        <div class="content-tours"><i class="icon flaticon-transport"></i>
                            <div class="wrapper-thin"><span class="wrapper-icon-thin"><i class="icon-thin fa fa-circle-thin"></i></span>
                                <div class="tours-title">140</div>
                            </div>
                            <div class="text">Centros escolares</div>
                        </div>
                        <div class="content-tours"><i class="icon flaticon-drink"></i>
                            <div class="wrapper-thin"><span class="wrapper-icon-thin"><i class="icon-thin fa fa-circle-thin"></i></span>
                                <div class="tours-title">8960</div>
                            </div>
                            <div class="text">Viajes hechos</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="wrapper-open-position padding-top padding-bottom">
            <div class="container">
                <div class="wrapper-position">
                    <h3 class="title-style-2">Prestaciones de servicios <img src="<?= base_url() ?>img/INNOVAC.jpg" alt="" class="img-responsive" style="position: absolute; z-index: 10; width: 168px; left:390px; top: -24px"></h3>

                    <div class="content-position"
                        <div class="row">
                            <div class="col-md-6 col-sm-10 col-xs-10 main-right">
                                <div class="content-open">
                                    <div class="main-position">
                                        <div class="img-position">
                                            <a href="#" class="img-open"><img src="<?= base_url() ?>img/background/bg-team-open.jpg" alt="" class="img-responsive"></a>
                                        </div>
                                        <ul class="list-info list-unstyled">
                                            <li><a href="https://www.facebook.com/pg/byebyegroup/photos/?ref=page_internal" class="link"><i class="icon fa fa-facebook"></i></a></li>
                                            <li><a href="https://twitter.com/byebyegroup" class="link"><i class="icon fa fa-twitter"></i></a></li>
                                            <li><a href="http://stalkture.com/p/byebyegroup/3680527830/" class="link"><i class="icon fa fa-instagram"></i></a></li>
                                         <!--   <li><a class="link"><i class="icon fa fa-linkedin"></i></a></li>
                                            <li><a class="link"><i class="icon fa fa-behance"></i></a></li>-->
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="wrapper-text-excel">
                                        <div class="text-excel"> Observaciones generales</div>
                                        <ul class="list-text list-unstyled">
                                            <li><a class="link-text"></i><span class="text-title">REGÍMENES</span></a>
                                                <p class="text">S.A. Sólo alojamiento – A.D. Alojamiento y desayuno – M.P Media pensión – P.C Pensión completa – S.P Según programa.
</p>
                                            </li>
                                            <li><a class="link-text"></i><span class="text-title">PRECIO</span></a>
                                                <p class="text">El precio del viaje combinado ha sido calculado según los tipos de cambio, tarifas de transporte, costes del carburante y tasas de impuestos aplicables en el momento de la consulta y confirmación de la reserva. Cualquier variación del precio de los citados elementos podrá dar lugar a la revisión del precio final del viaje.</p>
                                            </li>
                                            <li><a class="link-text"></i><span class="text-title">FIANZA</span></a>
                                                <p class="text">Los hoteles podrán exigir una fianza 100%, reembolsable si las instalaciones no han sufrido desperfectos a la salida del grupo. La agencia no asumirá los gastos que se puedan ocasionar por dichos motivos. El importe de la fianza lo designa cada establecimiento.</p>
                                            </li>

                                            <li><a class="link-text"></i><span class="text-title">PAGO Y RESERVA</span></a>
                                                <p class="text">Al realizar la reserva se abonará un 20% del presupuesto final y el resto se abonará 30 días antes de la salida del grupo.</p>
                                            </li>
                                            <li><a class="link-text"></i><span class="text-title">DOCUMENTACIÓN</span></a>
                                                <p class="text">La documentación que es necesaria llevar en los viajes de estudiantes es el DNI en vigor o PASAPORTE en vigor, y recomendable la Tarjeta Sanitaria..</p>
                                            </li>
                                            <li><a class="link-text"></i><span class="text-title">INFORMACIÓN MÉDICA</span></a>
                                                <p class="text">Si el estudiante que va a realizar el viaje fin de curso necesita cuidados de salud especiales, deberán ser notificados con antelación a nuestra empresa para poder gestionarlas con antelación. Deben comunicarlo a profesores y/o padres acompañantes, responsables del alojamiento elegido y coordinadores de actividades en el destino del viaje fin de curso.</p>
                                            </li>
                                            <li><a class="link-text"></i><span class="text-title">AUTOCAR</span></a>
                                                <p class="text">El autobús será nuestro medio de transporte durante nuestro trayecto; debemos por tanto mantenerlo limpio y cuidado.</p>
                                            </li>


                                               <!-- <ul>
                                                    <li><span>Create assets for the Zendesk website</span></li>
                                                    <li><span>Concept ideas around the Zendesk brand</span></li>
                                                    <li><span>Design and e xecute Zendesk online advertising</span></li>
                                                    <li><span>Design presentations and printed materials</span></li>
                                                </ul>-->
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-10 col-xs-10">
                                <div class="group-list group-number">
                                    <ul class="list-unstyled about-us-list">
                                        <li><span class="" style="font-weight: bold">SERVICIOS</span><span class="text-number"  style="font-weight: bold">LÍMITES</span></span></li>
                                        <li><span class="text">Asistencia médica y España</span><span class="text-number">601,01€</span></li>
                                        <li><span class="text">Repatriación o transporte de heridos y/o enfermos</span><span class="text-number">ILIMITADO</span></li>
                                        <li><span class="text">Repatriación de acompañante</span><span class="text-number">INCLUIDO</span></li>
                                        <li><span class="text">Repatriación o transporte de menores</span><span class="text-number">ILIMITADO</span></li>
                                        <li><span class="text">Desplazamiento de un familiar en caso de hospitalización <br>superior a cinco días </span><span class="text-number">ILIMITADO</span></li>
                                        <li><span class="text">Gastos de estancia en el extranjero (máximo de 10 días)</span><span class="text-number">30,05€/dia</span></li>

                                        <li><span class="text">Convalecencia en hotel (máximo de 10 días)</span><span class="text-number">30,05€/dia</span></li>

                                        <li><span class="text">Repatriación o transporte del asegurado fallecido</span><span class="text-number">ILIMITADO</span></li>
                                        <li><span class="text">Búsqueda, localización y envío de equipaje extraviado</span><span class="text-number">ILIMITADO</span></li>
                                        <li><span class="text">Robo y daños materiales del equipaje</span><span class="text-number">150,25€</span></li>
                                        <li><span class="text">	Transmisión de mensajes urgentes</span><span class="text-number">ILIMITADO</span></li>
                                        <li><span class="text">Regreso anticipado por fallecimiento u hospitalización<br>  superior a dos días de
un familiar de hasta segundo grado</span><span class="text-number">ILIMITADO</span></li>
                                        <li><span class="text">Seguro de Equipajes AXA</span><span class="text-number">150,25€</span></li>
                                        <li><span class="text">Seguro de Responsabilidad Civil AXA</span><span class="text-number">60,101€</span></li>
                                    </ul>
                                <!--</div>
                                <div class="wrapper-llc">
                                    <h3 class="title-style-2">On estem?</h3>
                                    <div class="text">Les activitats que es troben en aquest portal web estan organitzades per la nostra agència de viatges especialitzada en viatges d’ estudiants FINALIA VIAJES, S.L, Agencia de Viajes Minorista, Títol-Llicència Activitat otorgat per la Generalitat de Catalunya GC-002578, CIF: B65847170.</div>
                                    <ul class="list-llc list-unstyled">
                                        <li><i class="icon fa fa-map-marker"></i><a href="#" class="item">Girona, 34. 08700 IGUALADA (Barcelona) </a></li>
                                        <li><i class="icon fa fa-phone"></i>
                                            <a href="#" class="item">
                                                <p class="ph-number">902 002 068</p>

                                            </a>

                                        </li>
                                        <li><i class="icon fa fa-phone"></i>
                                            <a href="#" class="item">

                                                <p class="ph-number">93 803 06 94</p>
                                            </a>

                                        </li>
                                        <li><i class="icon fa fa-envelope-o"></i><a href="mailto:info@byebyegroup.com" class="item">info@byebyegroup.com</a></li>
                                        <li><i class="icon fa fa-envelope-o"></i><a href="mailto:reservas@byebyegroup.com" class="item">reservas@byebyegroup.com</a></li>
                                    </ul>-->
<!--                                </div><a href="#" class="view-more"><span class="more">View our company page</span></a></div>-->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="travelers"  style="background:url(<?= base_url() ?>img/background/bg-section-traveler_1.jpg); background-position: center bottom; background-repeat: repeat; background-size: cover;">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="traveler-wrapper padding-top padding-bottom">
                        <div class="group-title white">
                            <div class="sub-title">
                                <p class="text">INFORMACIÓN PARA </p><i class="icons flaticon-security"style=" color: #e7237e"></i></div>
                            <h2 class="main-title">AMPAS Y ProfesORES</h2></div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="traveler-list">
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/pares-cover.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/pares.jpg" alt="" class="img-responsive"></div>
                                <p class="name" style="
font-weight: 900;">AMPAS</p>
                                <p class="address">¿NECESITAIS PREPARAR UN VIAJE DE FINAL DE CURSO SIN MORIR EN EL INTENTO?</p>
                                <p class="description">Este es un verdadero reto al que os enfrentáis los padres cada curso.
                                    Nosotros, que ya llevamos 15 años de experiencia en organización y gestión de viajes, os ofrecemos nuestra ayuda, apoyo y entusiasmo para poder conseguirlo.
                                    No os preocupéis de nada, nos reunimos con vosotros, os aconsejamos y os planificamos todo el viaje, siempre ajustándonos a las necesidades de cada curso.
                                    No te la juegues, dejarlo todo en manos de NUESTRA AGENCIA.

                                </p>
                            </div>
                        </div>
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/pizarra.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/profe.jpg" alt="" class="img-responsive"></div>
                                <p class="name" style="
font-weight: 900;">PROFESORES</p>
                                <p class="address">¿DESPUÉS DE TODO EL AÑO TRABAJANDO NO OS APETECE DESCONECTAR?</p>
                                <p class="description">No penséis más y disfrutar.
                                    ¡Nosotros nos encargamos! Os organizamos un viaje, tanto con compañeros, para disfrutar de buenos momentos y experiencias únicas fuera del trabajo, como en familia.
                                    En nuestra agencia tenemos un gran equipo profesional, que os ayudará en el proceso y proporcionará todos los recursos necesarios, como por ejemplo: transporte, alojamiento, visitas, etc. adaptados a vuestras demandas.
                                    Si necesitáis viajar por otros motivos, como seminarios, ponencias, reuniones,
                                    congregaciones...
                                    también os podemos ayudar.
                                    ¡¡Animaros!!

                                </p>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
    </section>
        <?php $this->load->view('_contacto'); ?>
    </div>
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top" class="link"><i class="fa fa-angle-double-up"></i></a></div>
</div>
<!--- About page ---->
<script src="<?= base_url() ?>js/template/pages/about-us.js"></script>
<script src="<?= base_url() ?>js/template/libs/nst-slider/js/jquery.nstSlider.min.js"></script>
<script src="<?= base_url() ?>js/template/libs/plus-minus-input/plus-minus-input.js"></script>
