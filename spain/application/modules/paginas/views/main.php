<div class="main-content">
    <section class="page-banner homepage-default">
        <div class="container">
            <div class="homepage-banner-warpper">
                <div class="homepage-banner-content">
                    <div class="group-title">
                        <h1 class="title titlemain">Expertos</h1>
                        <p class="text">EN viajes de fin de curso

                        </p>
                    </div>
                    <div class="group-btn">
                        <a href="<?= site_url('destinos') ?>" data-hover="FES CLICK" class="btn-click">
                            <span class="text">Explora y disfruta</span>
                            <span class="icons fa fa-long-arrow-right"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>    
    
    <section class="tours padding-top padding-bottom" style="">
        <div class="container">
            <div class="tours-wrapper">
                <div class="group-title">
                    <div class="sub-title">
                        <p class="text">Viajes</p><i class="icons flaticon-people" style=" color: #e7237e"></i></div>
                    <h2 class="main-title">nacionales</h2></div>
                <div class="tours-content margin-top70">
                    <?php $this->db->limit(3) ?>
                         <div class="tours-list">
                            <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>1))->result() as $d): ?>
                                <?php $this->load->view('_destino',array('d'=>$d)); ?>
                            <?php endforeach ?>
                        </div>
                    <div class="tours-list" style="margin-top:120px">
                        <?php $this->db->limit(3,3) ?>
                        <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>1))->result() as $d): ?>
                                <?php $this->load->view('_destino',array('d'=>$d)); ?>
                        <?php endforeach ?>
                    </div>
                    <a href="<?= site_url('destinos/pack/3') ?>" class="btn btn-maincolor margin-top70" style=" margin-top: 120px">más viajes</a>
                </div>
            </div>
        </div>
    </section>

    
    <section class="tours padding-top padding-bottom" style="margin-top:80px;">
        <div class="container">
            <div class="tours-wrapper">
                <div class="group-title">
                    <div class="sub-title">
                        <p class="text">viajes</p><i class="icons flaticon-transport-1"style=" color: #e7237e"></i></div>
                    <h2 class="main-title">internacionales</h2></div>
                <div class="tours-content margin-top70">
                    <div class="tours-list">
                        <?php $this->db->limit(3) ?>
                        <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>2))->result() as $d): ?>
                                <?php $this->load->view('_destino',array('d'=>$d)); ?>
                        <?php endforeach ?>                        
                    </div>                    
                    <div class="tours-list" style="margin-top:120px">
                        <?php $this->db->limit(3,3) ?>
                        <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>2))->result() as $d): ?>
                                <?php $this->load->view('_destino',array('d'=>$d)); ?>
                        <?php endforeach ?>
                    </div>
                    
                    <a href="<?= site_url('destinos/pack/4') ?>" class="btn btn-maincolor margin-top70">más viajes</a></div>
            </div>
        </div>
    </section>
    
    
    <section class="hotels padding-top padding-bottom">
        <div class="container">
            <div class="tours-wrapper">
                <div class="group-title">
                    <div class="sub-title">
                        <p class="text">VIAJES Y PACKS </p><i class="icons flaticon-sport"style=" color: #e7237e"></i></div>
                    <h2 class="main-title">AVENTURA Y ESPECIALES</h2></div>
                <div class="tours-content margin-top70">
                    <?php $this->db->limit(3) ?>
                         <div class="tours-list">
                            <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>3))->result() as $d): ?>
                                <?php $this->load->view('_destino',array('d'=>$d)); ?>
                            <?php endforeach ?>
                        </div>
                    <div class="tours-list" style="margin-top:120px">
                        <?php $this->db->limit(3,3) ?>
                        <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>3))->result() as $d): ?>
                                <?php $this->load->view('_destino',array('d'=>$d)); ?>
                        <?php endforeach ?>
                    </div>
                    <a href="<?= site_url('destinos/pack/2') ?>" class="btn btn-maincolor margin-top70">más viajes</a>
                </div>
            </div>
        </div>
    </section>
    <section class="travelers">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="traveler-wrapper padding-top padding-bottom">
                        <div class="group-title white">
                            <div class="sub-title">
                                <p class="text">Recomendaciones de los</p><i class="icons flaticon-people-2"style=" color: #e7237e"></i></div>
                            <h2 class="main-title">Profesores</h2></div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="traveler-list">
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/homepage/cover-image-1.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/homepage/avatar-1.jpg" alt="" class="img-responsive"></div>
                                <p class="name">Tutores de 4º d'ESO</p>
                                <p class="address">INS Castellbisbal</p>
                                <p class="description">" Hemos realizado un viaje de estudios de final de 4º de ESO en San Sebastián con finales. Ha sido una buenísima experiencia, las actividades programadas,
                                                                         el hotel donde nos hemos alojado (muy bien dotado y con una atención impecable), incluso nos ha acompañado el buen tiempo. Muchas gracias a la gente de final por su profesionalidad tanto en lo que estaba programado como en la resolución de imprevistos y problemas"</p>
                            </div>
                        </div>
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/homepage/cover-image-2.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/homepage/avatar-2.jpg" alt="" class="img-responsive"></div>
                                <p class="name">Tutores de 4º d'ESO</p>
                                <p class="address">Escola L'Oreig</p>
                                <p class="description">"Este curso y por segundo año con los alumnos de 4 de ESO hemos ido en Cantabria con final, una organización muy profesional que cuida todos los detalles, desde el primer momento hasta el último.
                                                                         Ha sido una muy buena experiencia. Los monitores de primera, han sabido tratar a los alumnos porque disfrutaran y se llevaran un buen recuerdo, todo ello ligado con la estancia en un hotel con instalaciones adecuadas y un personal encantador"</p>
                            </div>
                        </div>
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/homepage/cover-image-3.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/homepage/avatar-3.jpg" alt="" class="img-responsive"></div>
                                <p class="name">Tutores de 4º d'ESO</p>
                                <p class="address">Inst. La Serreta</p>
                                <p class="description">"La experiencia de viajar a Mallorca con final ha sido fantástica! Sin trabas ni sustos, cosa que se agradece cuando se viaja con un grupo numeroso de adolescentes. Muy contentos con la profesionalidad y servicio, tanto del personal del hotel,
                                                                         los transfers, del barco y de los monitores de las actividades. La estancia ha sido divertida con actividades culturales y deportivas inclusivas y estimulantes. Todo el mundo lo volveríamos a repetir!"</p>
                            </div>
                        </div>
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/homepage/cover-image-4.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/homepage/avatar-4.jpg" alt="" class="img-responsive"></div>
                                <p class="name">Tutores de 4º d'ESO</p>
                                <p class="address">FEDAC Guissona</p>
                                <p class="description">"Cantàbria en 4 dies és possible? la resposta és rotundament sí.
                                    Han estat 4 dies de llibertat, natura, mar, sol, esport,
                                    muntanya, felicitat i bon humor. Tan els alumnes com nosaltres hem gaudit moltíssim, també del menjar i del dormir. No us perdeu el far de Santander,
                                    els pics d'Europa, el surf i les canoes, la playa de los locos, passejar a la nit per Suances i matinar molt per veure sortir el sol. Un viatge de 10 al que afegiríem, això sí, un parell de dies més."</p>
                            </div>
                        </div>
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/homepage/cover-image-5.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/homepage/avatar-5.jpg" alt="" class="img-responsive"></div>
                                <p class="name">Tutores de Batxillerat</p>
                                <p class="address">Escola Diocesana de Navàs</p>
                                <p class="description">"Roma en 5 días, con 10 estudiantes de Bachillerato y 2 profesoras. Una pequeña aventura que gracias a la atención de final realizamos muy felizmente.
                                                                         Salimos de madrugada, pero esto nos permitió aprovechar muy bien el primer día de viaje. A las 8 ya aterrizábamos en Roma y comenzaba un día lleno de aventuras y reconocimiento de la ciudad.
                                                                         Roma queda en nuestro recuerdo como un destino donde nos hemos podido conocer mejor, pero también como una ciudad interesante.
                                                                         Gracias por hacerlo posible."</p>
                            </div>
                        </div>
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/homepage/cover-image-6.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/homepage/avatar-6.jpg" alt="" class="img-responsive"></div>
                                <p class="name">Tutores de 4º d’ESO</p>
                                <p class="address">INS Fax</p>
                                <p class="description">"Nuestros alumnos disfrutaron mucho de todas las actividades de aventura (tiro con arco, escalada, kayak). Una organización impecable y unos monitores muy alentadores. Repetiremos seguro!
                                    "</p>
                            </div>
                        </div>
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/homepage/cover-image-7.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/homepage/avatar-7.jpg" alt="" class="img-responsive"></div>
                                <p class="name">Tutores de l’escola </p>
                                <p class="address">La Gaspar</p>
                                <p class="description">"La Escuela Municipal de Arte y Diseño de Igualada "La Gaspar" ha ido de viaje de fin de ciclo en Berlín y en la Bauhaus.
                                                                         Hemos elegido este destino porque por nuestros estudios en los ciclos formativos son un referente cultural imprescindible.
                                                                         Ha sido una experiencia inolvidable poder pasearnos por la escuela de la Bauhaus y descubrir lugares emblemáticos de Berlín.
                                                                         Agradecemos a finales-ByeByeGroup todas las gestiones hechas para poder hacer realidad este viaje.
                                    "</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="news padding-top padding-bottom">
        <div class="container">
            <div class="news-wrapper">
                <div class="group-title">
                    <div class="sub-title">
                        <p class="text">EXPLORA nuestras histórias</p><i class="icons flaticon-summer"style=" color: #e7237e"></i></div>
                    <h2 class="main-title">BLOG</h2></div>
                        <div class="news-content margin-top70">
                            <div class="news-list">
                                <?php foreach($blog->result() as $b): ?>
                                        <div class="new-layout">
                                            <div class="image-wrapper">
                                                <a href="<?= $b->link ?>" class="link">
                                                    <img src="<?= $b->foto ?>" alt="" class="img-responsive">
                                                </a>
                                            </div>
                                            <div class="content-wrapper">
                                                <a href="<?= $b->link ?>" class="title"><?= $b->titulo ?></a>
                                                <ul class="info list-inline list-unstyled">
                                                    <li><a href="#" class="link"><?= strftime("%B %d, %Y",strtotime($b->fecha)) ?> </a></li>
                                                    <li><a href="#" class="link"><?= $b->user ?></a></li>
                                                </ul>
                                                <p class="text"><?= substr(strip_tags($b->texto),0,500) ?></p>
                                                <?php substr(strip_tags($b->texto),0,300) ?>
                                                <a href="<?= $b->link ?>" class="btn btn-maincolor">Leer mas</a>
                                                <div class="tags">
                                                    <div class="title-tag">tags:</div>
                                                    <ul class="list-inline list-unstyled list-tags">
                                                        <?php foreach(explode(',',$b->tags) as $t): ?>
                                                            <li><a href="<?= site_url('blog') ?>?direccion=<?= $t ?>" class="tag"><?= $t ?></a></li>
                                                        <?php endforeach ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                <?php endforeach ?>
                            </div>
                        </div>
            </div>
        </div>
    </section>
    <section class="videos layout-1">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="video-wrapper padding-top padding-bottom">
                        <h5 class="sub-title">Los mejores <strong>finales </strong> de curso</h5>
                        <h2 class="title">explóranos</h2>
                        <div class="text">
                            Mira nuestro video y verás que apostamos por destinos que combinan una parte cultural con visitas a los lugares más emblemáticos de cada región, y una parte lúdica con divertidas actividades de aventura que encantarán a los alumnos.                        </div>
                        <a href="<?= site_url('p/nosotros') ?>" class="btn btn-maincolor">Leer más</a>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="video-thumbnail">
                        <div class="video-bg"><img src="<?= base_url() ?>img/homepage/video-bg.jpg" alt="" class="img-responsive"></div>
                        <div class="video-button-play"> <i class="icons fa fa-play"></i></div>
                        <div class="video-button-close"></div>
                        <iframe src="https://www.youtube.com/embed/7Iiw_NqzxN4?rel=0" allowfullscreen="allowfullscreen" class="video-embed"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php $this->load->view('_contacto'); ?>
</div>
<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="https://v2.zopim.com/?560oMeEimgaeT4NAgVG2vZSj9buasjpu";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->