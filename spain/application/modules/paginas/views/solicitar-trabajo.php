<!-- Top shadow -->
<div class="shadow"></div>
<!-- end top shadow -->

<!-- The splash screen -->
<div id="splash">
    <div class="loader">
        <img class="splash-logo" src="<?= base_url() ?>img/logo/logo.svg" />
        <div class="line"></div>
    </div>
</div>
<!-- End of splash screen -->

<div id="wrapper">
    <!-- main content -->
    <main>
        <!-- The header for content -->
        <header class="detail">
            <a href="<?= site_url() ?>" class="back" data-transition="slide-from-top">
                <h1>volver</h1>
            </a>
            <section>
                <h3 class="badge">Quieres colaborar</h3>
                <h1>con nosotros?</h1>
            </section>
        </header>
        <!-- end header -->
        <div class="content-wrap">
            <section class="content">
                <i class="icon bg icon-Users"></i>
                
                <section class="form-inline">
                    <header>
                        <h2>Colaboremos</h2>
                        <h4 class="serif">todos juntos!</h4>
                    </header>
                    <p>¿Quieres abrir una oficina Finques Sasi en tu zona?, ¿Quieres formar parte de nuestra plantilla como agente,...? ¿Tienes una amplia cartera de clientes y quieres compartirla? O simplemente tienes una idea que puede encajar en nuestra empresa. No dudes en rellenar el formulario adjunto y subir tu curriculum, inmediatamente nos pondremos en contacto contigo. </p>
                    <form action="<?= base_url('paginas/frontend/enviarCurriculum') ?>" id="formsol" method="post" class="form ambiance-html-form-solicitud">
                        <div class="row">
                            <div class="form-group">
                                <input name="nombre" id="nombre" type="text" placeholder="nombre">
                                <i class="icon icon-User"></i>
                            </div>
                            <div class="form-group">
                                <input class="full-border" name="telefono" id="telefon" type="text" placeholder="teléfono">
                                <i class="icon icon-iPad"></i>
                            </div>
                            <div class="form-group fullwidth">
                                <input class="full-border" name="email" id="email" type="email" placeholder="email">
                                <i class="icon icon-Email"></i>
                            </div>                            
                        </div>
                        
                        <div class="form-group">
                            <textarea rows="10" cols="40" required="required" name="mensaje" id="body" placeholder="mensaje"></textarea>
                            <i class="icon icon-Typing"></i>
                        </div>
                        <div class="form-group full-width formfile">
                            <label for="curriculum">
                                <div style="padding:15px 50px 15px 20px; cursor:pointer;">     
                                        Subir tu Curriculum
                                        <input type="file" id="curriculum" name="curriculum" style="visibility: hidden">
                                        <i class="icon icon-Attachment"></i>                                
                                </div>
                            </label>
                        </div>
                        <span class="message"><strong>Estado.</strong> idle</span>
                        <div class="submit">
                            <button type="submit" value="submit">
                                <i class="icon icon-Forward"></i>
                            </button>
                        </div>
                    </form>
                </section>
            </section>
        </div>
        <div data-remodal-id="modal">
            <i class="icon bg icon-CommentwithLines"></i>
            <button data-remodal-action="close" class="remodal-close"></button>
             <h1>Grácias!</h1>
            <p>Nos pondremos en contancto contigo lo más pronto posible!</p>
            <div class="signature center">
                <h6>-Finques Sasi-</h6>
            </div>
        </div>
    </main>
    <!-- end of main content -->
</div>

<!-- The slideshow -->
<ul id="slideshow" data-speed="6000">
    <li>
        <img src="<?= base_url() ?>img/slideshow/colabora.jpg" alt="slideshow image" />
    </li>
  <!-- 
  <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
    <li>
        <img src="<?= base_url() ?>img/slideshow/demo.jpg" alt="slideshow image" />
    </li>
 -->
</ul>
<!-- end of slideshow -->

<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe.
     It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides.
        PhotoSwipe keeps only 3 of them in the DOM to save memory.
        Don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                            <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>

            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

        </div>

    </div>

</div>
