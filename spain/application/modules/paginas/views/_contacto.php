<section class="contact style-1" id='contactenosform' style=" margin-top: 120px;">
    <div class="container">
        <div class="row">
            <div class="wrapper-contact-style">
                <div data-wow-delay="0.5s" class="contact-wrapper-images wow fadeInLeft">
                    <img src="<?= base_url() ?>img/homepage/contact-people.png" alt="" class="img-responsive">
                </div>
                <div class="col-lg-6 col-sm-7 col-lg-offset-4 col-sm-offset-5">
                    <div data-wow-delay="0.4s" class="contact-wrapper padding-top padding-bottom wow fadeInRight">
                        <div class="contact-box">
                            <?= @$_SESSION['msj'] ?>
                            <?php $_SESSION['msj'] = ''; ?>
                            <h5 class="title">FORMULARIO DE CONTACTO</h5>
                            <p class="text" style="color: #222;">Si no has encontrado el viaje que te gustaría hacer, cuéntanoslo !!.</p>
                            <form class="contact-form" method="post" action="<?= base_url('paginas/frontend/contacto') ?>?redirect=<?= base_url() ?>#contactenosform">
                                <input name="name" type="text" placeholder="Tu nombre" class="form-control form-input">
                                <input name='email' type="email" placeholder="Tu Email" class="form-control form-input">
                                <input name="tel" type="text" placeholder="Tu teléfono" class="form-control form-input">
                                <input name="centro" type="text" placeholder="Tu centro" class="form-control form-input">
                                <input name="localitat" type="text" placeholder="Tu localidad" class="form-control form-input">
                                <textarea name="message" placeholder="Tu viaje (Recuerda especificar: Número de viatjess, fecha del viaje, regimen deseado y actividades pedidas.)" class="form-control form-input"></textarea>
                                <div class="contact-submit">
                                    <div style="margin-bottom: 50px">
                                        <div class="g-recaptcha" data-sitekey="6LfG5i0UAAAAABfvs7-lS8DDkZPMlwEXOcs1TMWi"></div>
                                    </div>
                                    <button type="submit" data-hover="SEND NOW" class="btn btn-slide">
                                        <span class="text">enviar ahora</span>
                                        <span class="icons fa fa-long-arrow-right"></span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
