<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('form_validation');
        }        
        
        function read($url){
            $theme = $this->theme;
            $params = $this->uri->segments;
            $this->load->model('querys');            
            $this->loadView(
                array(
                    'view'=>'read',
                    'page'=>$this->load->view($theme.$url,array(),TRUE),
                    'title'=>ucfirst(str_replace('-',' ',$url))                    
                )
            );
        }
        
        function getFormReg($x = '2'){                    
            return $this->querys->getFormReg($x);
        }
        
        function editor($url){            
            $this->load->helper('string');
            if(!empty($_SESSION['user']) && $this->user->admin==1){                
                //$page = file_get_contents('application/modules/paginas/views/'.$url.'.php');
                /*$page = str_replace('<?php','[?php',$page);
                $page = str_replace('<?=','[?=',$page);
                $page = str_replace('&gt;','>',$page);
                $page = str_replace('&lt;','<',$page);*/
                $page = $this->load->view($url,array(),TRUE);
                $this->loadView(array('view'=>'cms/edit','scripts'=>true,'name'=>$url,'edit'=>TRUE,'page'=>$page,'title'=>'Editar '.ucfirst(str_replace('-',' ',$url))));
            }else{
                redirect(base_url());
            }
        }
        
        function contacto(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('name','Nombre','required');
            $this->form_validation->set_rules('message','Comentario','required');
            if($this->form_validation->run()){
                $this->load->library('recaptcha');
                if(!$this->recaptcha->recaptcha_check_answer($_POST['g-recaptcha-response'])){
                        $_SESSION['msj'] = $this->error("No se ha validado el captcha correctamente.");                        
                }else{
                    $this->enviarcorreo((object)$_POST,1,'info@byebyegroup.com');                
                    $this->enviarcorreo((object)$_POST,6);                
                    $_SESSION['msj'] = $this->success('Gracias por contactarnos, en breve le llamaremos');
                }
            }else{                
               $_SESSION['msj'] = $this->success('Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>');               
            }             
            if(!empty($_GET['redirect'])){
                redirect($_GET['redirect']);
            }else{
                redirect(base_url('p/contacto').'#contactenosform');
            }
        }
        
        function enviarCurriculum(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre','Nombre','required');
            $this->form_validation->set_rules('mensaje','Comentario','required');
            //$this->form_validation->set_rules('curriculum','Curriculum','required');            
            if($this->form_validation->run() && !empty($_FILES['curriculum'])){
                get_instance()->load->library('mailer');
                get_instance()->mailer->mail->AddAttachment($_FILES['curriculum']['tmp_name'],$_FILES['curriculum']['name']);
                $this->enviarcorreo((object)$_POST,2,'info@finques-sasi.com');
                //$_SESSION['msj'] = $this->success('Gracias por contactarnos, en breve le llamaremos');
                echo json_encode(array('success'=>TRUE,'message'=>'Gracias por contactarnos, en breve le contactaremos'));
            }else{
                //$_SESSION['msj'] = $this->error('Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>');
               echo json_encode(array('success'=>FALSE,'message'=>'Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>'));
            }
            /*if(!empty($_GET['redirect'])){
                redirect($_GET['redirect']);
            }else{
                redirect(base_url('p/contacto'));
            }*/
        }
        
        function subscribir(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            if($this->form_validation->run()){
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()==0?TRUE:FALSE;
                if($success){
                    $this->db->insert('subscritos',array('email'=>$_POST['email']));    
                    echo $this->success('Subscrito satisfactoriamente');
                }else{
                    echo $this->error('Correo ya existente');
                }
            }else{
                echo $this->error($this->form_validation->error_string());
            }
        }
        
        function unsubscribe(){
            if(empty($_POST)){
                $this->loadView('includes/template/unsubscribe');
            }else{
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()>0?TRUE:FALSE;
                if($success){
                    $this->db->delete('subscritos',array('email'=>$_POST['email']));
                    echo $this->success('Correo desafiliado al sistema de noticias');
                }            
                $this->loadView(array('view'=>'includes/template/unsubscribe','success'=>$success));
            }
        }
        
        function pdf(){
            require_once APPPATH.'libraries/html2pdf/html2pdf.php';
            $papel = 'L';
            $orientacion = 'P';
            $html2pdf = new HTML2PDF();
            $html2pdf->setDefaultFont('raleway');
            $html2pdf->addFont("ralewayb","B");
            $html2pdf->addFont("titanone","");
            $menu = $this->db->get('menu_del_dia')->row();
            $html = $menu->pdf;
            foreach($menu as $n=>$v){
                $html = str_replace('['.$n.']', str_replace('&bull;','<br/> &bull;',strip_tags($v)),$html);
            }
            $html = str_replace('[precio]',$this->db->get('ajustes')->row()->precio_menu_dia.'€',$html);
            $html = $this->load->view('pdf',array('html'=>$html),TRUE);
            $html2pdf->writeHTML($html); 
            //echo $this->db->get('menu_del_dia')->row()->pdf;
            ob_end_clean();         
            $html2pdf->Output('Menu-del-dia-'.date("dmY").'.pdf');
        }
    }
