<!-- MAIN CONTENT-->
<div class="main-content">
    <section class="page-banner contact-page">
        <div class="container">
            <div class="page-title-wrapper">
                <div class="page-title-content">
                    <ol class="breadcrumb">
                        <li><a href="index.html" class="link home">Home</a></li>
                        <li class="active"><a href="#" class="link">contacte</a></li>
                    </ol>
                    <div class="clearfix"></div>
                    <h2 class="captions">Aquí estem</h2></div>
            </div>
        </div>
    </section>
    <section class="padding-top padding-bottom contact-organization">
        <div class="container">
            <h3 class="title-style-2">El nostre equip</h3>
            <div class="row">
                <div class="wrapper-organization">
                    <div class="col-md-4 col-sm-4 col-xs-4 md-organization">
                        <div class="content-organization">
                            <div class="wrapper-img"><img src="<?= base_url() ?>img/homepage/avatar-contact-1.jpg" alt="" class="img img-responsive"></div>
                            <div class="main-organization">
                                <div class="organization-title"> <a href="#" class="title">Anna Riba</a>
                                    <p class="text">Commercial Director</p>
                                </div>
                                <div class="content-widget">
                                    <div class="info-list">
                                        <ul class="list-unstyled">
                                            <li class="main-list"><i class="icons fa fa-map-marker"></i><a href="#" class="link">1390 Somerset Drive, Raleigh, NC</a></li>
                                            <li class="main-list"><i class="icons fa fa-phone"></i><a href="#" class="link">910-740-6026</a></li>
                                            <li class="main-list"><i class="icons fa fa-envelope-o"></i><a href="#" class="link">domain@expooler.com</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 md-organization">
                        <div class="content-organization">
                            <div class="wrapper-img"><img src="<?= base_url() ?>img/homepage/avatar-contact-2.jpg" alt="" class="img img-responsive"></div>
                            <div class="main-organization">
                                <div class="organization-title"> <a href="#" class="title">Jason Williams</a>
                                    <p class="text">Commercial Director</p>
                                </div>
                                <div class="content-widget">
                                    <div class="info-list">
                                        <ul class="list-unstyled">
                                            <li class="main-list"><i class="icons fa fa-map-marker"></i><a href="#" class="link">1390 Somerset Drive, Raleigh, NC</a></li>
                                            <li class="main-list"><i class="icons fa fa-phone"></i><a href="#" class="link">910-740-6026</a></li>
                                            <li class="main-list"><i class="icons fa fa-envelope-o"></i><a href="#" class="link">domain@expooler.com</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 md-organization">
                        <div class="content-organization">
                            <div class="wrapper-img"><img src="<?= base_url() ?>img/homepage/avatar-contact-3.jpg" alt="" class="img img-responsive"></div>
                            <div class="main-organization">
                                <div class="organization-title"> <a href="#" class="title">Jason Williams</a>
                                    <p class="text">Commercial Director</p>
                                </div>
                                <div class="content-widget">
                                    <div class="info-list">
                                        <ul class="list-unstyled">
                                            <li class="main-list"><i class="icons fa fa-map-marker"></i><a href="#" class="link">1390 Somerset Drive, Raleigh, NC</a></li>
                                            <li class="main-list"><i class="icons fa fa-phone"></i><a href="#" class="link">910-740-6026</a></li>
                                            <li class="main-list"><i class="icons fa fa-envelope-o"></i><a href="#" class="link">domain@expooler.com</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="contact style-1 page-contact-form padding-top padding-bottom" id='contactenosform'>
        <div class="container">
            <div class="wrapper-contact-form">
                <div data-wow-delay="0.5s" class="contact-wrapper wow fadeInLeft">
                    <div class="contact-box">
                        <?= @$_SESSION['msj'] ?>
                        <?php $_SESSION['msj'] = ''; ?>
                        <h5 class="title" style="
    color: #222;
">FORMULARIO DE CONTACTO</h5>
                        <p class="text" style="
    color: #222;
">Subcribe to receive new properties with good price.</p>
                        <form class="contact-form" method="post" action="<?= base_url('paginas/frontend/contacto') ?>">
                            <input name="name" type="text" placeholder="El teu nom" class="form-control form-input">
                            <input name='email' type="email" placeholder="El teu Email" class="form-control form-input">
                            <textarea name="message" placeholder="Missatge" class="form-control form-input"></textarea>
                            <div class="contact-submit">
                                <button type="submit" data-hover="SEND NOW" class="btn btn-slide">
                                    <span class="text">ENVIAR ARA</span>
                                    <span class="icons fa fa-long-arrow-right"></span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div data-wow-delay="0.5s" class="wrapper-form-images wow fadeInRight"><img src="<?= base_url() ?>img/background/bg-banner-contact-form.jpg" alt="" class="img-responsive"></div>
            </div>
        </div>
    </section>
    <section class="page-contact-map">
        <div class="map-block">
            <div class="wrapper-info">
                <div class="map-info">
                    <h3 class="title-style-2">ON ESTEM</h3>
                    <p class="address"><i class="fa fa-map-marker"></i> Carrer Girona, 34. <br>08700 IGUALADA. Barcelona</p>
                    <p class="phone"><i class="fa fa-phone"></i> +34-938 030 694</p>
                    <p class="mail">
                        <a href="mailto:domain@expooler.com"> <i class="fa fa-envelope-o"></i>info@byebyegroup.com</a>
                    </p>
                    <div class="footer-block"><a class="btn btn-open-map">Veure Mapa</a></div>
                </div>
            </div>
            <div id="googleMap"></div>
        </div>
    </section>
</div>