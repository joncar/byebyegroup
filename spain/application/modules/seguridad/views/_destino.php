<div class="tours-layout">
    <?php if ($d->lowcost): ?>
        <img src="<?= base_url() ?>img/tours/lowcost.svg" alt="" class="img-responsive" style="position: absolute; z-index: 10000; width: 110px; left:-16px; top: 5px">
    <?php endif ?>
    <div class="image-wrapper">
        <a href="<?= site_url('destinos/' . toURL($d->id . '-' . $d->destinos_nombre)) ?>" class="link">
            <img src="<?= base_url() ?>img/destinos/<?= $d->portada ?>" alt="" class="img-responsive">
        </a>
        <div class="title-wrapper">
            <a href="<?= site_url('destinos/' . toURL($d->id . '-' . $d->destinos_nombre)) ?>" class="title"><?= $d->destinos_nombre ?></a>
            <i class="icons flaticon-circle"></i>
        </div>                                
    </div>
    <div class="content-wrapper">
        <ul class="list-info list-inline list-unstyle">
            <li><a href="#" class="link"><i class="icons fa fa-eye"></i><span class="text number"><?= $d->visitas ?></span></a></li>
            <li><a id='likes<?= $d->id ?>' href="javascript:sumarCorazon('<?= $d->id ?>')" class="link"><i class="icons fa fa-heart"></i><span class="text number"><?= count(json_decode($d->likes)) ?></span></a></li>
            <li>
                <a href="#" class="link">                    
                        <i class="icons fa <?php if($d->tipo_transporte==1) echo 'fa-bus'; elseif($d->tipo_transporte==2)echo 'fa-plane'; else echo'fa-ship'; ?>"></i>
                        <span class="text number"> <?php if($d->tipo_transporte==1) echo 'AUTOCAR'; elseif($d->tipo_transporte==2)echo 'AVIÒ'; else echo 'vaixell'; ?></span>
                </a>
            </li>
        </ul>
        <div class="content">
            <div class="title">
                <div class="price"><span class="number"><?= $d->precio ?></span><sup>€</sup></div>
                <p class="for-price"><?= $d->disponibilidad ?></p>
            </div>
            <p class="text"><?= $d->descripcion_corta ?></p>
            <div class="group-btn-tours">
                <a href="<?= site_url('destinos/' . toURL($d->id . '-' . $d->destinos_nombre)) ?>" class="left-btn" tabindex="0">Pressupost</a>
                <?php $active = !empty($_SESSION['user']) && $this->db->get_where('wishlist',array('destinos_id'=>$d->id,'user_id'=>$this->user->id))->num_rows()>0?true:false; ?>
                <a id="wish<?= $d->id ?>" href="javascript:addToWish(<?= $d->id ?>)" class="right-btn <?= $active>0?'active':'' ?>" tabindex="0">Afegir a preferits</a>
            </div>
        </div>
    </div>
</div>