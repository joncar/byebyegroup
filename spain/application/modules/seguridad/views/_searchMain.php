<section>
        <div class="tab-search tab-search-long tab-search-default">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul role="tablist" class="nav nav-tabs">
                            <li role="presentation" class="tab-btn-wrapper active">
                                <a href="#flight" aria-controls="flight" role="tab" data-toggle="tab" class="tab-btn">
                                    <i class="flaticon-people"></i>
                                    <span class="text">DESTINS</span>
                                    <span class="xs">FIND YOUR FLIGHT</span>
                                </a>
                            </li>
                            <li
                                role="presentation" class="tab-btn-wrapper">
                                <a href="#transfer" aria-controls="transfer" role="tab" data-toggle="tab" class="tab-btn">
                                    <i class="flaticon-security"></i>
                                    <span class="text">CULTURALS</span>
                                    <span class="xs">FIND TRANSFER</span>
                                </a>
                            </li>
                            <li
                                role="presentation" class="tab-btn-wrapper">
                                <a href="#hotel" aria-controls="hotel" role="tab" data-toggle="tab" class="tab-btn">
                                    <i class="flaticon-sport"></i>
                                    <span class="text">AVENTURES</span>
                                    <span class="xs">FIND HOTEL</span>
                                </a>
                            </li>
                            <li role="presentation"
                                class="tab-btn-wrapper">
                                <a href="#tours" aria-controls="tours" role="tab" data-toggle="tab" class="tab-btn">
                                    <i class="flaticon-transport-7"></i>
                                    <span class="text">AMB BUS</span>
                                    <span class="xs">FIND TOURS</span>
                                </a>
                            </li>
                            <li role="presentation"
                                class="tab-btn-wrapper">
                                <a href="#car-rent" aria-controls="car-rent" role="tab" data-toggle="tab" class="tab-btn">
                                    <i class="flaticon-transport-1"></i>
                                    <span class="text"> amb AVIÓ</span>
                                    <span class="xs">FIND CAR RENT</span>
                                </a>
                            </li>
                            <li
                                role="presentation" class="tab-btn-wrapper">
                                <a href="#cruises" aria-controls="cruises" role="tab" data-toggle="tab" class="tab-btn">
                                    <i class="flaticon-transport-11"></i>
                                    <span class="text">amb VAIXELL</span>
                                    <span class="xs">FIND CRUISES</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-content-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="tab-content">
                                <div role="tabpanel" id="flight" class="tab-pane fade in active">
                                    <div class="find-widget find-flight-widget widget">
                                        <h4 class="title-widgets">BUSCA EL TEU DESTÍ QUE DESITGES</h4>
                                        <?php $this->load->view('_searchMainForm',array()); ?>
                                    </div>
                                </div>
                                <div role="tabpanel" id="transfer" class="tab-pane fade">
                                    <div class="find-widget find-transfer-widget widget">
                                        <h4 class="title-widgets">BUSQUES VIATGES CULTURALS</h4>
                                        <?php $this->load->view('_searchMainForm',array('hidden'=>array('tipo_destino','1'))); ?>
                                    </div>
                                </div>
                                <div role="tabpanel" id="hotel" class="tab-pane fade">
                                    <div class="find-widget find-hotel-widget widget">
                                        <h4 class="title-widgets">BUSQUES VIATGES D'AVENTURA?</h4>
                                        <?php $this->load->view('_searchMainForm',array('hidden'=>array('tipo_destino','2'))); ?>
                                    </div>
                                </div>
                                <div role="tabpanel" id="tours" class="tab-pane fade">
                                    <div class="find-widget find-tours-widget widget">
                                        <h4 class="title-widgets">BUSQUES viatges amb bus? </h4>
                                        <?php $this->load->view('_searchMainForm',array('hidden'=>array('tipo_transporte','1'))); ?>
                                    </div>
                                </div>
                                <div role="tabpanel" id="car-rent" class="tab-pane fade">
                                    <div class="find-widget find-car-widget widget">
                                        <h4 class="title-widgets">Busques viatges mab avió?</h4>
                                       <?php $this->load->view('_searchMainForm',array('hidden'=>array('tipo_transporte','2'))); ?>
                                    </div>
                                </div>
                                <div role="tabpanel" id="cruises" class="tab-pane fade">
                                    <div class="find-widget find-cruises-widget widget">
                                        <h4 class="title-widgets">Busques viatges amb vaixell?</h4>
                                        <?php $this->load->view('_searchMainForm',array('hidden'=>array('tipo_transporte','3'))); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>