<!-- WRAPPER-->
<div id="wrapper-content">
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <section class="page-banner tour-view">
            <div class="patterndestino"></div>
            <div class="container">
                <div class="page-title-wrapper">
                    <div class="page-title-content">
                        <ol class="breadcrumb">
                            <li><a href="index.html" class="link home">Home</a></li>
                            <li><a href="hotel-result.html" class="link">Galeria</a></li>
<!--                            <li class="active"><a href="#" class="link">london tour</a></li>-->
                        </ol>
                        <div class="clearfix"></div>
                        <h2 class="captions">Galeria</h2>
<!--                        <div class="price"><span class="text">from</span><span class="number">1500</span><sup class="unit">$</sup></div>-->
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="tour-view-main padding-top padding-bottom">                
                <div class="gallery-block margin-top70">
                    <div class="container">
<!--                        <h3 class="title-style-2">Gallery</h3>-->
                        <div class="grid">
                            <div class="grid-sizer"></div>
                            <div class="gutter-sizer"></div>
                            <?php $this->db->order_by('orden','ASC') ?>
                            <?php foreach($this->db->get('galeria')->result() as $p): ?>
                                <div class="grid-item img-small pdr">
                                    <div class="gallery-image">
                                        <a href="<?= base_url() ?>img/galeria/<?= $p->foto ?>" data-fancybox-group="gallery" class="title-hover dh-overlay fancybox">
                                            <i class="icons fa fa-eye"></i>
                                        </a>
                                        <div class="bg"></div>
                                    </div>
                                </div>
                            <?php endforeach ?>      
                        </div>
                    </div>
                </div>                
            </div>
        </section>
    </div>
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top" class="link"><i class="fa fa-angle-double-up"></i></a></div>
</div>
<!------- Tour page ---------->
<script src="<?= base_url() ?>js/template/libs/isotope/isotope.min.js"></script>
<script src="<?= base_url() ?>js/template/libs/fancybox/js/jquery.mousewheel-3.0.6.pack.js"></script>        
<script src="<?= base_url() ?>js/template/libs/mouse-direction-aware/jquery.directional-hover.js"></script>
<script src="<?= base_url() ?>js/template/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url() ?>js/template/pages/tour-view.js"></script>
