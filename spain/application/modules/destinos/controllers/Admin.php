<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        public function conserge(){
            $crud = $this->crud_function('','');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        public function tarifas_bus(){
            $crud = $this->crud_function('','');     
            $crud->set_subject('Tarifa');
            $crud->set_clone();
            $crud->unset_delete();
            $crud = $crud->render();
            $crud->title = 'Tarifa de bus';
            $this->loadView($crud);
        }
        
        public function categorias_destinos(){
            $crud = $this->crud_function('','');     
            $crud->set_subject('Categoria');
            $crud->add_action('<i class"fa fa-plane"></i> Destinos','',base_url('destinos/admin/destinos').'/');
            $crud->set_lang_string('insert_success_message','Su registro fue almacenado con éxito, <a href="'.base_url('destinos/admin/destinos').'/{id}/add">Adm. Destins</a> |');
            $crud->set_field_upload('foto','img/destinos');
            $crud->set_field_upload('fondo','img/destinos');
            
            $crud = $crud->render();
            $crud->title = 'Categoria de destinos';
            $this->loadView($crud);
        }
        
       public function destinos($x = ''){
            $crud = $this->crud_function('','');     
            $crud->set_subject('Destino');
            $crud->set_field_upload('portada','img/destinos')
                 ->set_field_upload('imagen_itinerario','img/destinos');
            $crud->set_relation('categorias_destinos_id','categorias_destinos','categorias_destinos_nombre');
            $crud->unset_fields('likes','visitas');
            $crud->columns('portada','destinos_nombre','visitas');
            $crud->field_type('destino','enum',array('Europa','Catalunya','España'))
                 ->field_type('servicios_pdf','tags');
             $crud->field_type('tipo_destino','dropdown',array('1'=>'Cultural','2'=>'Aventura'));
             $crud->field_type('tipo_transporte','dropdown',array('1'=>'Bus','2'=>'Avion','3'=>'Barco'));
             $crud->field_type("lowcost","dropdown",array("3"=>"Normal","1"=>"Lowcost","2"=>"Estrella"))
                      ->display_as("lowcost","Tipo de desino")
                      ->display_as('imagen_itinerario','Imagen del itinerario (795x200 px) (Solo para el PDF)');
              $crud->field_type('servicios','set',array(
                  '<i class="icon-journey flaticon-transport-1"></i> <p class="text">Transportes</p>',
                  '<i class="icon-journey flaticon-suitcase"></i> <p class="text">Destinos Próximos</p>',
                  '<i class="icon-journey flaticon-drink"></i> <p class="text">Clima perfecto</p>',
                  '<i class="icon-journey flaticon-security"></i> <p class="text">Ocio y cultura</p>',
                  '<i class="icon-journey flaticon-people"></i> <p class="text">Accesible </p>',
                  '<i class="icon-journey flaticon-people-2"></i> <p class="text">Nos entenderemos!</p>'
             ));
            $crud->field_type('servicios','set',array(
                '<i class="icon-journey flaticon-transport-1"></i> <p class="text">Transportes</p>',
                '<i class="icon-journey flaticon-suitcase"></i> <p class="text">Destinos Próximos</p>',
                '<i class="icon-journey flaticon-drink"></i> <p class="text">Clima perfecto</p>',
                '<i class="icon-journey flaticon-security"></i> <p class="text">Ocio y cultura</p>',
                '<i class="icon-journey flaticon-people"></i> <p class="text">Accesible </p>',
                '<i class="icon-journey flaticon-people-2"></i> <p class="text">Nos entenderemos!</p>'
             ));
            
            $crud->field_type('transporte','enum',array(
                '<i class="icons fa fa-bus"></i><span class="text number">AUTOBUS</span>',
                '<i class="icons fa fa-plane"></i><span class="text number">AVIÓN</span>',
                '<i class="icons fa fa-ship"></i><span class="text number">BARCO</span>',
             ));
            $crud->callback_before_delete(function($id){
                get_instance()->db->delete('destinos_actividades',array('destinos_id'=>$id));
                get_instance()->db->delete('destinos_galeria',array('destinos_id'=>$id));
                get_instance()->db->delete('destinos_itinerario',array('destinos_id'=>$id));
                foreach(get_instance()->db->get_where('destinos_precios',array('destinos_id'=>$id))->result() as $d){
                    get_instance()->db->delete('destinos_precios_detalles',array('destinos_precios_id'=>$d->id));
                }
                get_instance()->db->delete('destinos_precios',array('destinos_id'=>$id));                
                get_instance()->db->delete('testimonios',array('destinos_id'=>$id));
                get_instance()->db->delete('wishlist',array('destinos_id'=>$id));
                get_instance()->db->delete('solicitudes',array('destinos_id'=>$id));
                
            });
            $crud->set_clone();
            $crud->add_action('<i class"fa fa-print"></i> PDF','',base_url('destinos/frontend/crear_pdf2/207').'/');
            $crud->add_action('<i class"fa fa-money"></i> Precios','',base_url('destinos/admin/precios').'/');
            $crud->add_action('<i class"fa fa-image"></i> Itinerario','',base_url('destinos/admin/destinos_itinerario').'/');
            $crud->add_action('<i class"fa fa-image"></i> Galeria','',base_url('destinos/admin/destinos_galeria').'/');
            $crud->add_action('<i class"fa fa-image"></i> Actividades','',base_url('destinos/admin/destinos_actividades').'/');
            $crud->add_action('<i class"fa fa-image"></i> Testiminios','',base_url('destinos/admin/testimonios').'/');
            //$crud->set_lang_string('insert_success_message','Su registro fue almacenado con éxito, <script>document.location.href="'.base_url('destinos/admin/precios').'/{id}/add'.'";</script>');
            $crud = $crud->render();
            $crud->title = 'Destinos';
            $this->loadView($crud);
        }
        
        public function testimonios($x = ''){            
            $crud = $this->crud_function('','');     
            $crud->set_subject('Testimonio');
            $crud->set_field_upload("foto","img/destinos");
            $crud->set_field_upload("portada","img/destinos");
            $crud->field_type('destinos_id','hidden',$x);
            $crud->where('destinos_id',$x);            
            //$crud->unset_delete();                        
            $crud = $crud->render();
            $crud->title = 'Testimonios';
            $this->loadView($crud);
        }
        
        public function precios($x = ''){
            $this->as['precios'] = 'destinos_precios';
            $crud = $this->crud_function('','');     
            $crud->set_subject('Precio');
            
            
            $crud->field_type('destinos_id','hidden',$x);
            $crud->where('destinos_id',$x);            
            //$crud->unset_delete();
            $crud->add_action('<i class"fa fa-money"></i> Detalles','',base_url('destinos/admin/destinos_precios_detalles').'/');
            $crud->set_lang_string('insert_success_message','Su registro fue almacenado con éxito, <script>document.location.href="'.base_url('destinos/admin/destinos_precios_detalles').'/{id}/add'.'";</script>');
            $crud = $crud->render();
            $crud->title = 'Precios';
            $this->loadView($crud);
        }
            
        function destinos_precios_detalles($x = ''){
            $crud = $this->crud_function('','');     
            $crud->set_subject('Detalle de precio');
            
            
            $crud->field_type('destinos_precios_id','hidden',$x);
            $crud->where('destinos_precios_id',$x);            
           //$crud->unset_delete();            
            $crud->set_lang_string('insert_success_message','Su registro fue almacenado con éxito, <a href="'.base_url('destinos/admin/destinos_itinerario').'/{id}/2/add">Adm. Itinerario</a> |');
            $crud = $crud->render();
            $crud->title = 'Detalles de precio de paquete';
            $this->loadView($crud);
        }
        
        function destinos_itinerario($x = '',$y = ''){
            $crud = $this->crud_function('','');     
            $crud->set_subject('Itinerario');
            if(is_numeric($y)){
                $this->db->select('destinos.id');
                $this->db->join('destinos_precios','destinos_precios.destinos_id = destinos.id');
                $this->db->join('destinos_precios_detalles','destinos_precios_detalles.destinos_precios_id = destinos_precios.id');
                $x = $this->db->get_where('destinos',array('destinos_precios_detalles.id'=>$x))->row()->id;
            }
            $crud->set_field_upload('foto','img/destinos');
             $crud->field_type('descripcion','editor',array('type'=>'textarea'));
                    
            $crud->field_type('destinos_id','hidden',$x);
            $crud->where('destinos_id',$x);            
            //$crud->unset_delete();            
            $crud->set_lang_string('insert_success_message','Su registro fue almacenado con éxito, <a href="'.base_url('destinos/admin/destinos_actividades').'/'.$x.'/add">Adm. Actividades</a> |');
            $crud = $crud->render();
            $crud->title = 'Itinerarios';
            $this->loadView($crud);
        }
        
        function destinos_actividades($x = ''){
            $crud = $this->crud_function('','');     
            $crud->set_subject('Actividad');                        
                    
            $crud->field_type('destinos_id','hidden',$x);
            $crud->where('destinos_id',$x);            
            //$crud->unset_delete();            
            $crud->set_lang_string('insert_success_message','Su registro fue almacenado con éxito, <a href="'.base_url('destinos/admin/destinos_galeria').'/'.$x.'">Adm. Galeria</a> |');
            $crud = $crud->render();
            $crud->title = 'Actividades';
            $this->loadView($crud);
        }
        
        function destinos_galeria($x = ''){
            $x = $this->db->get_where('destinos',array('id'=>$x))->row()->categorias_destinos_id;
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_relation_field('destinos_id')
                     ->set_table('destinos_galeria')
                     ->set_url_field('foto')
                     ->set_image_path('img/destinos')
                     ->set_subject('Galeria')
                     ->set_ordering_field('orden');
            $crud->module = 'destinos';
            $crud = $crud->render();
            $crud->title = 'Galeria del destino';
            $crud->output = '<p><a class="btn btn-success" href="'.base_url('destinos/admin/destinos/'.$x).'">Destinos</a></p>'.$crud->output;
            $this->loadView($crud);
        }
        
        function solicitudes($x = ''){
            $crud = $this->crud_function('','');     
            $crud->set_subject('Solicitud de presupuest');
            $crud->set_relation('destinos_id','destinos','destinos_nombre');
            $crud->order_by("solicitudes.id","DESC");
            //$crud->unset_columns('sesion');
            $crud->display_as('destinos_id','Destino')
                     ->display_as('fecha','Fecha de salida');
            $crud->callback_column('sf00eabc1',function($val,$row){return '<a href="'.base_url('destinos/'.  toURL($row->id.'-'.$val)).'" target="_new">'.$val.'</a>';});
            $crud->add_action('Ver PDF','',base_url('destinos/frontend/crear_pdf/').'/');
            //$crud->where('estado',1);
            $crud = $crud->render();
            $crud->title = 'Solicitudes de presupuestos';
            $this->loadView($crud);
        }
    }
?>
