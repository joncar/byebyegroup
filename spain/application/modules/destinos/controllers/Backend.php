<?php
    require_once APPPATH.'controllers/Panel.php';    
    class Backend extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function wishlist(){
            $crud = $this->crud_function('','');
            $crud->field_type('user_id','hidden',$this->user->id);
            $crud = $crud->render();
            $this->loadView($crud);
            
        }
    }
?>