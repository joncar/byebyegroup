<div id="wrapper-content">
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <!-- Page Title -->
        <section class="page-title page-banner" style="top: -143px; margin-bottom: -143px;">
            <div class="container">
                <div class="page-title-wrapper">
                    <div class="page-title-content">
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?= site_url() ?>" class="link home">Home</a>
                            </li>
                            <!--<li>
                                <a href="<?/*= site_url('destinos') */?>" class="link">Destinos</a>
                            </li>-->
                            <li class="active">
                                <a href="<?= site_url('favoritos') ?>" class="link">Pressupostos</a>
                            </li>
                        </ol>				
                        <div class="clearfix"></div>
                        <h2 class="captions">
                            els meus pressupots
                        </h2>
                    </div>
                </div>
            </div>
        </section>
        <!-- Content section -->
        <div class="section section-padding page-detail padding-top padding-bottom">
            <div class="container">
                <div class="row">
                    <div id="page-content" class="col-md-12 col-xs-12">
                        <div id="post-659" class="post-659 page type-page status-publish hentry">
                            <div class="section-page-content clearfix ">
                                <div class="entry-content">
                                    <div id="yith-wcwl-messages">
                                        <?= @$_SESSION['msj'] ?>
                                        <?php unset($_SESSION['msj']); ?>
                                    </div>
                                    <?= $output ?>
                                </div>
                            </div>                           
                            <div class="group-button">
                                <a href="<?= base_url('destinos/frontend/reservar') ?>" type="submit" class="btn btn-maincolor">Enviar</a>
                                <a href="<?= site_url('destinos') ?>" class="btn">Fer més pressupostos</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #section -->
    </div>
    <!-- MAIN CONTENT-->
</div>