<page id="pagina1">
    <div style="background:url(<?= base_url('img/pdf/bg1.jpg') ?>); width:797px; height:1122px;">
        <div style="position:absolute; top:640px; width:795px; text-align: center; font-size:30px; font-family: nunitosansbold; font-weight:bold;">VIAJE DE FINAL DE CURSO A <?= strtoupper($solicitud->destinos_nombre) ?></div>
        <div style="position:absolute; top:720px; width:795px; text-align: center; font-size:14px; font-family: nunitosans">REFERENCIA DEL PRESUPUESTO: <?= $solicitud->referencia ?></div>
        <div style="position: absolute; rotate:45; text-align: center; font-size: 28px; font-family: neomocon; top: 970px; font-weight: bold; color: white; width: 170px; left: 590px;">
            Beneficíate <br>de la Reserva Anticipada
        </div>
    </div>
</page>
<page id="pagina2">
    <div style="background:url(<?= base_url('img/pdf/bg44.jpg') ?>); width:795px; height:1120px; font-family: nunitosans">
        <div style="width:795px; height:300px;">            
            <div style="width:795px; height:190px; background:url(<?= $solicitud->imagen_itinerario ?>); background-position:center"></div>
            <div style="position:absolute; top: 150px; left:45px; font-family: nunitosans; color:white;">                
                <span style="font-family:neomocon; font-weight: bold; font-size: 32px; margin-top: 6px;"><?= $solicitud->destinos_nombre ?></span>
                <span style="font-size: 12px;  margin-left: 5px">DESDE</span>
                <span style="font-family:nunitosansbold; font-weight: bold; font-size: 32px;  color: #eb2c72;"><?= str_replace(',00','',$solicitud->precio) ?>&euro;</span>
            </div>

            <div style="margin-top:20px; font-size:12px; padding:30px">
                <table>
                    <?php foreach ($solicitud->itinerario->result() as $p): ?>
                    <tr>                                                    
                            <td style="width: 50px; padding:5px;">
                                <div style="width:2px; height:100%; position: absolute; left:30px; top:0px;"></div>
                                <span style="background-color: #ffdd00;"><?= $p->etiqueta ?></span>
                            </td>
                            <td style="width: 380px; padding:5px;">
                                <span style="font-family:nunitosansbold; font-weight: bold"><?= $p->lugar ?></span><br><br/>
                                <span><?= $p->descripcion ?></span>                        
                            </td>
                            <td style="width: 530px; vertical-align:middle;  padding:5px;">
                                <img style="width:220px;" alt="" src="<?= base_url() ?>img/destinos/<?= $p->foto ?>">
                            </td>
                    </tr>
                    <?php endforeach ?>
                </table>
            </div>
        </div>
    </div>
</page>
<page id="pagina3">
    <div style="background:url(<?= base_url() ?>img/pdf/bg3.jpg); width:797px; height:1122px; font-family: nunitosans">
        <div style="position: absolute; width: 795px; text-align: left; font-size: 68px; top: 30px; left: 45px; font-family: neomocon;"><?= $solicitud->destinos_nombre ?></div>
        <div style="position: absolute; width: 795px; text-align: left; font-size: 18px; left: 45px; top: 90px;"><?= $solicitud->disponibilidad ?></div>
        <div style="width: 795px; position: absolute; top: 180px; left: 45px;">FECHA DEL PRESUPUESTO:<span style="font-family:nunitosansbold; font-size: 14px"> <?= date("d/m/Y") ?></span></div>

        <div style="position: absolute; width: 225px; top: 165px; left: 537px; text-decoration: underline; font-size: 15px; font-family: nunitosansbold;">DATOS DE TU ASESOR</div>
        
        
        <div style="top: 190px; width: 410px; left: 525px; position: relative;">

            <img style="width: 60px; position: absolute; top: 10px; left: 0px;" src="<?= $solicitud->asesorfoto ?>">
            <div style="position: absolute; left: 65px; top: 10px; font-size: 12px;">
                
                <span style="font-family:nunitosansbold;">Anabel Mateos</span><br>
                Nacional<br>
                693 806 251<br>
                a.mateos@byebyegroup.com
                <br>
                
            </div>
        </div>
        

        <div style="width: 795px; position: absolute; top: 160px; left: 45px;">REFERENCIA DEL PRESUPUESTO: <span style="font-family:nunitosansbold; font-size: 14px"> <?= $solicitud->referencia ?></span></div>
        
        <div style="width: 795px; position: absolute; font-size: 15px; top: 246px; left: 45px; font-family: nunitosansbold; text-decoration: underline;">DATOS DEL CLIENTE</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 270px; left: 45px;">Instituto</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 270px; left: 190px; font-family: nunitosansbold"><?= $solicitud->instituto ?></div>
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 292px;">Localidad</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 295px; left: 190px; font-family: nunitosansbold"><?= $solicitud->localidad ?></div>
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 316px;">Nivel del curso</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 319px; left: 190px; font-family: nunitosansbold"><?= $solicitud->curso ?></div>
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 340px;">Nombre</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 343px; left: 190px; font-family: nunitosansbold"><?= $solicitud->nombre ?></div>
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 366px;">Email</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 369px; left: 190px; font-family: nunitosansbold"><?= $solicitud->email ?></div>
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 392px;">Teléfono</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 395px; left: 190px; font-family: nunitosansbold"><?= $solicitud->telefono ?></div>
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 420px;">Cargo</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 423px; left: 190px; font-family: nunitosansbold"><?= $solicitud->cargo ?></div>
        
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 450px;">Comentario</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 453px; left: 190px; font-family: nunitosansbold"><?= $solicitud->comentario ?></div>
        
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 590px; font-family: nunitosansbold; text-decoration: underline;">Datos de tu viaje</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 612px; left: 45px;">Ciudad de salida</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 615px; left: 190px; font-family: nunitosansbold"><?= $solicitud->bus->ciudad ?></div>

        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 638px;">Circuito de destino</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 641px; left: 190px; font-family: nunitosansbold"><?= $solicitud->destinos_nombre ?></div>
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 666px;">Fecha de salida</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 669px; left: 190px; font-family: nunitosansbold"><?= $solicitud->fecha ?></div>
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 691px;">Nº Estudiantes</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 694px; left: 190px; font-family: nunitosansbold"><?= $solicitud->alumnos ?></div>
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 719px;">Nª Profesores</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 721px; left: 190px; font-family: nunitosansbold"><?= $solicitud->profesores ?></div>
        <div style="position:absolute; left:31px; top:670px; width:498px; height:422px">
            <img src="<?= base_url() ?>img/pdf/icon1.png" style="position: absolute; top: 130px; width: 52px;">
            <img src="<?= base_url() ?>img/pdf/icon2.png" style="position: absolute; width: 48px; top: 176px;">
            <img src="<?= base_url() ?>img/pdf/icon3.png" style="position: absolute; width: 52px; top: 220px;">
            <img src="<?= base_url() ?>img/pdf/icon4.png" style="position: absolute; width: 52px; top: 268px;">
            <img src="<?= base_url() ?>img/pdf/icon5.png" style="position: absolute; width: 52px; top: 320px;">
            <img src="<?= base_url() ?>img/pdf/icon6.png" style="position: absolute; width: 52px; top: 365px;">
        </div>
        <div style="position: absolute; left: 31px; top: 765px; width: 500px; height: 422px;">
            <div style="margin: 15px 30px 30px 65px;">
                <div style="font-family:nunitosansbold; text-decoration: underline;font-size: 18px">SERVICIOS INCLUIDOS</div><br>
                <div style="font-family:nunitosans;  font-size: 11px">También le recordamos todos los servicios que están incluidos en el presupuesto: <br><br>
            • Autocar durante todo el recorrido con recogida y regreso. <br><br>

            • Alojamiento en la categoría y régimen seleccionado durante su estancia. <br><br>

            • Distribución en habitaciones múltiples, antes de la salida haremos el rooming-list. <br><br>

            • Seguro de asistencia en viaje y responsabilidad civil (RC). <br><br>

            • Gratuidades para los profesores acompañantes según ratio. <br><br>

            • Visitas y entradas incluidas inidicadas en cada itinerario. <br><br>

            • Monitores para la coordinación y realización de actividades detalladas. <br><br>

            • Obsequios para todos los estudiantes participantes, se entregará al inicio I del viaje. <br><br>

            • Asesoramiento y gestión del circuito por parte de nuestra Agencia de Viajes. <br><br>

            • 1 Talonario gratis por cada estudiante para financiarse 200 € del viaje.
                    <?php foreach(explode(',',$solicitud->servicios_pdf) as $s): ?>
                        <div>
                            <?= $s ?>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</page>





<page id="pagina4">
    <div style="background:url(<?= base_url() ?>img/pdf/bg9.jpg); width:795px; height:1120px; font-family: nunitosans">
        <div style="position: absolute; width: 795px; text-align: left; font-size: 48px; top: 30px; left: 45px; font-family: neomocon;"><?= $solicitud->destinos_nombre ?></div>
        <div style="position: absolute; width: 795px; text-align: left; font-size: 18px; left: 45px; top: 90px;"><?= $solicitud->disponibilidad ?></div>
        <div style="margin-top: 250px; padding-left:45px; ">
            <span style="font-family:nunitosansbold">Precio de trayecto <?= $solicitud->bus->ciudad ?>-<?= $solicitud->destinos_nombre ?>:</span>
            <span style="width: 795px; text-align: left; font-size: 28px">
                <?= is_numeric($solicitud->bus->precio)?str_replace(',00','',number_format($solicitud->bus->precio, 2, ',', '.')).' &euro;':$solicitud->bus->precio ?>
                <?php if(!is_numeric($solicitud->bus->precio)){$solicitud->bus->precio = 0;} ?>
            </span>
        </div><br><br>
        <div style="padding-left: 45px; margin-bottom: 10px; font-family: nunitosansbold; font-size: 24px;"><u>ACTIVIDADES INCLUIDAS</u></div>
        <div style="padding-left:45px; ">
            <table style="font-size:13px">
                <tbody>
                    <tr style="background:#ffdd00">
                        <th style="padding: 5px; text-align:center; width:200px">Actividades</th>
                        <th style="padding: 5px; text-align:center">Precio</th>
                        <th style="padding: 5px; text-align:center">Transporte</th>
                        <th style="padding: 5px; text-align:center">Duración</th>
                        <th style="padding: 5px; text-align:center">Seguro</th>
                        <th style="padding: 5px; text-align:center">Guia</th>
                        <th style="padding: 5px; text-align:center">Menú</th>
                        <th style="padding: 5px; text-align:center">Monitores</th>
                    </tr>
                    <?php
                    $extras = 0;
                    foreach (explode('//', $solicitud->actividades)as $e):
                        @list($id,$name) = @explode("--",$e,2);
                        foreach ($this->db->get_where('destinos_actividades', array('id'=>$id,'precio <'=>0))->result() as $a):
                            $a->transporte = $a->transporte ? 'SI' : 'NO';
                            $a->duracion = $a->duracion ? 'SI' : 'NO';
                            $a->seguro = $a->seguro ? 'SI' : 'NO';
                            $a->guia = $a->guia ? 'SI' : 'NO';
                            $a->menu = $a->menu ? 'SI' : 'NO';
                            $a->monitores = $a->monitores ? 'SI' : 'NO';
                            ?>
                            <tr>
                                <td style="padding: 5px; border-top:1px solid lightgray; word-wrap:break-word;"><?= wordwrap($a->actividad,30,'<br/>') ?></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"> <span>Incluida</span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->transporte ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->duracion ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->seguro ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->guia ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->menu ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->monitores ?><span class="hide"></span></td>
                            </tr>
                        <?php endforeach ?>
                        <?php 
                            $actividad = $this->db->get_where('destinos_actividades', array('id'=>$id,'precio >='=>0));
                            if($actividad->num_rows()>0){
                                $extras+= $actividad->row()->precio;
                            }
                            
                        ?>
                            
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>        
        <div style="padding-left:45px; ">
            <span style="font-family:nunitosansbold">PRECIO DEL ITINERARIO BASE:</span>
            <span style="width: 795px; text-align: left; font-size: 28px"><?= str_replace(",00","",number_format($solicitud->preci-$extras,2,',','.')); ?> &euro;</span>
        </div>
        <div style="padding-left: 45px; margin-top: 51px; margin-bottom: 10px; font-family: nunitosansbold; font-size: 24px;"><u>ACTIVIDADES EXTRAS SELECCIONADAS</u></div>
        <div style="padding-left:45px; ">
            <table style="font-size:13px">
                <tbody>
                    <tr style="background:#ffdd00">
                        <th style="padding: 5px; text-align:center; width:200px">Actividades</th>
                        <th style="padding: 5px; text-align:center">Precio</th>
                        <th style="padding: 5px; text-align:center">Transporte</th>
                        <th style="padding: 5px; text-align:center">Duración</th>
                        <th style="padding: 5px; text-align:center">Seguro</th>
                        <th style="padding: 5px; text-align:center">Guia</th>
                        <th style="padding: 5px; text-align:center">Menú</th>
                        <th style="padding: 5px; text-align:center">Monitores</th>
                    </tr>
<?php
$total = 0;
$totali = str_replace(",00","",number_format($total,2,",","."));
foreach (explode('//', $solicitud->actividades)as $e):
    @list($id,$name) = @explode("--",$e,2);
    foreach ($this->db->get_where('destinos_actividades', array('id'=>$id, 'precio >=' => 0))->result() as $a):
        $a->transporte = $a->transporte ? 'SI' : 'NO';
        $a->duracion = $a->duracion ? 'SI' : 'NO';
        $a->seguro = $a->seguro ? 'SI' : 'NO';
        $a->guia = $a->guia ? 'SI' : 'NO';
        $a->menu = $a->menu ? 'SI' : 'NO';
        $a->monitores = $a->monitores ? 'SI' : 'NO';
        $total += $a->precio;
        $totali = str_replace(",00","",number_format($total,2,",","."));
        ?>
                            <tr>
                                <td style="padding: 5px; border-top:1px solid lightgray;"><?= wordwrap($a->actividad,30,'<br/>') ?></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"> <span><?= str_replace(',00','',number_format($a->precio, 2, ',', '.')) ?></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->transporte ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->duracion ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->seguro ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->guia ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->menu ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->monitores ?><span class="hide"></span></td>
                            </tr>
    <?php endforeach ?>
<?php endforeach ?>
                </tbody>
            </table>
        </div>
        <div style="padding-left:45px; ">
            <span style="font-family:nunitosansbold">PRECIO ACTIVIDADES EXTRAS:</span>
            <span style="width: 795px; text-align: left; font-size: 28px"><?= str_replace(',00','',$totali) ?> &euro;</span>
        </div>

        <div style="top: 70px; width: 410px; left: 525px; position: absolute;">

            <img style="width: 60px; position: absolute; top: 10px; left: 0px;" src="<?= $solicitud->asesorfoto ?>">
            <div style="position: absolute; left: 65px; top: 10px; font-size: 12px;">
                <span style="font-family:nunitosansbold;">Anabel Mateos</span><br>
                Nacional<br>
                693 806 251<br>
                a.mateos@byebyegroup.com
                <br>
            </div>
        </div>
    </div>
</page>


<page id="pagina5">
    <div style="background:url(<?= base_url() ?>img/pdf/bg8.jpg); width:795px; height:1120px; font-family: nunitosans">
        <div style="position: absolute; width: 795px; text-align: left; font-size: 48px; top: 30px; left: 45px; font-family: neomocon;"><?= $solicitud->destinos_nombre ?></div>
        <div style="position: absolute; width: 795px; text-align: left; font-size: 18px; left: 45px; top: 90px;"><?= $solicitud->disponibilidad ?></div>


        <div style="margin-top: 290px; margin-left:45px; margin-right: 45px; font-family: nunitosansbold; font-size: 24px;">
            <u>PRECIO FINAL DEL VIAJE Y MÉTODO DE RESERVA</u>
        </div>
        <div style="margin-top: 20px; margin-left:45px; margin-right: 45px;">
<?php $total = $solicitud->preci + $solicitud->bus->precio ?>
            <div><span style="font-family:nunitosans">Total Nº de alumnos:</span> <span style="font-family:nunitosansbold"> <?= $solicitud->alumnos ?></span></div>
            <div><span style="font-family:nunitosans">Total Nº de profesores/acompañantes:</span> <span style="font-family:nunitosansbold"> <?= $solicitud->profesores ?></span></div><br>
            <div><span style="font-family:nunitosans">Fecha 1r pago:</span> <span style="font-family:nunitosansbold">Reserva de plazas y bloqueo del circuito</span></div>
            <div><span style="font-family:nunitosans">Fecha 2º pago: </span> <span style="font-family:nunitosansbold">30 dias antes de la salida </span></div><br/>
            <div style="background:#ffdd00;  border: 10px solid #ffdd00; width: auto"><span style="font-family:nunitosansbold; font-size:24px;">PVP TOTAL por alumno <?= str_replace(',00','',$total) ?> &euro;</span></div>
            <br/>
            <div>
                <span style="font-family:nunitosans">Primer pago (10%): </span><span style="font-family:nunitosansbold"><?= str_replace(',00','',number_format(($total * 10) / 100, 2, ',', '.')) ?> &euro;</span><br/>
                <span style="font-family:nunitosans">Segundo pago (90%):</span>  <span style="font-family:nunitosansbold"><?= str_replace(',00','',number_format(($total * 90) / 100, 2, ',', '.')) ?> &euro;</span>
            </div>
            <br/>
<?php $total = $total * $solicitud->alumnos ?>
            <div><span style="font-family:nunitosans;text-decoration: underline;">PVP TOTAL por grupo</span><span style="font-family:nunitosansbold; text-decoration: underline;"> <?= number_format($total, 2, ',', '.') ?> &euro;</span></div><br/>
            <div>
                <span style="font-family:nunitosans">Primer pago (10%):</span> <span style="font-family:nunitosansbold"><?= str_replace(',00','',number_format(($total * 10) / 100, 2, ',', '.')) ?> &euro;</span><br/>
                <span style="font-family:nunitosans">Segundo pago (90%):</span> <span style="font-family:nunitosansbold"><?= str_replace(',00','',number_format(($total * 90) / 100, 2, ',', '.')) ?> &euro;</span></div>
        </div>
        <div style="margin-top: 60px; margin-left:45px; margin-right: 45px;">
            Los centros pueden hacer el pago mediante transferéncia bancaria o en nuestras oficinas, sólo es necesario indicar en el concepto el número de presupuesto y el nombre del centro.
        </div>
        <div style="top: 70px; width: 410px; left: 525px; position: absolute;">

            <img style="width: 60px; position: absolute; top: 10px; left: 0px;" src="<?= $solicitud->asesorfoto ?>">
            <div style="position: absolute; left: 65px; top: 10px; font-size: 12px;">
                <span style="font-family:nunitosansbold;">Anabel Mateos</span><br>
                Nacional<br>
                693 806 251<br>
                a.mateos@byebyegroup.com
                <br>
            </div>
        </div>
    </div>
</page>


<page id="pagina6">
    <div style="background:url(<?= base_url() ?>img/pdf/bg9.jpg); width:795px; height:1120px; font-family: nunitosans">
        <div style="position: absolute; width: 795px; text-align: left; font-size: 48px; top: 30px; left: 45px; font-family: neomocon;"><?= $solicitud->destinos_nombre ?></div>
        <div style="position: absolute; width: 795px; text-align: left; font-size: 18px; left: 45px; top: 90px;"><?= $solicitud->disponibilidad ?></div>
        <div style="padding-left:45px;  margin-top:161px; margin-bottom: 40px; font-size: 18px"><u>ACTIVIDADES EXTRAS</u></div>
        <div style="padding-left:45px; ">
            <table style="font-size:13px">
                <tbody>
                    <tr style="background:#ffdd00">
                        <th style="padding: 5px; text-align:center">Actividades</th>
                        <th style="padding: 5px; text-align:center">Precio</th>
                        <th style="padding: 5px; text-align:center">Transporte</th>
                        <th style="padding: 5px; text-align:center">Duración</th>
                        <th style="padding: 5px; text-align:center">Seguro</th>
                        <th style="padding: 5px; text-align:center">Guia</th>
                        <th style="padding: 5px; text-align:center">Menú</th>
                        <th style="padding: 5px; text-align:center">Monitores</th>
                    </tr>
<?php
foreach ($this->db->get_where('destinos_actividades', array('destinos_id' => $solicitud->destinos_id, 'precio >=' => 0))->result() as $a):
    $a->transporte = $a->transporte ? 'SI' : 'NO';
    $a->duracion = $a->duracion ? 'SI' : 'NO';
    $a->seguro = $a->seguro ? 'SI' : 'NO';
    $a->guia = $a->guia ? 'SI' : 'NO';
    $a->menu = $a->menu ? 'SI' : 'NO';
    $a->monitores = $a->monitores ? 'SI' : 'NO';
    ?>
                        <tr>
                            <td style=" border-top:1px solid lightgray; "><?= wordwrap($a->actividad,30,'<br/>') ?></td>
                            <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"> <span><?= str_replace(',00','',number_format($a->precio, 2, ',', '.')) ?></span></td>
                            <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->transporte ?><span class="hide"></span></td>
                            <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->duracion ?><span class="hide"></span></td>
                            <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->seguro ?><span class="hide"></span></td>
                            <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->guia ?><span class="hide"></span></td>
                            <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->menu ?><span class="hide"></span></td>
                            <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->monitores ?><span class="hide"></span></td>
                        </tr>
<?php endforeach ?>
                </tbody>
            </table>
        </div>
        <div style="top: 70px; width: 410px; left: 525px; position: absolute;">

            <img style="width: 60px; position: absolute; top: 10px; left: 0px;" src="<?= $solicitud->asesorfoto ?>">
            <div style="position: absolute; left: 75px; top: 10px; font-size: 12px;">
                <span style="font-family:nunitosansbold;">Anabel Mateos</span><br>
                Nacional<br>
                693 806 251<br>
                a.mateos@byebyegroup.com
                <br>
            </div>
        </div>
    </div>
</page>

<page id="pagina7">
    <div style="background:url(<?= base_url() ?>img/pdf/bg6.jpg); width:795px; height:1120px;">
        <div style="position: absolute; width: 795px; text-align: left; font-size: 48px; top: 30px; left: 45px; font-family: neomocon;"><?= $solicitud->destinos_nombre ?></div>
        <div style="position: absolute; width: 795px; text-align: left; font-size: 18px; left: 45px; top: 90px;"><?= $solicitud->disponibilidad ?></div>
        <div style="padding-left:45px; margin-top:161px; margin-bottom: 40px; font-size: 18px; ">CONDICIONES GENERALES</div>
        <div style="margin-left:45px; margin-right:240px;font-size: 11px">
<!--            <span style="font-family:nunitosansbold; text-decoration: underline;"><b>ORGANITZACIÓN</b></span><br/><br/>
            L’organització tècnica dels viatges ha sigut realitzada per Finalia Viajes S.L. (ByeBye Group), agència de viatges minorista majorista amb CIF B65847170, domicili al C/ Girona, 34, 08700 d’Igualada (Barcelona) i amb títol-llicència número GC-002578 atorgat per la Generalitat de Catalunya.<br/><br/>-->
            <span style="font-family:nunitosansbold; text-decoration: underline;"><b>PAGO I RESERVA </b></span><br/><br/>
            Al realizar la reserva se abonará un 10% del presupuesto final y el resto se abonará 30 días antes de la salida del grupo. La confirmación de la reserva se efectuará en el momento de la entrega de los bonos y documentación final del viaje que conforman el contrato. En caso de que el viaje sea internacional la reserva será de un 40%, ya que ya que se procederá a la reserva y pago de los vuelos.
            <br/><br/>
            <span style="font-family:nunitosansbold; text-decoration: underline;"><b>PRECIO</b></span><br/><br/>
            El precio del viaje combinado ha sido calculado según los tipos de cambio, tarifas de transporte, costes del carburante y tasas de impuestos aplicables al momento de la consulta y confirmación de la reserva. Cualquier variación de los precios de los citados elementos, podrá dar lugar a la revisión del precio final del viaje.
            <br><br>
            NUESTROS PRECIOS INCLUYEN
            <br>
            1. El transporte de ida y vuelta (cuando este servicio está incluido en el contrato y según sus especificaciones).
            <br>
            2. Alojamiento en habitaciones múltiples para los estudiantes (habitaciones individuales para los acompañantes, siempre que haya disponibilidad y en la medida de lo posible) y pensión alimentaria según el régimen contratado (SA Sólo alojamiento - AD Alojamiento y desayuno - MP Media pensión - PC Pensión completa - SP Según programa).
            <br>
            3. Asistencia durante el viaje.
            <br>
            4. Todos aquellos servicios y complementos especificados concretamente en los itinerarios correspondientes.
            <br>
            5. Los impuestos indirectos (IVA, I.G.I.C.) cuando éstos sean aplicables. Los precios establecidos en el itinerario se han calculado en base a los impuestos y IVA / I.G.I. C. vigentes a 15/08/2012, y cualquier modificación de los mismos será repercutida al precio final del viaje.
            <br><br>
            NUESTROS PRECIOS NO INCLUYEN
            <br>
            Vuelos, tasas de aeropuertos, tasas de hoteles, certificados de vacunación, extras en los hoteles, regímenes alimenticios especiales, servicios de lavandería, servicios opcionales de los alojamientos (alquiler de tv...) y, en general, cualquier otro servicio que no especifique expresamente como incluido.
            <br/><br/>
            <span style="font-family:nunitosansbold; text-decoration: underline;"><b>NOTAS IMPORTANTES SOBRE LOS ALOJAMIENTOS
 </b></span><br/><br/>
            Al realizar la reserva es imprescindible hacer la declaración correcta del número de personas que se alojarán en el establecimiento, siendo esta responsabilidad del cliente. A la llegada se abonará la correspondiente fianza para responder a los desperfectos que se puedan ocasionar. El horario habitual de entrada y salida de los alojamientos es en función del primer y último servicio que el usuario utilice. Como norma general, las habitaciones podrán ser utilizadas a partir de las 12 h del día de llegada y deberán quedar libres antes de las 12 h. del día de salida, incluso durante su manipulación en los traslados. En caso de ocurrir alguna de estas circunstancias, recomendamos presentar la pertinente reclamación, el momento, a la compañía de transportes.
            <br/><br/>
            <span style="font-family:nunitosansbold; text-decoration: underline;"><b>TRANSPORTES
</b></span><br/><br/>
            El medio de transporte durante nuestro trayecto tendremos que mantenerlo cuidado.

            <br/><br/>
            <span style="font-family:nunitosansbold; text-decoration: underline;"><b>DOCUMENTACIÓN
</b></span><br/><br/>
            La documentación que es necesaria llevar por los viajes de estudiantes es el DNI en vigor o PASAPORTE en vigor, y recomendable la Tarjeta Sanitaria. En cuanto a los viajes internacionales, será necesaria también la autorización paternal en caso de que el estudiante sea menor de edad.
            <br/><br/>
            <span style="font-family:nunitosansbold; text-decoration: underline;"><b>INFORMACIÓN MÉDICA
</b></span><br/><br/>
            Si el estudiante que va a realizar el viaje de fin de curso necesita cuidados de salud especiales, deberán ser notificadas con antelación a nuestra empresa para poder gestionarlas. Deben comunicarlo a profesores y / o padres acompañantes, responsables del alojamiento elegido y coordinador de actividades del destino del viaje de fin de curso.
            <br/><br/>
            <span style="font-family:nunitosansbold; text-decoration: underline;"><b>PROTECCIÓN DE DATOS DE CARÁCTER PERSONAL
</b></span><br/><br/>
            Conforme a la Ley Orgánica 15/1999, de 13 de diciembre, sobre la Protección de Datos de Carácter Personal, los datos facilitados por el usuario quedarán incorporados automáticamente en un fichero, propiedad BYE BYE GROUP el cual será procesado con el fin de poder proporcionar los servicios solicitados. El usuario autoriza a BYE BYE GROUP para incluir sus datos personales en los respectivos ficheros, así como su utilización para la gestión y registro de las operaciones suscritas entre ambas partes.
            <br/><br/>


        </div>
    </div>
</page>
<page id="pagina8">
    <div style="background:url(<?= base_url() ?>img/pdf/bg7.jpg); width:797px; height:1122px;">
      <!--  <div style="position: absolute; left: 520px; top: 304px;">@byebyegroup</div>
        <div style="position: absolute; left: 520px; top: 354px;">@byebyegroup</div>
        <div style="position: absolute; left: 520px; top: 404px;">@byebyegroup</div>-->
    </div>
</page>
