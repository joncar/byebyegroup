<!-- WRAPPER-->
<div id="wrapper-content">
    <form class="customer-info" action="" onsubmit="return send(this)">
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <section class="page-banner tour-view" style="background:url(<?= $detail->portada; ?>); background-size:cover">
            <div class="patterndestino"></div>
            <div class="container">
                <div class="page-title-wrapper">
                    <div class="page-title-content">
                        <ol class="breadcrumb">
                            <li><a href="<?= site_url() ?>" class="link home">Home</a></li>
                            <li><a href="<?= site_url("destinos/pack/".$detail->categorias_destinos_id) ?>" class="link"><?= $detail->categoria ?></a></li>
                            <li class="active"><a href="#" class="link"><?= $detail->destinos_nombre ?></a></li>
                        </ol>
                        <div class="clearfix"></div>
                        <h2 class="captions"><?= $detail->destinos_nombre ?></h2>
<!--                        <div class="price"><span class="text">des de</span><span class="number">--><?//= str_replace(",00","",number_format($detail->precio,2,',','.')) ?><!--</span><sup class="unit">€</sup></div>-->
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="tour-view-main padding-top padding-bottom">
                <div class="container">
                    <div class="tours-layout">
                        <div class="content-wrapper">
                            <ul class="list-info list-inline list-unstyle">
                                <li class="share">
                                    <a href="javascript:void(0);" class="link">
                                        <i class="icons fa fa-share-alt"></i>
                                    </a>
                                    <ul class="share-social-list">
                                        <li>
                                            <a href="http://www.facebook.com/sharer.php?u=<?= base_url("destinos/".toUrl($detail->id.'-'.$detail->destinos_nombre)) ?>&amp;picture=<?= base_url("img/destinos/".$detail->portada) ?>" onclick="window.open(this.href, 'Share Window','left=50,top=50,width=600,height=350,toolbar=0');; return false;" class="link-social">
                                                <i class="icons fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/intent/tweet?text=<?= $detail->destinos_nombre ?>;url=<?= base_url("destinos/".toUrl($detail->id.'-'.$detail->destinos_nombre)) ?>&amp;via=<?= $detail->destinos_nombre ?>" onclick="window.open(this.href, 'Share Window','left=50,top=50,width=600,height=350,toolbar=0');; return false;" class="link-social">
                                                <i class="icons fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="http://plus.google.com/share?url=<?= base_url("destinos/".toUrl($detail->id.'-'.$detail->destinos_nombre)) ?>" onclick="window.open(this.href, 'Share Window','left=50,top=50,width=600,height=350,toolbar=0');; return false;" class="link-social">
                                                <i class="icons fa fa-google-plus"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>                    
                    <h2 class="title-style-2"><?= $detail->disponibilidad ?> / <?= $detail->transporte ?>
                        <?php if ($detail->lowcost=="Actiu" || $detail->lowcost==1): ?>
                            <img src="<?= base_url() ?>img/tours/lowcost.svg" alt="" class="img-responsive destinoicono">
                        <?php elseif ($detail->lowcost==2): ?>                                
                            <img src="<?= base_url() ?>img/tours/viajeestrella.svg" alt="" class="img-responsive destinoicono">
                        <?php endif ?>
                    </h2>
                    <?php if(/*$detail->mostrar_precios*/ 1==0): ?>
                        <?php foreach($detail->precios->result() as $p): ?>
                            <div class="schedule-block">
                                <div class="element">
                                    <p class="schedule-title">Dies</p><span class="schedule-content">de <?= date("d/m/Y",strtotime($p->fecha_desde)) ?> al <?= date("d/m/Y",strtotime($p->fecha_hasta)) ?></span></div>
                                    <?php foreach($p->detalle->result() as $pd): ?>
                                        <div class="element">
                                            <p class="schedule-title"><?= $pd->alumnos ?></p><span class="schedule-content"><?= str_replace(",00","",number_format($pd->precio,2,',','.')) ?>€</span>
                                        </div>
                                    <?php endforeach ?>                            
                            </div>
                            <br>
                        <?php endforeach ?>                   
                        <p class="description">*Precio sujeto según disponibilidad de plazas.  ** Para ver suplementos de bus según zona.</p>
                    <?php endif ?>
                    <div class="wrapper-btn margin-top70" style=" margin-top: 39px">
                        <div onclick="iraform()" class="btn btn-maincolor btn-book-tour" style="font-weight: 600;">HAZ TU PRESUPUESTO Y CIRCUITO A MEDIDA</div>
                    </div>
                    <div class="joury-block margin-top70">
                        <h3 class="title-style-3">Qué ofrecemos</h3>
                        <div class="wrapper-journey">
                            <?php foreach(explode(',',$detail->servicios) as $s): ?>
                                <div class="item feature-item">
                                    <?= $s ?>
                                </div>
                            <?php endforeach ?>
                        </div>
                        <div class="overview-block clearfix">
                            <h3 class="title-style-3">Planning del viaje</h3>
                            <div class="timeline-container">
                                <div class="timeline">                                    
                                    <?php foreach($detail->itinerario->result() as $p): ?>
                                        <div class="timeline-block">
                                            <div class="timeline-title"> <span><?= $p->etiqueta ?></span></div>
                                            <div class="timeline-content medium-margin-top">
                                                <div class="row">
                                                    <div class="timeline-point"><i class="fa fa-circle-o"></i></div>
                                                    <div class="timeline-custom-col content-col">
                                                        <div class="timeline-location-block">
                                                            <p class="location-name"><?= $p->lugar ?><i class="fa fa-map-marker icon-marker"></i></p>
                                                            <p class="description"><?= $p->descripcion ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="timeline-custom-col image-col">
                                                        <div class="timeline-image-block">
                                                            <img src="<?= base_url() ?>img/destinos/<?= $p->foto ?>" alt="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach ?>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                            <div class="wrapper-btn margin-top70" id="presupuesto">
                                <a href="#" class="btn btn-maincolor btn-book-tour" style="font-weight: 600;">HAZ TU PRESUPUESTO Y CIRCUITO A MEDIDA</a>
                            </div>
                            <?php $this->load->view('frontend/_presupuesto'); ?>
                        </div>
                    </div>
                </div>
                <div class="gallery-block margin-top70">
                    <div class="container">
                        <h3 class="title-style-2">Galeria</h3>
                        <div class="grid">
                            <div class="grid-sizer"></div>
                            <div class="gutter-sizer"></div>
                            <?php foreach($detail->galeria->result() as $p): ?>
                                <div class="grid-item img-small pdr">
                                    <div class="gallery-image">
                                        <a href="<?= base_url() ?>img/destinos/<?= $p->foto ?>" data-fancybox-group="gallery" class="title-hover dh-overlay fancybox">
                                            <i class="icons fa fa-eye"></i>
                                        </a>
                                        <div class="bg"></div>
                                    </div>
                                </div>
                            <?php endforeach ?>                                    
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="expert-block padding-top">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <h3 class="title-style-2">CONTACTA con nuestra experta</h3>
                                <p>Estamos aquí para charlar sobre su próxima gran idea.</p>
                                <ul class="nav list-time">
                                    <li><span class="time"><i class="fa fa-long-arrow-right"></i>8 - 15 pm De lunes a viernes.</span></li>
                                    <!--                                    <li><span class="time"><i class="fa fa-long-arrow-right"></i>7 am to 1 pm Fridays (US central) toll free or by skype.</span></li>-->
                                </ul>
                                
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="about-us-wrapper">
                                    <div class="avatar">
                                        <div class="image-wrapper"><img src="<?= base_url() ?>img/homepage/avatar-contact-2.jpg" alt="" class="img img-responsive"></div>
                                        <div class="content-wrapper">
                                            <p class="name">ANABEL MATEOS MIjARES</p>
                                            <p class="position">Controller Administrativa</p>
                                        </div>
                                    </div>
                                    <div class="row group-list contact-list">
                                        <div class="col-sm-4 col-xs-4 col-contact">
                                            <div class="media contact-list-media">
                                                <div class="media-left"><a href="tel:693806251" class="icons"><i class="fa fa-whatsapp"></i></a></div>
                                                <div class="media-right"><a href="tel:693806251" class="title">Whatsapp</a>
                                                    <p class="text">693 806 251</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-4 col-contact">
                                            <div class="media contact-list-media">
                                                <div class="media-left"><a href="tel:902002068" class="icons"><i class="fa fa-phone"></i></a></div>
                                                <div class="media-right"><a href="tel:902002068" class="title">Telèfon</a>
                                                    <p class="text">902 002 068 ext 309</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 col-xs-4 col-contact">
                                            <div class="media contact-list-media">
                                                <div class="media-left"><a href="#" class="icons"><i class="fa fa-envelope-o"></i></a></div>
                                                <div class="media-right"><a href="mailto:
a.mateos@byebyegroup.com
" class="title">Email</a><p class="text">a.mateos@byebyegroup.com
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>

                        </div>

                    </div>
                </div>

        <section class="travelers"  style="background:url(<?= base_url() ?>img/background/bg-section-traveler_2.jpg); background-position: center bottom; background-repeat: repeat; background-size: cover;">
            <div class="container" style=" margin-top: 80px">
                <div class="row">
                    <div class="col-md-4">
                        <div class="traveler-wrapper padding-top padding-bottom">
                            <div class="group-title white">
                                <div class="sub-title">
                                    <p class="text">Recomendaciones de</p><i class="icons flaticon-people-2"style=" color: #e7237e"></i></div>
                                <h2 class="main-title">Bye Bye Group</h2></div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="traveler-list">
                            <?php foreach($testimonios->result() as $p): ?>
                                <div class="traveler">
                                    <div class="cover-image"><img src="<?= base_url("img/destinos/".$p->portada) ?>" alt=""></div>
                                    <div class="wrapper-content">
                                        
                                        <p class="name"><?= $p->autor ?></p>
                                        <p class="description"><?= $p->mensaje ?></p>
                                    </div>
                                </div>
                            <?php endforeach ?>                                    
                        </div>
                    </div>
                </div>
            </div>
        </section>
    
                <div class='container'style=" margin-top: 64px">
                    <div class="special-offer new-style margin-top70">
                        <h3 class="title-style-2">También te gustará</h3>
                        <div class="special-offer-list">
                            <?php foreach($relacionados->result() as $p): ?>
                                <div class="special-offer-layout">
                                    <div class="image-wrapper">
                                        <a href="<?= $p->link ?>" class="link"><img src="<?= $p->portada ?>" alt="" class="img-responsive"></a>
                                        <div class="title-wrapper">
                                            <a href="<?= $p->link ?>" class="title"><?= $p->destinos_nombre ?></a>
                                            <i class="icons flaticon-circle"></i>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach ?>                                    
                        </div>
                    </div>
                </div>
    </form>
    
            </div>
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top" class="link"><i class="fa fa-angle-double-up"></i></a></div>
    
</div>

<script src='<?= base_url() ?>js/template/slzexploore-core-form.js?ver=2.0'></script>
<script src='<?= base_url() ?>js/template/jquery.form.min.js?ver=3.51.0-2014.06.20'></script>
<script src='<?= base_url() ?>js/template/select2.min.js?ver=4.5.3'></script>
<script src='<?= base_url() ?>js/template/libs/plus-minus-input/plus-minus-input.js?ver=4.5.3'></script>
<script src="<?= base_url() ?>js/template/libs/isotope/isotope.min.js"></script>
<script src="<?= base_url() ?>js/template/libs/fancybox/js/jquery.mousewheel-3.0.6.pack.js"></script>
<script src="<?= base_url() ?>js/template/libs/fancybox/js/jquery.fancybox.js"></script>
<script src="<?= base_url() ?>js/template/libs/fancybox/js/jquery.fancybox-buttons.js"></script>
<script src="<?= base_url() ?>js/template/libs/fancybox/js/jquery.fancybox-thumbs.js"></script>
<script src="<?= base_url() ?>js/template/libs/mouse-direction-aware/jquery.directional-hover.js"></script>
<script src="<?= base_url() ?>js/template/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url() ?>js/template/pages/tour-view.js"></script>

