<div class="timeline-book-block book-tour <?= !empty($_GET['solicitud']) ? 'show-book-block' : '' ?>">
    <div class="find-widget find-hotel-widget widget new-style" >
        <h4 class="title-widgets"><?= $detail->destinos_nombre ?></h4>
        <div class="content-widget tour-booking slz-booking-wrapper">

            <div class="extra-item">
                <h4 class="capt"> 
                    <span class="btn btn-maincolor btn-book-tour" style=" font-weight: 600; border: 1px solid #fff; background-color: #fff;font-size: 18px;border-radius: 0px; margin-left: -32px; margin-right: 10px;">1</span>
                    HOLA! ESCOGE UNA OPCIÓN DE VIAJE
                </h4>
                <div class="text-box-wrapper">
                    <label class="tb-label">
                        Ciudad de salida 
                    </label>
                    <div class="input-group">
                        <?php $this->db->where('destino',str_replace("  "," ",$detail->destinos_nombre)); ?>
                        <?php $this->db->order_by('ciudad','ASC'); ?>
                        <?php $ciudades = $this->db->get('tarifas_bus'); ?>
                        <?php 
                            if($ciudades->num_rows()>0){
                                echo form_dropdown_from_query('ciudad',$ciudades,'id','ciudad','','id="ciudad" placeholder="Exem:Igualada" style="float:none"',FALSE,'tb-input');                        
                            }else{ ?>
                            <input type="text" placeholder="Ejem: Igualada" name="ciudad" class="tb-input" id="ciudad">
                        <?php } ?>
                        <i class="tb-icon fa fa-home input-group-addon"></i>
                    </div>
                </div>
                <div class="text-box-wrapper">
                    <label class="tb-label">
                        Circuito de destino
                    </label>
                    <div class="input-group">
                        <input type="text" id='destino' name="destino" placeholder="Circuit de destí" class="tb-input" value="<?= $detail->destinos_nombre ?>">
                        <i class="tb-icon fa fa-suitcase input-group-addon"></i>
                    </div>
                </div>
                <div class="text-box-wrapper">
                    <label class="tb-label">
                        Fecha de salida
                    </label>
                    <div class="input-group">
                        <input type="text" id='fecha' name="fecha" placeholder="DD/MM/YY" class="tb-input" value="<?= !empty($editar) ? date("d/m/Y", strtotime($editar->fecha)) : '' ?>">
                        <i class="tb-icon fa fa-calendar input-group-addon"></i>
                    </div>
                </div>
                <div class="count text-box-wrapper">
                    <label class="tb-label">
                        Alumnos
                    </label>
                    <div class="input-group">
                        <button type="button" disabled="disabled" data-type="minus" data-field="alumnos" class="input-group-btn btn-minus">
                            <span class="tb-icon fa fa-minus"></span>
                        </button>
                        <input type="number" id="alumnos" name="alumnos" min="0" max="9000" value="<?= empty($editar) ? 0 : $editar->alumnos ?>" class="tb-input count">
                        <button type="button" data-type="plus" data-field="alumnos" class="input-group-btn btn-plus">
                            <span class="tb-icon fa fa-plus"></span>
                        </button>
                    </div>
                </div>
                <div class="text-box-wrapper">
                    <label class="tb-label">
                        Profesores
                    </label>
                    <div class="input-group">
                        <button type="button" disabled="disabled" data-type="minus" data-field="profesores" class="input-group-btn btn-minus">
                            <span class="tb-icon fa fa-minus"></span>
                        </button>
                        <input type="number" name="profesores" min="1" max="900" value="<?= empty($editar) ? 1 : $editar->profesores ?>" class="tb-input count">
                        <button type="button" data-type="plus" data-field="profesores" class="input-group-btn btn-plus">
                            <span class="tb-icon fa fa-plus"></span>
                        </button>
                    </div>
                </div>
            </div><!-- End extra items -->

            <div class="extra-item">

                <h4  class="capt">
                    <span class="btn btn-maincolor btn-book-tour" style=" font-weight: 600; border: 1px solid #fff; background-color: #fff;font-size: 18px;border-radius: 0px; margin-left: -32px; margin-right: 10px;">2</span>RELLENA ESTE FORMULARIO PARA RECIBIR TU PRESUPUESTO</h4>
                <div class="text-box-wrapper">
                    <label class="tb-label">
                        Tu nombre
                    </label>
                    <div class="input-group">
                        <input type="text" id='nombre' name="nombre" placeholder="Escriu tu nombre" class="tb-input" value="<?= !empty($editar) ? $editar->nombre : '' ?>">
                        <i class="tb-icon fa fa-pencil input-group-addon"></i>
                    </div>
                </div>
                <div class="text-box-wrapper">
                    <label class="tb-label">
                        Tu teléfono
                    </label>
                    <div class="input-group">
                        <input type="text" id='telefono' name="telefono" placeholder="Escribe tu teléfono" class="tb-input" value="<?= !empty($editar) ? $editar->telefono : '' ?>">
                        <i class="tb-icon fa fa-phone input-group-addon"></i>
                    </div>
                </div>
                <div class="text-box-wrapper">
                    <label class="tb-label">
                       Ref. del Presupuesto
                    </label>
                    <div class="input-group">
                        <input type="text" id='referencia' name="referencia" placeholder="Pon el nombre que más te guste" class="tb-input" value="<?= !empty($editar) ? $editar->referencia : '' ?>">
                        <!--                        <i class="tb-icon fa fa-calendar input-group-addon"></i>-->
                    </div>
                </div>
                <div class="text-box-wrapper">
                    <label class="tb-label">
                        Nombre del instituto
                    </label>
                    <div class="input-group">
                        <input type="text" id='instituto' name="instituto" placeholder="Nombre del instituto" class="tb-input" value="<?= !empty($editar) ? $editar->instituto : '' ?>">
                        <i class="tb-icon fa fa-graduation-cap input-group-addon"></i>
                    </div>
                </div>
                <div class="text-box-wrapper">
                    <label class="tb-label">
                        Localidad
                    </label>
                    <div class="input-group">
                        <input type="text" id='localidad' name="localidad" placeholder="Localidad del instituto" class="tb-input" value="<?= !empty($editar) ? $editar->localidad : '' ?>">
                        <i class="tb-icon fa fa-home input-group-addon"></i>
                    </div>
                </div>
            </div><!-- End extra items -->

            <div class="extra-item" style=" margin-left: 15px">
                <!--                <h4 style="font-weight: 600;font-size: 16.1px;    text-decoration: underline;">Completa este pequeño formulario para recibir tu presupuesto</h4>               -->
                <div>
                    <label class="tb-label " style=" text-decoration: underline;">
                        Nivel del curso
                    </label>
                    <div>
                        <label><input type="radio" id='curso' name="curso" value="<?= "1º ESO" ?>"> 1º ESO</label>
                        <label><input type="radio" id='curso' name="curso" value="<?= "2º ESO" ?>"> 2º ESO</label>
                        <label><input type="radio" id='curso' name="curso" value="<?= "3º ESO" ?>"> 3º ESO</label>
                        <label><input type="radio" id='curso' name="curso" value="<?= "4º ESO" ?>"> 4º ESO</label>
                        <label><input type="radio" id='curso' name="curso" value="<?= "1º Bachillerato" ?>"> 1º Bachillerato</label>
                        <label><input type="radio" id='curso' name="curso" value="<?= "2er Bachillerato" ?>"> 2º Bachillerato</label>
                        <label><input type="radio" id='curso' name="curso" value="<?= "Ciclo formativo" ?>"> Ciclo formativo</label>
                    </div>
                </div>
            </div><!-- End extra items -->

            <div class="extra-item" style=" margin-left: 15px">
                <div style=" margin-top: 14px">
                    <label class="tb-label" style=" text-decoration: underline;">
                        Cargo
                    </label>
                    <div>
                        <label><input type="radio" id='cargo' name="cargo" value="<?= 'Profesor' ?>"> Profesor</label>
                        <label><input type="radio" id='cargo' name="cargo" value="<?= 'Director' ?>"> Director</label>
                        <label><input type="radio" id='cargo' name="cargo" value="<?= 'Coordinador' ?>"> Coordinador</label>
                        <label><input type="radio" id='cargo' name="cargo" value="<?= 'AMPA' ?>"> AMPA</label>
                        <label><input type="radio" id='cargo' name="cargo" value="<?= 'Padre/ Madre' ?>"> Padre/Madre</label>
                    </div>
                </div>
            </div><!-- End extra items -->

            <!-- Extra Items -->
            <div class="extra-item">
                <h4  class="capt">
                    <span class="btn btn-maincolor btn-book-tour" style=" font-weight: 600; border: 1px solid #fff; background-color: #fff;font-size: 18px;border-radius: 0px; margin-left: -32px; margin-right: 10px;">3</span>ACTIVIDADES INCLUIDAS EN TU DESTINO</h4>
                <p class="des" style=" margin-left: 15px">Si algunas de estas actividades no te satisfacen las puedes deseleccionar y no se incluirán en tu viaje.</p>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th>Activides</th>
                            <th>Precio</th>
                            <th>Transporte</th>
                            <th>Duración</th>
                            <th>Seguro</th>
                            <th>Guia</th>
                            <th>Menú</th>
                            <th>Monitores</th>
                            <th>Añadir</th>
                        </tr>
                        <?php if (!empty($editar)) $actividades = explode("//", $editar->actividades); ?>
                        <?php foreach ($detail->actividades->result() as
                                       $a): ?>
                            <?php if ($a->precio == -1): ?>
                                <tr>
                                    <td class="item-info">
                                        <h5><?= $a->actividad ?></h5>
                                    </td>
                                    <td class="item-price"> <span>Incluida</span></td>
                                    <td class="is-person"><?= $a->transporte ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->duracion ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->seguro ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->guia ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->menu ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->monitores ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="number-item">
                                        <input type="checkbox" class="actividad" data-id="<?= $a->id ?>" data-precio='<?= $a->precio >= 0 ? $a->precio : 0 ?>' data-name="<?= $a->actividad ?>"
                                            <?php
                                            if (empty($editar) || !empty($actividades) && in_array($a->actividad, $actividades))
                                                echo "checked"
                                            ?>>
                                    </td>
                                </tr>
                            <?php endif ?>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
                <div class="booking-des hide"></div>
            </div><!-- End extra items --> <!--



            <!-- Extra Items -->
            <div class="extra-item">
                <h4  class="capt">
                    <span class="btn btn-maincolor btn-book-tour" style=" font-weight: 600; border: 1px solid #fff; background-color: #fff;font-size: 18px;border-radius: 0px; margin-left: -32px; margin-right: 10px;">3</span>¿QUIERES ALGUNA ACTIVIDAD EXTRA?</h4>
                <p class="des" style=" margin-left: 15px">Selecciona las actividades que más se adapten a su viaje.</p>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th>Actividad</th>
                            <th>Precio</th>
                            <th>Transporte</th>
                            <th>Duración</th>
                            <th>Seguro</th>
                            <th>Guia</th>
                            <th>Menú</th>
                            <th>Monitores</th>
                            <th>Añadir</th>
                        </tr>
                        <?php if (!empty($editar)) $actividades = explode("//", $editar->actividades); ?>
                        <?php foreach ($detail->actividades->result() as
                                       $a): ?>
                            <?php if ($a->precio >= 0): ?>
                                <tr>
                                    <td class="item-info">
                                        <h5><?= $a->actividad ?></h5>
                                    </td>
                                    <td class="item-price"> <span><?= str_replace(",00","",number_format($a->precio,2,',','.')) ?></span> €</td>
                                    <td class="is-person"><?= $a->transporte ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->duracion ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->seguro ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->guia ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->menu ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->monitores ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="number-item">
                                        <input type="checkbox" class="actividad" data-id="<?= $a->id ?>" data-precio='<?= $a->precio >= 0 ? $a->precio : 0 ?>' data-name="<?= $a->actividad ?>"
                                            <?php if (!empty($actividades) && in_array($a->actividad, $actividades)) echo "checked" ?>>
                                    </td>
                                </tr>
                            <?php endif ?>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
                <div class="booking-des hide"></div>
            </div><!-- End extra items -->

            <div class="extra-item">
                <h4  class="capt">
                    <span class="btn btn-maincolor btn-book-tour" style=" font-weight: 600; border: 1px solid #fff; background-color: #fff;font-size: 18px;border-radius: 0px; margin-left: -32px; margin-right: 10px;">4</span>SELECCIONA UNA ASESORA Y SI QUIERES HAZLE UN COMENTARIO</h4>
                <div class="row" style="margin-left:0px; margin-right:0px;">
                    <div class="col-xs-12 col-sm-6" style=" margin-top: 28px">
                        <div class="col-xs-12" align='left'>
                            <!--                            <p> Seleccioni el comercial que vol que l'antegui</p>-->
                        </div>
                        <div class="col-xs-8" style="margin-bottom:20px;">
                            <div class="avatar" style="float:left; text-align: center;">
                                <img class="img-responsive" style="border-radius:100%; width:60px;" alt="" src="<?= base_url('img/homepage/avatar-contact-2.jpg') ?>">
                            </div>
                            <div style="float:left; margin-left:10px; width:60%">
                                <label style="width:100%;"><input type='radio' name='asesor' value='Emma Soler'> <b>Anabel Mateos Mijares</b></label>
                                <label style="width:100%;" for="asesor"><span>Nacional</span></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="input-group" style="border: 1px solid rgb(204, 204, 204); padding: 20px; width:100%; margin:15px 0; width:100%; text-align:left;">
                            <label for="comentario">Deja tu comentario</label>
                            <textarea class="form-control" name="comentario" id="comentario" placeholder="Comentario" style="border: 1px solid rgb(204, 204, 204); min-height: 110px;" placeholder='Deja tu comentario'><?= empty($editar) ? '' : $editar->comentario ?></textarea>
                        </div>
                    </div>
                </div>
            </div><!-- End extra items -->

            <div class="extra-item">
                <h4  class="capt">
                    <span class="btn btn-maincolor btn-book-tour" style=" font-weight: 600; border: 1px solid #fff; background-color: #fff;font-size: 18px;border-radius: 0px; margin-left: -32px; margin-right: 10px;">5</span>ENHORABUENA YA LO TENEMOS TODO</h4>
                <p class="des" style=" margin-left: 15px" >Haz click en el botón de consultar y recibirás el presupuesto por email.</p>
                
                
                <div  class="premail text-box-wrapper">
                    <label class="tb-label">
                        Email
                    </label>
                    <div class="input-group">
                        <input type="text" id='email' name="email" placeholder="email" class="tb-input" value="<?= !empty($editar) ? $editar->email : '' ?>">
                        <i class="tb-icon fa fa-envelope input-group-addon"></i>
                    </div>
                </div>
                <div class="precaptcha text-box-wrapper">
                    <div>
                        <div class="g-recaptcha" data-sitekey="6LfG5i0UAAAAABfvs7-lS8DDkZPMlwEXOcs1TMWi"></div>
                    </div>
                </div>
            
            </div><!-- End extra items -->

        </div>
        <input type="hidden" name="actividades" id="actividades" value="<?= !empty($editar) ? $editar->actividades : '' ?>">
        <input type="hidden" name="destinos_id" value="<?= $detail->id ?>">
        <input type="hidden" name="estado" value="0">
        <input type="hidden" id="precioinput" name="precio" value="<?= $detail->precios->num_rows() > 0 && $detail->precios->row(0)->detalle->num_rows() ? number_format($detail->precios->row(0)->detalle->row(0)->precio, 2, ',', '.') : number_format($detail->precio, 2, ',', '.') ?>">
        <input type="hidden" name="sesion" value="<?= empty($_SESSION['user']) ? $_SESSION['tmpsesion'] : $_SESSION['user'] ?>">
        <div class="message"></div>
        <div class="clearfix"></div>
        <div class="slz-btn-group">
                <button type="submit" id="guardar" data-hover="Enviar" class="btn btn-slide btn-check slz-add-to-cart" data-id="588">
                <span class="text">Consultar</span>
                <span class="icons fa fa-long-arrow-right"></span>
            </button>
        </div>

    </div>
</div>

<script type='text/javascript'>
/* <![CDATA[ */
var ajaxurl = "http:\/\/wp.swlabs.co\/exploore\/wp-admin\/admin-ajax.php";
/* ]]> */

var precios = <?php
        $precios = array();
        foreach($detail->precios->result() as $p){
            $pre = array();
            $pre['desde'] = $p->fecha_desde;
            $pre['hasta'] = $p->fecha_hasta;
            $detalle = array();
            foreach($p->detalle->result() as $d){
                $d->alumnos = (int)$d->alumnos;
                $d->precio = $d->precio<0?0:$d->precio;
                $detalle[] = $d;                
            }
            $pre['detalle'] = $detalle;
            $precios[] = $pre;
        }
        echo json_encode($precios);
    ?>;
        
<?php if(!empty($editar)): ?>
    $(document).on("ready",function(){
       total(); 
    });
<?php endif ?>
    
    function iraform(){
        $(".book-tour").addClass("show-book-block");
        document.location.href="#presupuesto";
    }
</script>

<script>
    function in_date_range(value,desde,hasta) {
        // format: dd/mm/yyyy;
        value = value.split("/");
        desde = desde.split("-");
        hasta = hasta.split("-");
        var startDate = new Date(desde[0], desde[1]-1, desde[2]),
            endDate = new Date(hasta[0], hasta[1]-1, hasta[2]),
            date = new Date(value[2], value[1]-1, value[0]);
        return startDate <= date && date <= endDate;
    }

    function total(){
        var fecha = $("#fecha").val(),
            alumnos = parseInt($("#alumnos").val()),
            subtotal = $("#subtotals"),
            plazas = $("#plazas"),
            fecha_salida = $("#fechasalida"),
            total = $("#totals,#precioalumno");
        var encontrado = false;
        var preciado = false;
        var precio =  0;
        for(var i in precios){
            
            if(in_date_range(fecha,precios[i].desde,precios[i].hasta)){
                for(var k in precios[i].detalle){
                    if(!encontrado){
                        encontrado = true;
                        precio = precios[i].detalle[k].precio;
                    }
                    if(precios[i].detalle[k].alumnos >= alumnos && !preciado){
                        preciado = true;
                        //encontrado = true;
                        subtotal.html(precios[i].detalle[k].precio);
                        precio = precios[i].detalle[k].precio;
                    }
                    var last = precios[i].detalle.length-1;
                    if(precios[i].detalle[last].alumnos<alumnos){
                        subtotal.html(precios[i].detalle[last].precio);
                        precio = precios[i].detalle[last].precio;
                    }
                }
                if(!encontrado && alumnos>precios[i].detalle[precios[i].detalle.length-1].alumnos){
                    subtotal.html(precios[i].detalle[precios[i].detalle.length-1].precio);
                    precio = precios[i].detalle[precios[i].detalle.length-1].precio;
                }
                total.html(precio);
                $("#precioinput").val(precio);
                plazas.html(alumnos);
                fecha_salida.html(fecha);
            }
        }
        var extras = 0;
        $("#actividades").val("");
        $("#extraslabel").html("0");
        $(".actividad").each(function(){
            var tot = parseFloat($("#precioinput").val());
            if(!isNaN(tot) && $(this).prop("checked")){
                tot+= parseFloat($(this).data("precio"));
                extras+= parseFloat($(this).data("precio"));
                $("#extraslabel").html(extras);
                $("#precioinput").val(tot);
                total.html(tot);
                var actividades = $("#actividades").val();
                actividades = actividades+$(this).data("id")+"--"+$(this).data("name")+"//";
                $("#actividades").val(actividades);
            }
        });
    }

    function send(form){
        var url_validation = "<?php
            if(empty($editar)){
                echo base_url('destinos/frontend/solicitudes/insert_validation/');
            }else{
                echo base_url('destinos/frontend/solicitudes/update_validation/'.$editar->id);
            }
            ?>";
        var url = "<?php
            if(empty($editar)){
                echo base_url('destinos/frontend/solicitudes/insert');
            }else{
                echo base_url('destinos/frontend/solicitudes/update/'.$editar->id);
            }
            ?>";
        var f = new FormData(form);
        $(".message").html("Por favor se paciente, estamos generando el presupuesto inmediatamente te enviaremos a tu correo.");
        $("#guardar").attr('disabled',true);
        $.ajax({
            url:url_validation,
            data: f,
            context: document.body,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success:function(data){
                $("#guardar").attr('disabled',false);
                data = data.replace(/<[^p].*?>/g,"",data);
                data = JSON.parse(data);
                if(data.success){
                    $("#guardar").attr('disabled',true);
                    $.ajax({
                        url:url,
                        data: f,
                        context: document.body,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success:function(data){
                            $("#guardar").attr('disabled',false);
                            data = data.replace(/<[^p].*?>/g,"",data);
                            data = JSON.parse(data);
                            if(data.success){
                                $(".message").html("¡Perfecto! Su presupuesto se ha enviado correctamente, consulta tu correo y sorpréndete del viaje que tienes planificado. Cualquier consulta o modificación del itinerario en el presupuesto posee los datos del asesor para cambiar lo que necesita.");
                            }else{
                                grecaptcha.reset();
                                $(".message").html("Ha ocurrido un error interno, por favor comunícate con ByeBye Group");
                        }
                        }
                    });
                }else{
                    grecaptcha.reset();
                    $(".message").html(data.error_message);
                }
            }
        });
        return false;
    }
    $(document).ready(function(){
        var mindate = "<?php        
            if($detail->precios->num_rows()>0){
                if(strtotime($detail->precios->row(0)->fecha_desde)>strtotime(date("Y-m-d"))){
                    echo date("d/m/Y",strtotime($detail->precios->row(0)->fecha_desde));             
                }
                else{
                    echo date("d/m/Y");
                }
            }
            else{
                echo date("d/m/Y");
            }
         ?>";
        mindate = mindate.split("/");
        mindate = new Date(mindate[2], mindate[1]-1, mindate[0]);

        var maxdate = "<?= $detail->precios->num_rows()>0?date("d/m/Y",strtotime($detail->precios->row($detail->precios->num_rows()-1)->fecha_hasta)):date("d/m/Y") ?>";
        maxdate = maxdate.split("/");
        maxdate = new Date(maxdate[2], maxdate[1]-1, maxdate[0]);

        $('#fecha').datepicker({
            format: 'dd/mm/yyyy',
            maxViewMode: 0,
            startDate:mindate,
            endDate:maxdate
        });
        $("#fecha, #alumnos, .actividad").on('change',function(){
            total();
        });
        total();
    });
</script>
