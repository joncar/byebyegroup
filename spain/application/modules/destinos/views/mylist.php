
<!-- MAIN CONTENT-->
    <div class="main-content">
        <?php if(is_numeric($categoria)): ?>
        <?php
        $cat = $this->db->get_where("categorias_destinos",array("id"=>$categoria)); 
        $fondo = $cat->row(0)->fondo;
        ?>    
        
        <section class="page-banner car-rent-result" style="background:url(<?= base_url("img/destinos/".$fondo) ?>); background-size:cover">
        <?php elseif(!empty($_GET) && !empty($_GET["lowcost"])): ?>
        <section class="page-banner car-rent-result" style="background:url(<?= base_url("img/destinos/".$this->db->get_where('categorias_destinos',array('id'=>7))->row()->fondo) ?>); background-size:cover">
        <?php else: ?>
        <section class="page-banner car-rent-result">
        <?php endif ?>       
            <div class="patterndestino"></div>
            <div class="container">
                <div class="page-title-wrapper">
                    <div class="page-title-content"> 
                        <ol class="breadcrumb">
                            <li><a href="<?= site_url() ?>" class="link home">Home</a></li>
                            <li><a href="<?= site_url("destinos") ?>" class="link">tots els destins</a></li>
                        </ol>
                        <div class="clearfix"></div>
                        <?php if(is_numeric($categoria)): ?>                            
                            <h2 class="captions"><?= $cat->num_rows()>0?$cat->row()->categorias_destinos_nombre:'' ?></h2>
                        <?php elseif(empty($_GET)): ?>
                            <h2 class="captions">Destins</h2>
                         <?php elseif(!empty($_GET["lowcost"])): ?>
                            <h2 class="captions">Viatges Estrella</h2>
                         <?php else: ?>|
                            <h2 class="captions">Resultats de la busqueda</h2>
                        <?php endif ?>
                        </div>
                </div>
            </div>
        </section>
        <div class="page-main" id="list">
            <?php if(!is_numeric($categoria) && !empty($_GET) && empty($_GET['lowcost'])): ?>
                    <div class="trip-info">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="label-route-widget">
                                        <span class="departure">
                                                <span class="country">Destino: <?= @$_GET["destino"] ?></span>  |
                                                <span class="country">Busqueda: <?= @$_GET["destinos_nombre"] ?></span> |
                                                <span class="country">Fecha de sortida: <?= @$_GET["fecha_desde"] ?></span> |
                                                <span class="country">Fecha de llegada: <?= @$_GET["fecha_hasta"] ?></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
             <?php endif ?>
            <div class="clearfix"></div>
            <div class="car-rent-result-main padding-top padding-bottom">                
                <?= $output ?>                
            </div>
    </div>
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top" class="link"><i class="fa fa-angle-double-up"></i></a></div>
</div>
<script src="<?= base_url() ?>js/template/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url() ?>js/template/libs/nst-slider/js/jquery.nstSlider.min.js"></script>
<script src="<?= base_url() ?>js/template/libs/plus-minus-input/plus-minus-input.js"></script>
<script src="<?= base_url() ?>js/template/pages/sidebar.js"></script>
