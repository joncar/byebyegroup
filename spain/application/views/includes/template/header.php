<header>
    <div class="bg-transparent header-01">
        <div class="header-topbar">
            <div class="container">
                <ul class="topbar-left list-unstyled list-inline pull-left">
                    <li><a href="void(0)" class="monney dropdown-text"><i class="topbar-icon fa fa fa-phone "></i><span> 902 002 068</span></a>
                    </li><li><a href="https://www.facebook.com/pg/byebyegroup/photos/?ref=page_internal" class="link  facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
<!--                    <li><a href="https://plus.google.com/" class="link  google-plus" target="_blank"><i class="fa fa-google-plus"></i></a></li>-->
                    <li><a href="https://twitter.com/byebyegroup" class="link  twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="http://stalkture.com/p/byebyegroup/3680527830/" class="link  instagram" target="_blank"><i class="fa fa-instagram"></i></a></li></ul>
                    <ul class="topbar-right pull-right list-unstyled list-inline login-widget">
<!--                        <li><a href="--><?//= site_url('presupuesto') ?><!--" class="item"><i class="fa fa-book"></i> Pressupostos</a></li>-->
<!--                        <li><a href="--><?//= site_url('favoritos') ?><!--" class="item"><i class="fa fa-heart"></i> Preferits</a></li>-->
                        <li style="display:inline-block !important;"><a href="<?= site_url() ?>" class="item"><img src="<?= base_url('img/mapa.png') ?>" style="width:30px; height:30px;"> Nacional</a></li>
                        <?php if(empty($_SESSION['user'])): ?>
                        <li><a href="<?= site_url('panel') ?>" class="item">Entrar</a></li>
<!--                        <li><a href="--><?//= site_url('panel') ?><!--" class="item">Registrar</a></li>-->
                        <?php else: ?>
                        <li><a href="<?= site_url('main/unlog') ?>" class="item"><i class="fa fa-close"></i> Salir</a></li>
                        <?php endif ?>
                    </ul>
            </div>
        </div>
        <div class="header-main">
            <div class="container">
                <div class="header-main-wrapper">
                    <div class="hamburger-menu">
                        <div class="hamburger-menu-wrapper">
                            <div class="icons"></div>
                        </div>
                    </div>

                    <div class="navbar-header">
                        <div class="logo">
                            <a href="<?= site_url() ?>" class="header-logo"><img src="<?= base_url() ?>img/logo/logo.svg" alt="" /></a>
                        </div>
                    </div>
                    <nav class="navigation">
                        <ul class="nav-links nav navbar-nav">
                            <li class="active">
                                <a href="<?= site_url() ?>" class="main-menu">
                                    <span class="text" style=" font-weight: 600">Home</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= site_url('p/nosotros') ?>" class="main-menu">
                                    <span class="text" style=" font-weight: 600">Nosotros</span>
                                </a>
                            </li>
                            <!--<li>
                                <a href="about-us.html" class="main-menu">
                                    <span class="text">about</span>
                                </a>
                            </li>-->
                            <li class="dropdown" style="position:initial;">
                                <a href="<?= site_url('destinos') ?>" class="main-menu">
                                    <span class="text"style=" font-weight: 600">Destinos</span>
                                    <span class="icons-dropdown">
                                        <i class="fa fa-angle-down"></i>
                                    </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-1" style="padding:30px;">
                                        <div class="row">
                                            <?php $this->db->order_by('orden','ASC'); ?>
                                            <?php foreach($this->db->get_where('categorias_destinos',array('id !='=>4))->result() as $c): ?>
                                                <?php if($c->id==1): ?>
                                                    <div class="col-xs-12 col-sm-3">
                                                        <a href="<?= site_url('destinos/pack/'.$c->id) ?>" class="link-page">
                                                            <div class="item" style="border-top:0px">
                                                                    <b><?= $c->categorias_destinos_nombre ?></b>
                                                            </div>
                                                        </a>
                                                        <?php $this->db->order_by('destinos_nombre','ASC'); ?>
                                                        <?php $this->db->limit(6); ?>
                                                        <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>$c->id))->result() as $d): ?>
                                                            <a href="<?= site_url('destinos/'.  toURL($d->id.'-'.$d->destinos_nombre)) ?>" class="link-page">
                                                                <div class="item">                                                                
                                                                    <i class="fa <?php if($d->tipo_transporte==1) echo 'fa-bus'; elseif($d->tipo_transporte==2)echo 'fa-plane'; else echo'fa-ship'; ?>"></i> <?= $d->destinos_nombre ?>                                                                
                                                                </div>
                                                            </a>
                                                        <?php endforeach ?>
                                                    </div>
                                                <?php endif ?>
                                                <div class="col-xs-12 col-sm-3">
                                                    
                                                        <a href="<?= site_url('destinos/pack/'.$c->id) ?>" class="link-page">
                                                            <div class="item" style="border-top:0px">
                                                                <?php if($c->id!=1): ?>
                                                                    <b><?= $c->categorias_destinos_nombre ?></b>
                                                                <?php else: ?>
                                                                    <b>&nbsp;</b>
                                                                <?php endif ?>
                                                            </div>
                                                        </a>                                                    
                                                    <?php $this->db->order_by('destinos_nombre','ASC'); ?>
                                                    <?php if($c->id==1): ?>
                                                    <?php $this->db->limit(6,6); ?>
                                                    <?php endif ?>
                                                    <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>$c->id))->result() as $d): ?>
                                                        <a href="<?= site_url('destinos/'.  toURL($d->id.'-'.$d->destinos_nombre)) ?>" class="link-page">
                                                            <div class="item">                                                                
                                                                <i class="fa <?php if($d->tipo_transporte==1) echo 'fa-bus'; elseif($d->tipo_transporte==2)echo 'fa-plane'; else echo'fa-ship'; ?>"></i> <?= $d->destinos_nombre ?>                                                                
                                                            </div>
                                                        </a>
                                                    <?php endforeach ?>
                                                </div>
                                            <?php endforeach ?>
                                        </div>
                                </div>
                            </li>
                            <li class="dropdown">
                                <a href="<?= site_url('destinos') ?>?lowcost=2" class="main-menu">
                                    <span class="text"style=" font-weight: 600">Viajes Estrella</span>
                                    <span class="icons-dropdown">
                                        <i class="fa fa-angle-down"></i>
                                    </span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-1">
                                    <?php $this->db->order_by('id','DESC'); /*$this->db->limit(6);*/ ?>
                                    <?php foreach($this->db->get_where('destinos',array('lowcost'=>2))->result() as $d): ?>
                                            <li>
                                                <a href="<?= site_url('destinos/'.  toURL($d->id.'-'.$d->destinos_nombre)) ?>" class="link-page">
                                                    <?= $d->destinos_nombre ?>
                                                </a>
                                            </li>
                                    <?php endforeach ?>
                                </ul>
                            </li>
                            <li><a href="<?= site_url('galeria') ?>" class="main-menu"><span class="text"style=" font-weight: 600">Galeria</span></a></li>
                            <li><a href="<?= site_url('blog') ?>" class="main-menu"><span class="text"style=" font-weight: 600">Blog</span></a></li>
                            <li><a href="<?= site_url('p/contacto') ?>" class="main-menu"><span class="text"style=" font-weight: 600">Contacto</span></a></li>
                        </ul>
                    </nav>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</header>
