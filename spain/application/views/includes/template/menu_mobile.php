<div class="menu-mobile">
    <ul class="nav-links nav navbar-nav">
        <li class="active">
            <a href="<?= site_url() ?>" class="main-menu">
                <span class="text" style=" font-weight: 600">Home</span>
            </a>
        </li>
        <li>
            <a href="<?= site_url('p/nosotros') ?>" class="main-menu">
                <span class="text" style=" font-weight: 600">Nosotros</span>
            </a>
        </li>
        <!--<li>
            <a href="about-us.html" class="main-menu">
                <span class="text">about</span>
            </a>
        </li>-->
        <li class="dropdown">
            <a href="<?= site_url('destinos') ?>" class="main-menu">
                <span class="text"style=" font-weight: 600">Destinos</span>
            </a>
            <span class="icons-dropdown">
                <i class="fa fa-angle-down"></i>
            </span>
            <ul class="dropdown-menu dropdown-menu-1">
                <?php $this->db->order_by('orden','ASC'); ?>
                <?php foreach($this->db->get('categorias_destinos')->result() as $c): ?>
                    <li>
                        <a href="<?= site_url('destinos/pack/'.$c->id) ?>" class="link-page">
                            <b><?= $c->categorias_destinos_nombre ?></b>
                        </a>
                    </li>
                    <?php $this->db->order_by('destinos_nombre','ASC'); ?>
                    <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>$c->id))->result() as $d): ?>
                            <li>
                                <a href="<?= site_url('destinos/'.  toURL($d->id.'-'.$d->destinos_nombre)) ?>" class="link-page">
                                    <?= $d->destinos_nombre ?>
                                </a>
                            </li>
                    <?php endforeach ?>
                <?php endforeach ?>
            </ul>
        </li>
        <li class="dropdown">
            <a href="<?= site_url('destinos') ?>?lowcost=2" class="main-menu">
                <span class="text"style=" font-weight: 600">Viajes Estrella</span>
            </a>
            <span class="icons-dropdown">
                <i class="fa fa-angle-down"></i>
            </span>
            <ul class="dropdown-menu dropdown-menu-1">
                <?php $this->db->order_by('id','DESC'); /*$this->db->limit(6);*/ ?>
                <?php foreach($this->db->get_where('destinos',array('lowcost'=>2))->result() as $d): ?>
                        <li>
                            <a href="<?= site_url('destinos/'.  toURL($d->id.'-'.$d->destinos_nombre)) ?>" class="link-page">
                                <?= $d->destinos_nombre ?>
                            </a>
                        </li>
                <?php endforeach ?>
            </ul>
        </li>
        <li><a href="<?= site_url('galeria') ?>" class="main-menu"><span class="text"style=" font-weight: 600">Galeria</span></a></li>
        <li><a href="<?= site_url('blog') ?>" class="main-menu"><span class="text"style=" font-weight: 600">Blog</span></a></li>
        <li><a href="<?= site_url('p/contacto') ?>" class="main-menu"><span class="text"style=" font-weight: 600">Contacto</span></a></li>
</div>
