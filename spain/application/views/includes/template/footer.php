<footer>
    <div class="subscribe-email">
        <div class="container">
            <div class="subscribe-email-wrapper">
                <div class="subscribe-email-left">
                    <p class="subscribe-email-title">Suscríbite y <span class="logo-text">Explora</span>&nbsp;las últimas ofertas y destinos</p>
                    <p class="subscribe-email-text">¿Quieres recibir información de nuevos viajes, ofertas, noticias, etc. Envíanos tu email y te informaremos.</p>
                </div>
                <div class="subscribe-email-right">
                    <form onsubmit="return subscribir(this)">
                        <div id="subscralert"></div>
                        <div class="input-group form-subscribe-email">
                            <input type="email" placeholder="Escribe tu email" class="form-control" />
                            <span class="input-group-btn">
                                <button type="submit" class="btn-email">&#8594;</button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="footer-main padding-top padding-bottom">
        <div class="container">
            <div class="footer-main-wrapper">
                <a href="<?= site_url() ?>" class="logo-footer"><img src="<?= base_url() ?>img/logo/logob.svg" alt="" class="img-responsive" style="width:500px"/></a>
                <div class="row">
                    <div class="col-2">
                        <div class="col-md-3 col-xs-5">
                            <div class="contact-us-widget widget">
                                <div class="title-widget">CONTACTA</div>
                                <div class="content-widget">
                                    <div class="info-list">
                                        <ul class="list-unstyled">
                                            <li><i class="icons fa fa-map-marker"></i><a href="https://www.google.es/maps/place/GRUPO+FINALIA/@41.5855626,1.6241467,16z/data=!4m5!3m4!1s0x0:0x122bed82c9971130!8m2!3d41.5859947!4d1.6237342" class="link">Anabel Mateos Mijares</a></li>
                                            <li><i class="icons fa fa-phone"></i><a href="tel:693805251" class="link">693 80 52 51</a></li>
                                            <li><i class="icons fa fa-envelope-o"></i><a href="mailto:a.mateos@byebyegroup.com" class="link">a.mateos@byebyegroup.com</a></li>
                                        </ul>
                                    </div>
                                    <div class="form-email">
                                        <p class="text">Regístrate en nuestra lista de correo para obtener las últimas actualizaciones y ofertas.</p>
                                        <form onsubmit="return subscribir(this)">
                                            <div id="footsubscralert"></div>
                                            <div class="input-group">
                                                <input type="email" placeholder="dirección email" class="form-control form-email-widget" />
                                                <span class="input-group-btn">
                                                    <button type="submit" class="btn-email">&#10004;</button>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-3">
                            <div class="booking-widget widget text-center">
                                <div class="title-widget">LOS + VISTOS</div>
                                <div class="content-widget">
                                    <ul class="list-unstyled">
                                        <?php $this->db->limit(6); $this->db->order_by('visitas','DESC'); ?>
                                        <?php foreach($this->db->get('destinos')->result() as $d): ?>
                                            <li><a href="<?= site_url('destinos/'.  toURL($d->id.'-'.$d->destinos_nombre)) ?>" class="link"><?= $d->destinos_nombre ?></a></li>                                            
                                        <?php endforeach ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-4">
                            <div class="explore-widget widget">
                                <div class="title-widget">MENÚ</div>
                                <div class="content-widget">
                                    <ul class="list-unstyled">
                                        <li><a href="<?= site_url() ?>" class="link">Home</a></li>
                                        <li><a href="<?= site_url('p/nosotros') ?>" class="link">Nosotros</a></li>
                                        <li><a href="<?= site_url('destinos') ?>" class="link">Destinos</a></li>
                                        <li class="dropdown"><a href="<?= site_url('destinos') ?>?lowcost=1" class="link">LowCost</a></li>
                                        <li><a href="<?= site_url('galeria') ?>" class="link">Galeria</a></li>
                                        <li><a href="<?= site_url('blog') ?>" class="link">Blog</a></li>
                                        <li><a href="<?= site_url('p/contacto') ?>" class="link">Contacto</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-2">                        
                        <div class="col-md-5 col-xs-12">
                            <div class="destination-widget widget">
                                <div class="title-widget">GALERIA</div>
                                <div class="content-widget main-gallery-fancybox">
                                    <ul class="list-unstyled list-inline">
                                        <?php $this->db->order_by('id','DESC') ?>
                                        <?php $this->db->limit(8) ?>
                                        <?php $galeria = $this->db->get('destinos_galeria'); ?>
                                            <?php foreach($galeria->result() as $p): ?>
                                                <li class="gallery-content">                                                    
                                                    <a href="<?= base_url() ?>img/destinos/<?= $p->foto ?>" data-fancybox-group="gallery" class="wp-gallery glry-relative fancybox thumb">
                                                        <div style="width:100px; height:100px; background:url(<?= base_url() ?>img/destinos/<?= $p->foto ?>); background-size:auto 100%; background-position:center;"></div>
                                                        <img src="<?= base_url() ?>img/destinos/<?= $p->foto ?>" alt="" class="img-responsive" style="width:100px; height:100px; display: none" />
                                                    </a>
                                                </li>
                                        <?php endforeach ?>                                           
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hyperlink">
        <div class="container">
            <div class="slide-logo-wrapper">
                <div class="logo-item">
                    <a href="#" class="link"><img src="<?= base_url() ?>img/footer/logo-01.png" alt="" class="img-responsive" /></a>
                </div>
                <div class="logo-item">
                    <a href="#" class="link"><img src="<?= base_url() ?>img/footer/logo-02.png" alt="" class="img-responsive" /></a>
                </div>
                <div class="logo-item">
                    <a href="#" class="link"><img src="<?= base_url() ?>img/footer/logo-03.png" alt="" class="img-responsive" /></a>
                </div>
                <div class="logo-item">
                    <a href="#" class="link"><img src="<?= base_url() ?>img/footer/logo-04.png" alt="" class="img-responsive" /></a>
                </div>
                <div class="logo-item">
                    <a href="#" class="link"><img src="<?= base_url() ?>img/footer/logo-05.png" alt="" class="img-responsive" /></a>
                </div>
                <div class="logo-item">
                    <a href="#" class="link"><img src="<?= base_url() ?>img/footer/logo-06.png" alt="" class="img-responsive" /></a>
                </div>
                <div class="logo-item">
                    <a href="#" class="link"><img src="<?= base_url() ?>img/footer/logo-01.png" alt="" class="img-responsive" /></a>
                </div>
                <div class="logo-item">
                    <a href="#" class="link"><img src="<?= base_url() ?>img/footer/logo-02.png" alt="" class="img-responsive" /></a>
                </div>
                <div class="logo-item">
                    <a href="#" class="link"><img src="<?= base_url() ?>img/footer/logo-03.png" alt="" class="img-responsive" /></a>
                </div>
                <div class="logo-item">
                    <a href="#" class="link"><img src="<?= base_url() ?>img/footer/logo-04.png" alt="" class="img-responsive" /></a>
                </div>
                <div class="logo-item">
                    <a href="#" class="link"><img src="<?= base_url() ?>img/footer/logo-05.png" alt="" class="img-responsive" /></a>
                </div>
                <div class="logo-item">
                    <a href="#" class="link"><img src="<?= base_url() ?>img/footer/logo-06.png" alt="" class="img-responsive" /></a>
                </div>
            </div>
            <div class="social-footer">
                <ul class="list-inline list-unstyled">
                    <li><a href="https://www.facebook.com/byebyegroup/photos/?ref=page_internal#" class="link facebook"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/byebyegroup" class="link twitter"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="http://stalkture.com/p/byebyegroup/3680527830/" class="link pinterest"><i class="fa fa-instagram"></i></a></li>
<!--                    <li><a href="#" class="link google"><i class="fa fa-google"></i></a></li>-->
                </ul>
            </div>
            <div class="name-company">&copy; Disseny per Hipo.</div>
        </div>
    </div>
</footer>
