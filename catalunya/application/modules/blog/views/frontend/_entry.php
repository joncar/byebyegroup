<div class="blog-post-intro <?= $n==0?'sticky-post':'' ?>  animated slideInRight"> 
    <div data-bg="<?= base_url('img/fotos/'.$detail->foto) ?>" class="blog-post-intro-bg" style="background-image: url(<?= base_url('img/fotos/'.$detail->foto) ?>);"></div> 
    <h3 class="text-uppercase post-title" style="display:inline-block"><?= $detail->titulo ?></h3> 
    <span class="post-divider">
        <i aria-hidden="true" class="fa fa-file-o"></i> 
    </span>
    <p style="margin: 0px;"> 
        <?= substr(strip_tags($detail->texto),0,255) ?>
    </p>
    <div class="hovered text-uppercase"> 
        <a class="post-link" href="<?= site_url('blog/'.toURL($detail->id.'-'.$detail->titulo)) ?>" title="<?= $detail->titulo ?>" rel="bookmark">lleguir Més</a>
    </div> 
</div>