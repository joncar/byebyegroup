<div class="main-content">
    <section class="page-banner homepage-default">
        <div class="container">
            <div class="homepage-banner-warpper">
                <div class="homepage-banner-content">
                    <div class="group-title">
                        <h1 class="title">EXPERTS</h1>
                        <p class="text">EN VIATGES DE FINAL DE CURS

                        </p>
                    </div>
                    <div class="group-btn"><a href="#" data-hover="FES CLICK" class="btn-click"><span class="text">Explora i gaudeix</span><span class="icons fa fa-long-arrow-right"></span></a></div>
                </div>
            </div>
        </div>
    </section>
    <?php $this->load->view('_searchMain'); ?>
    <section class="about-us layout-1 padding-top padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="group-title">
                        <div class="sub-title">
                            <p class="text">per que viatjar</p><i class="icons flaticon-people"></i></div>
                        <h2 class="main-title">amb nosaltres?</h2></div>
                    <div class="about-us-wrapper">
                        <p class="text">
                            Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut eim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat uisas aute irure dolor ullamco laboris nisi in reprehenderit.
                        </p>
                        <div class="group-list">
                            <ul class="list-unstyled about-us-list">
                                <li>
                                    <p class="text">First Class Flights</p>
                                </li>
                                <li>
                                    <p class="text">5 Star Accommodations</p>
                                </li>
                                <li>
                                    <p class="text">Inclusive Packages</p>
                                </li>
                                <li>
                                    <p class="text">Latest Model Vehicles</p>
                                </li>
                            </ul>
                            <ul class="list-unstyled about-us-list">
                                <li>
                                    <p class="text">Handpicked Hotels</p>
                                </li>
                                <li>
                                    <p class="text">Accesibility managment</p>
                                </li>
                                <li>
                                    <p class="text">10 Languages available</p>
                                </li>
                                <li>
                                    <p class="text">+120 Premium city tours</p>
                                </li>
                            </ul>
                        </div>
                        <div class="group-button"><a href="tour-result.html" class="btn btn-maincolor" style="font-weight: 600;">Pressupost</a><a href="about-us.html" class="btn">Lleguir Més</a></div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div data-wow-delay="0.5s" class="about-us-image wow zoomIn"><img src="<?= base_url() ?>img/homepage/about-us-1.jpg" alt="" class="img-responsive"></div>
                </div>
            </div>
        </div>
    </section>
    
    <div class="container">
        <div class="special-offer margin-top70">
            <h3 class="title-style-2">DESTINS, PAQUETS I VIATGES ESPECIALS </h3>
            <div class="special-offer-list">
                
                <?php foreach($this->db->get('categorias_destinos')->result() as $c): ?>
                    <div class="special-offer-layout">
                        <div class="image-wrapper">
                            <a href="<?= site_url('destinos/pack/'.$c->id) ?>" class="link">
                                <img src="<?= base_url() ?>img/destinos/<?= $c->foto ?>" alt="" class="img-responsive">
                            </a>
                            <div class="title-wrapper">
                                <a href="<?= site_url('destinos/pack/'.$c->id) ?>" class="title"><?= $c->categorias_destinos_nombre ?></a>
                                <i class="icons flaticon-circle"></i>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
               
            </div>
        </div>
    </div>
    
    <section class="tours padding-top padding-bottom" style="margin-top:120px;">
        <div class="container">
            <div class="tours-wrapper">
                <div class="group-title">
                    <div class="sub-title">
                        <p class="text">Viatges</p><i class="icons flaticon-people" style=" color: #e7237e"></i></div>
                    <h2 class="main-title">nacionals</h2></div>
                <div class="tours-content margin-top70">
                    <?php $this->db->limit(3) ?>
                         <div class="tours-list">
                            <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>3))->result() as $d): ?>
                                <?php $this->load->view('_destino',array('d'=>$d)); ?>
                            <?php endforeach ?>
                        </div>
                    <div class="tours-list" style="margin-top:120px">
                        <?php $this->db->limit(3,3) ?>
                        <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>3))->result() as $d): ?>
                                <?php $this->load->view('_destino',array('d'=>$d)); ?>
                        <?php endforeach ?>
                    </div>
                    <a href="<?= site_url('destinos/pack/3') ?>" class="btn btn-transparent margin-top70">més viatges</a>
                </div>
            </div>
        </div>
    </section>
    
    <section class="videos layout-1">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="video-wrapper padding-top padding-bottom">
                        <h5 class="sub-title">Els millors <strong>finals de curs</strong> de curs</h5>
                        <h2 class="title">explora ara</h2>
                        <div class="text">There are many variations of passages of. Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look.</div><a href="tour-result.html" class="btn btn-maincolor">lleguir més</a></div>
                </div>
                <div class="col-md-7">
                    <div class="video-thumbnail">
                        <div class="video-bg"><img src="<?= base_url() ?>img/homepage/video-bg.jpg" alt="" class="img-responsive"></div>
                        <div class="video-button-play"> <i class="icons fa fa-play"></i></div>
                        <div class="video-button-close"></div><iframe src="https://www.youtube.com/embed/moOosWuoDyA?rel=0" allowfullscreen="allowfullscreen" class="video-embed"></iframe></div>
                </div>
            </div>
        </div>
    </section>
    
    
    <section class="tours padding-top padding-bottom" style="margin-top:120px;">
        <div class="container">
            <div class="tours-wrapper">
                <div class="group-title">
                    <div class="sub-title">
                        <p class="text">viatges</p><i class="icons flaticon-transport-1"style=" color: #e7237e"></i></div>
                    <h2 class="main-title">internacionals</h2></div>
                <div class="tours-content margin-top70">
                    <div class="tours-list">
                        <?php $this->db->limit(3) ?>
                        <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>4))->result() as $d): ?>
                                <?php $this->load->view('_destino',array('d'=>$d)); ?>
                        <?php endforeach ?>                        
                    </div>                    
                    <div class="tours-list" style="margin-top:120px">
                        <?php $this->db->limit(3,3) ?>
                        <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>4))->result() as $d): ?>
                                <?php $this->load->view('_destino',array('d'=>$d)); ?>
                        <?php endforeach ?>
                    </div>
                    
                    <a href="<?= site_url('destinos/pack/4') ?>" class="btn btn-transparent margin-top70">more tours</a></div>
            </div>
        </div>
    </section>
    
    
    <section class="hotels padding-top padding-bottom">
        <div class="container">
            <div class="tours-wrapper">
                <div class="group-title">
                    <div class="sub-title">
                        <p class="text">VIATGES I PACKS </p><i class="icons flaticon-sport"style=" color: #e7237e"></i></div>
                    <h2 class="main-title">AVENTURA I ESPECIALS</h2></div>
                <div class="tours-content margin-top70">
                    <?php $this->db->limit(3) ?>
                         <div class="tours-list">
                            <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>2))->result() as $d): ?>
                                <?php $this->load->view('_destino',array('d'=>$d)); ?>
                            <?php endforeach ?>
                        </div>
                    <div class="tours-list" style="margin-top:120px">
                        <?php $this->db->limit(3,3) ?>
                        <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>2))->result() as $d): ?>
                                <?php $this->load->view('_destino',array('d'=>$d)); ?>
                        <?php endforeach ?>
                    </div>
                    <a href="<?= site_url('destinos/pack/2') ?>" class="btn btn-transparent margin-top70">més viatges</a>
                </div>
            </div>
        </div>
    </section>
    <section class="travelers">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="traveler-wrapper padding-top padding-bottom">
                        <div class="group-title white">
                            <div class="sub-title">
                                <p class="text">RELAXA'T I DISFRUTA</p><i class="icons flaticon-people-2"style=" color: #e7237e"></i></div>
                            <h2 class="main-title">TESTIMONIS</h2></div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="traveler-list">
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/homepage/cover-image-1.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/homepage/avatar-1.jpg" alt="" class="img-responsive"></div>
                                <p class="name">Sandara park</p>
                                <p class="address">roma, italy</p>
                                <p class="description">" There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form."</p>
                            </div>
                        </div>
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/homepage/cover-image-2.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/homepage/avatar-2.jpg" alt="" class="img-responsive"></div>
                                <p class="name">Kown Jiyong</p>
                                <p class="address">london, England</p>
                                <p class="description">" There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form."</p>
                            </div>
                        </div>
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/homepage/cover-image-3.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/homepage/avatar-3.jpg" alt="" class="img-responsive"></div>
                                <p class="name">taylor rose</p>
                                <p class="address">pari, France</p>
                                <p class="description">" There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form."</p>
                            </div>
                        </div>
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/homepage/cover-image-4.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/homepage/avatar-4.jpg" alt="" class="img-responsive"></div>
                                <p class="name">john smith</p>
                                <p class="address">new york, USA</p>
                                <p class="description">" There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form."</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="news padding-top padding-bottom">
        <div class="container">
            <div class="news-wrapper">
                <div class="group-title">
                    <div class="sub-title">
                        <p class="text">EXPLORA LES NOSTRES HISTORIES</p><i class="icons flaticon-summer"style=" color: #e7237e"></i></div>
                    <h2 class="main-title">BLOG</h2></div>
                        <div class="news-content margin-top70">
                            <div class="news-list">
                                <?php foreach($blog->result() as $b): ?>
                                        <div class="new-layout">
                                            <div class="image-wrapper">
                                                <a href="<?= $b->link ?>" class="link">
                                                    <img src="<?= $b->foto ?>" alt="" class="img-responsive">
                                                </a>
                                                <div class="description">
                                                    <?php substr(strip_tags($b->texto),0,150) ?>
                                                </div>
                                            </div>
                                            <div class="content-wrapper">
                                                <a href="<?= $b->link ?>" class="title"><?= $b->titulo ?></a>
                                                <ul class="info list-inline list-unstyled">
                                                    <li><a href="#" class="link"><?= strftime("%B %d, %Y",strtotime($b->fecha)) ?> </a></li>
                                                    <li><a href="#" class="link"><?= $b->user ?></a></li>
                                                </ul>
                                                <?php substr(strip_tags($b->texto),0,300) ?>
                                                <a href="<?= $b->link ?>" class="btn btn-maincolor">Leer mas</a>
                                                <div class="tags">
                                                    <div class="title-tag">tags:</div>
                                                    <ul class="list-inline list-unstyled list-tags">
                                                        <?php foreach(explode(',',$b->tags) as $t): ?>
                                                            <li><a href="<?= site_url('blog') ?>?direccion=<?= $t ?>" class="tag"><?= $t ?></a></li>
                                                        <?php endforeach ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                <?php endforeach ?>
                            </div>
                        </div>
            </div>
        </div>
    </section>
    <section class="banner-sale-3 new-style">
            <div class="clouds_one"></div>
            <div class="clouds_two"></div>
            <div class="container">
                <div class="text-salebox">
                    <div class="text-left">
                        <div class="sale-box">
                            <div class="sale-box-top">
                                <h2 class="number">50</h2><span class="sup-1">%</span><span class="sup-2">off</span></div>
                            <h2 class="text-sale">sale</h2></div>
                    </div>
                    <div class="text-right">
                        <h5 class="title">Epic Journeys From The Explooer</h5>
                        <p class="text">cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                            cupidatat non proident, sunt in culpa velit esse cillum dolore eu fugiat. Ut enim ad minim veniam.</p>
                        <div class="group-button"><a href="tour-view.html" class="btn btn-maincolor">book now</a><a href="tour-view.html" class="btn btn-transparent">read more</a></div>
                    </div>
                </div>
            </div>
        </section>
    <section class="a-fact padding-top padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="group-title">
                        <div class="sub-title">
                            <p class="text">Local.litza </p><i class="icons flaticon-security"style=" color: #e7237e"></i></div>
                        <h2 class="main-title">ELS NOSTRES DESTINS</h2></div>
                    <div class="a-fact-wrapper">
                        <p class="text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, by injected humour. </p>
                        <div class="a-fact-list">
                            <ul class="list-unstyled">
                                <li>
                                    <p class="text">1456 flight in the world.</p>
                                </li>
                                <li>
                                    <p class="text">2385 happy customer enjoy jouneys with Explooer.</p>
                                </li>
                                <li>
                                    <p class="text">356 best destinational we explore.</p>
                                </li>
                                <li>
                                    <p class="text">2345 package tours every year.</p>
                                </li>
                                <li>
                                    <p class="text">top 10 best tourism services.</p>
                                </li>
                            </ul>
                        </div><a href="#" class="btn btn-maincolor">veure destins</a></div>
                </div>
                <div class="col-md-7">
                    <div class="a-fact-image-wrapper">
                        <div class="a-fact-image"><a href="#" class="icons icons-1"><i class="fa fa-plane"></i></a><img src="<?= base_url() ?>img/homepage/area-1.png" alt="" class="img-responsive"></div>
                        <div class="a-fact-image"><a href="#" class="icons icons-2"><i class="fa fa-map-marker"></i></a><img src="<?= base_url() ?>img/homepage/area-2.png" alt="" class="img-responsive"></div>
                        <div class="a-fact-image"><a href="#" class="icons icons-3"><i class="fa fa-users"></i></a><img src="<?= base_url() ?>img/homepage/area-3.png" alt="" class="img-responsive"></div>
                        <div class="a-fact-image"><a href="#" class="icons icons-4"><i class="fa fa-suitcase"></i></a><img src="<?= base_url() ?>img/homepage/area-4.png" alt="" class="img-responsive"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php $this->load->view('_contacto'); ?>
</div>
