<div id="wrapper-content">
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <section class="page-banner car-rent-result">

            <div class="container">
                <div class="page-title-wrapper">
                    <div class="page-title-content">
                        <ol class="breadcrumb">
                            <li><a href="index.html" class="link home">Home</a></li>
                            <li class="active"><a href="#" class="link">Resultats de la busqueda</a></li>
                        </ol>
                        <div class="clearfix"></div>
                        <h2 class="captions">Madrid</h2></div>
                </div>
            </div>
        </section>
        <div class="page-main">
            <div class="trip-info">
            <div class="clearfix"></div>
            <div class="car-rent-result-main padding-top padding-bottom">
                <div class="container">
                    <div class="result-meta">
                        <div class="row">
                            <div class="col-lg-4 col-md-12">
                                <div class="result-count-wrapper">Viatges trobats: <span class="result-count">132</span></div>
                            </div>

                            <div class="col-lg-8 col-md-12">
                                <div class="result-filter-wrapper">
                                    <div class="result-meta row">
                                        <div class="col-md-7"><div class="result-count-wrapper"><span class="search-result-title">Showing 1-6 of 8 results  </span> </div></div><div class="col-md-5"><div class="result-filter-wrapper"><div class="result-filter">
                                                    <div class="hide slz-sort-type" data-type="tour"></div>
                                                    <label class="result-filter-label">
                                                        Sort by:	</label>
                                                    <div class="selection-bar">
                                                        <div class="select-wrapper" style="z-index: 100;">
                                                            <select class="custom-select selectbox" name="sort_by" sb="97242287" style="display: none;">
                                                                <option value="" selected="selected">- Latest -</option>
                                                                <option value="az_order">Name - Ascending</option>
                                                                <option value="za_order">Name - Descending</option>
                                                                <option value="az_start_date">Tour Date - Ascending</option>
                                                                <option value="za_start_date">Tour Date - Descending</option>
                                                                <option value="az_review_rating">Review Rating - Ascending</option>
                                                                <option value="za_review_rating">Review Rating - Descending</option>
                                                                <option value="az_discount">Discount - Low to High</option>
                                                                <option value="za_discount">Discount - High to Low</option>
                                                                <option value="az_price">Price - Low to High</option>
                                                                <option value="za_price">Price - High to Low</option>
                                                            </select>
                                                    </div>
                                                </div><div class="hide slz-archive-column" data-col="2"></div></div></div>					</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="result-body">
                        <div class="row">
                            <div class="col-md-8 main-right">
                                <div class="car-rent-list">


                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 post-155 slzexploore_tour">
                                            <div class="tours-layout">
                                                <img src="<?= base_url() ?>img/tours/lowcost.svg" alt="" class="img-responsive" style="position: absolute; z-index: 10000; width: 110px; left:-16px; top: 5px">
                                                <div class="image-wrapper">
                                                    <a href="tour-view.html" class="link">
                                                        <img src="<?= base_url() ?>img/tours/tour-1.jpg" alt="" class="img-responsive">
                                                    </a>
                                                    <div class="title-wrapper">
                                                        <a href="tour-view.html" class="title">Madrid</a>
                                                        <i class="icons flaticon-circle"></i>
                                                    </div>
                                                </div>
                                                <div class="content-wrapper">
                                                    <ul class="list-info list-inline list-unstyle">
                                                        <li><a href="#" class="link"><i class="icons fa fa-eye"></i><span class="text number">234</span></a></li>
                                                        <li><a href="#" class="link"><i class="icons fa fa-heart"></i><span class="text number">234</span></a></li>
                                                        <li><a href="#" class="link"><i class="icons fa fa-bus"></i><span class="text number">AUTOCAR</span></a></li>
                                                    </ul>
                                                    <div class="content">
                                                        <div class="title">
                                                            <div class="price"><span class="number">233,60</span><sup>€</sup></div>
                                                            <p class="for-price">5 dies 4 nits</p>
                                                        </div>
                                                        <p class="text">Lorem ipsum dolor sit amet, consectetur elit. Nulla rhoncus ultrices purus, volutpat.</p>
                                                        <div class="group-btn-tours"><a href="tour-view.html" class="left-btn">Pressupost</a><a href="blog.html" class="right-btn">Afegir a preferits</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tours-layout">
                                                <div class="image-wrapper">
                                                    <a class="link" href="http://wp.swlabs.co/exploore/tours/stockholm-city/"><img src="http://wp.swlabs.co/exploore/wp-content/uploads/2016/05/stockholm-city-400x270.jpg" class="img-responsive" alt="stockholm-city" srcset="http://wp.swlabs.co/exploore/wp-content/uploads/2016/05/stockholm-city-400x270.jpg 400w, http://wp.swlabs.co/exploore/wp-content/uploads/2016/05/stockholm-city-800x540.jpg 800w" sizes="(max-width: 400px) 100vw, 400px" width="400" height="270"></a>
                                                    <div class="title-wrapper"><a href="http://wp.swlabs.co/exploore/tours/stockholm-city/" class="title">Stockholm City</a><i class="icons flaticon-circle"></i></div>
                                                </div>
                                                <div class="content-wrapper">
                                                    <ul class="list-info list-inline list-unstyle">
                                                        <li class="view">
                                                            <a href="http://wp.swlabs.co/exploore/tours/stockholm-city/" class="link">
                                                                <i class="icons fa fa-eye"></i>
                                                                <span class="text number">2873</span>
                                                            </a>
                                                        </li>
                                                        <li class="wishlist">
                                                            <a href="http://wp.swlabs.co/exploore/shop/login/" class="link  list-wist" data-item="155">
                                                                <i class="icons fa fa-heart"></i>
                                                                <span class="text number">1</span>
                                                            </a>
                                                        </li>
                                                        <li class="share">
                                                            <a href="javascript:void(0);" class="link">
                                                                <i class="icons fa fa-share-alt"></i>
                                                            </a>
                                                            <ul class="share-social-list">
                                                                <li>
                                                                    <a href="http://www.facebook.com/sharer.php?u=http%3A%2F%2Fwp.swlabs.co%2Fexploore%2Ftours%2Fstockholm-city%2F" class="link-social" onclick="window.open(this.href, 'Share Window','left=50,top=50,width=600,height=350,toolbar=0'); return false;">
                                                                        <i class="icons fa fa-facebook"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="https://twitter.com/intent/tweet?text=Stockholm+City&amp;url=http%3A%2F%2Fwp.swlabs.co%2Fexploore%2Ftours%2Fstockholm-city%2F&amp;via=Exploore+-+Travel%2C+Exploration%2C+Booking+WordPress+Theme" class="link-social" onclick="window.open(this.href, 'Share Window','left=50,top=50,width=600,height=350,toolbar=0'); return false;">
                                                                        <i class="icons fa fa-twitter"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="http://plus.google.com/share?url=http%3A%2F%2Fwp.swlabs.co%2Fexploore%2Ftours%2Fstockholm-city%2F" class="link-social" onclick="window.open(this.href, 'Share Window','left=50,top=50,width=600,height=350,toolbar=0'); return false;">
                                                                        <i class="icons fa fa-google-plus"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="comment">
                                                            <a href="http://wp.swlabs.co/exploore/tours/stockholm-city/#comments" class="link">
                                                                <i class="icons fa fa-comment"></i>
                                                                <span class="text number">1/1</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div class="content">
                                                        <div class="title"><div class="price"><sup>$</sup><span class="number">350</span></div><p class="for-price">2 days 2 nights</p></div>
                                                        <div class="text">Lorem ipsum dolor sit amet, consectetur elit. Nulla rhoncus ultrices purus, volutpat.</div>
                                                        <div class="group-btn-tours"><a href="http://wp.swlabs.co/exploore/tours/stockholm-city/" class="btn btn-gray">Book Now</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6 col-sm-6 post-155 slzexploore_tour">
                                            <div class="tours-layout">
                                                <div class="image-wrapper">
                                                    <a class="link" href="http://wp.swlabs.co/exploore/tours/stockholm-city/"><img src="http://wp.swlabs.co/exploore/wp-content/uploads/2016/05/stockholm-city-400x270.jpg" class="img-responsive" alt="stockholm-city" srcset="http://wp.swlabs.co/exploore/wp-content/uploads/2016/05/stockholm-city-400x270.jpg 400w, http://wp.swlabs.co/exploore/wp-content/uploads/2016/05/stockholm-city-800x540.jpg 800w" sizes="(max-width: 400px) 100vw, 400px" width="400" height="270"></a>
                                                    <div class="title-wrapper"><a href="http://wp.swlabs.co/exploore/tours/stockholm-city/" class="title">Stockholm City</a><i class="icons flaticon-circle"></i></div>
                                                </div>
                                                <div class="content-wrapper">
                                                    <ul class="list-info list-inline list-unstyle">
                                                        <li class="view">
                                                            <a href="http://wp.swlabs.co/exploore/tours/stockholm-city/" class="link">
                                                                <i class="icons fa fa-eye"></i>
                                                                <span class="text number">2873</span>
                                                            </a>
                                                        </li>
                                                        <li class="wishlist">
                                                            <a href="http://wp.swlabs.co/exploore/shop/login/" class="link  list-wist" data-item="155">
                                                                <i class="icons fa fa-heart"></i>
                                                                <span class="text number">1</span>
                                                            </a>
                                                        </li>
                                                        <li class="share">
                                                            <a href="javascript:void(0);" class="link">
                                                                <i class="icons fa fa-share-alt"></i>
                                                            </a>
                                                            <ul class="share-social-list">
                                                                <li>
                                                                    <a href="http://www.facebook.com/sharer.php?u=http%3A%2F%2Fwp.swlabs.co%2Fexploore%2Ftours%2Fstockholm-city%2F" class="link-social" onclick="window.open(this.href, 'Share Window','left=50,top=50,width=600,height=350,toolbar=0'); return false;">
                                                                        <i class="icons fa fa-facebook"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="https://twitter.com/intent/tweet?text=Stockholm+City&amp;url=http%3A%2F%2Fwp.swlabs.co%2Fexploore%2Ftours%2Fstockholm-city%2F&amp;via=Exploore+-+Travel%2C+Exploration%2C+Booking+WordPress+Theme" class="link-social" onclick="window.open(this.href, 'Share Window','left=50,top=50,width=600,height=350,toolbar=0'); return false;">
                                                                        <i class="icons fa fa-twitter"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="http://plus.google.com/share?url=http%3A%2F%2Fwp.swlabs.co%2Fexploore%2Ftours%2Fstockholm-city%2F" class="link-social" onclick="window.open(this.href, 'Share Window','left=50,top=50,width=600,height=350,toolbar=0'); return false;">
                                                                        <i class="icons fa fa-google-plus"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="comment">
                                                            <a href="http://wp.swlabs.co/exploore/tours/stockholm-city/#comments" class="link">
                                                                <i class="icons fa fa-comment"></i>
                                                                <span class="text number">1/1</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div class="content">
                                                        <div class="title"><div class="price"><sup>$</sup><span class="number">350</span></div><p class="for-price">2 days 2 nights</p></div>
                                                        <div class="text">Lorem ipsum dolor sit amet, consectetur elit. Nulla rhoncus ultrices purus, volutpat.</div>
                                                        <div class="group-btn-tours"><a href="http://wp.swlabs.co/exploore/tours/stockholm-city/" class="btn btn-gray">Book Now</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="row">                                        
                                        <div class="col-md-6 col-sm-6 post-155 slzexploore_tour">
                                            <div class="tours-layout">
                                                <div class="image-wrapper">
                                                    <a class="link" href="http://wp.swlabs.co/exploore/tours/stockholm-city/"><img src="http://wp.swlabs.co/exploore/wp-content/uploads/2016/05/stockholm-city-400x270.jpg" class="img-responsive" alt="stockholm-city" srcset="http://wp.swlabs.co/exploore/wp-content/uploads/2016/05/stockholm-city-400x270.jpg 400w, http://wp.swlabs.co/exploore/wp-content/uploads/2016/05/stockholm-city-800x540.jpg 800w" sizes="(max-width: 400px) 100vw, 400px" width="400" height="270"></a>
                                                    <div class="title-wrapper"><a href="http://wp.swlabs.co/exploore/tours/stockholm-city/" class="title">Stockholm City</a><i class="icons flaticon-circle"></i></div>
                                                </div>
                                                <div class="content-wrapper">
                                                    <ul class="list-info list-inline list-unstyle">
                                                        <li class="view">
                                                            <a href="http://wp.swlabs.co/exploore/tours/stockholm-city/" class="link">
                                                                <i class="icons fa fa-eye"></i>
                                                                <span class="text number">2873</span>
                                                            </a>
                                                        </li>
                                                        <li class="wishlist">
                                                            <a href="http://wp.swlabs.co/exploore/shop/login/" class="link  list-wist" data-item="155">
                                                                <i class="icons fa fa-heart"></i>
                                                                <span class="text number">1</span>
                                                            </a>
                                                        </li>
                                                        <li class="share">
                                                            <a href="javascript:void(0);" class="link">
                                                                <i class="icons fa fa-share-alt"></i>
                                                            </a>
                                                            <ul class="share-social-list">
                                                                <li>
                                                                    <a href="http://www.facebook.com/sharer.php?u=http%3A%2F%2Fwp.swlabs.co%2Fexploore%2Ftours%2Fstockholm-city%2F" class="link-social" onclick="window.open(this.href, 'Share Window','left=50,top=50,width=600,height=350,toolbar=0'); return false;">
                                                                        <i class="icons fa fa-facebook"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="https://twitter.com/intent/tweet?text=Stockholm+City&amp;url=http%3A%2F%2Fwp.swlabs.co%2Fexploore%2Ftours%2Fstockholm-city%2F&amp;via=Exploore+-+Travel%2C+Exploration%2C+Booking+WordPress+Theme" class="link-social" onclick="window.open(this.href, 'Share Window','left=50,top=50,width=600,height=350,toolbar=0'); return false;">
                                                                        <i class="icons fa fa-twitter"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="http://plus.google.com/share?url=http%3A%2F%2Fwp.swlabs.co%2Fexploore%2Ftours%2Fstockholm-city%2F" class="link-social" onclick="window.open(this.href, 'Share Window','left=50,top=50,width=600,height=350,toolbar=0'); return false;">
                                                                        <i class="icons fa fa-google-plus"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="comment">
                                                            <a href="http://wp.swlabs.co/exploore/tours/stockholm-city/#comments" class="link">
                                                                <i class="icons fa fa-comment"></i>
                                                                <span class="text number">1/1</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div class="content">
                                                        <div class="title"><div class="price"><sup>$</sup><span class="number">350</span></div><p class="for-price">2 days 2 nights</p></div>
                                                        <div class="text">Lorem ipsum dolor sit amet, consectetur elit. Nulla rhoncus ultrices purus, volutpat.</div>
                                                        <div class="group-btn-tours"><a href="http://wp.swlabs.co/exploore/tours/stockholm-city/" class="btn btn-gray">Book Now</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6 col-sm-6 post-155 slzexploore_tour">
                                            <div class="tours-layout">
                                                <div class="image-wrapper">
                                                    <a class="link" href="http://wp.swlabs.co/exploore/tours/stockholm-city/"><img src="http://wp.swlabs.co/exploore/wp-content/uploads/2016/05/stockholm-city-400x270.jpg" class="img-responsive" alt="stockholm-city" srcset="http://wp.swlabs.co/exploore/wp-content/uploads/2016/05/stockholm-city-400x270.jpg 400w, http://wp.swlabs.co/exploore/wp-content/uploads/2016/05/stockholm-city-800x540.jpg 800w" sizes="(max-width: 400px) 100vw, 400px" width="400" height="270"></a>
                                                    <div class="title-wrapper"><a href="http://wp.swlabs.co/exploore/tours/stockholm-city/" class="title">Stockholm City</a><i class="icons flaticon-circle"></i></div>
                                                </div>
                                                <div class="content-wrapper">
                                                    <ul class="list-info list-inline list-unstyle">
                                                        <li class="view">
                                                            <a href="http://wp.swlabs.co/exploore/tours/stockholm-city/" class="link">
                                                                <i class="icons fa fa-eye"></i>
                                                                <span class="text number">2873</span>
                                                            </a>
                                                        </li>
                                                        <li class="wishlist">
                                                            <a href="http://wp.swlabs.co/exploore/shop/login/" class="link  list-wist" data-item="155">
                                                                <i class="icons fa fa-heart"></i>
                                                                <span class="text number">1</span>
                                                            </a>
                                                        </li>
                                                        <li class="share">
                                                            <a href="javascript:void(0);" class="link">
                                                                <i class="icons fa fa-share-alt"></i>
                                                            </a>
                                                            <ul class="share-social-list">
                                                                <li>
                                                                    <a href="http://www.facebook.com/sharer.php?u=http%3A%2F%2Fwp.swlabs.co%2Fexploore%2Ftours%2Fstockholm-city%2F" class="link-social" onclick="window.open(this.href, 'Share Window','left=50,top=50,width=600,height=350,toolbar=0'); return false;">
                                                                        <i class="icons fa fa-facebook"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="https://twitter.com/intent/tweet?text=Stockholm+City&amp;url=http%3A%2F%2Fwp.swlabs.co%2Fexploore%2Ftours%2Fstockholm-city%2F&amp;via=Exploore+-+Travel%2C+Exploration%2C+Booking+WordPress+Theme" class="link-social" onclick="window.open(this.href, 'Share Window','left=50,top=50,width=600,height=350,toolbar=0'); return false;">
                                                                        <i class="icons fa fa-twitter"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="http://plus.google.com/share?url=http%3A%2F%2Fwp.swlabs.co%2Fexploore%2Ftours%2Fstockholm-city%2F" class="link-social" onclick="window.open(this.href, 'Share Window','left=50,top=50,width=600,height=350,toolbar=0'); return false;">
                                                                        <i class="icons fa fa-google-plus"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="comment">
                                                            <a href="http://wp.swlabs.co/exploore/tours/stockholm-city/#comments" class="link">
                                                                <i class="icons fa fa-comment"></i>
                                                                <span class="text number">1/1</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <div class="content">
                                                        <div class="title"><div class="price"><sup>$</sup><span class="number">350</span></div><p class="for-price">2 days 2 nights</p></div>
                                                        <div class="text">Lorem ipsum dolor sit amet, consectetur elit. Nulla rhoncus ultrices purus, volutpat.</div>
                                                        <div class="group-btn-tours"><a href="http://wp.swlabs.co/exploore/tours/stockholm-city/" class="btn btn-gray">Book Now</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <nav class="pagination-list margin-top70">
                                    <ul class="pagination">
                                        <li><a href="#" aria-label="Previous" class="btn-pagination previous"><span aria-hidden="true" class="fa fa-angle-left"></span></a></li>
                                        <li><a href="#" class="btn-pagination active">01</a></li>
                                        <li><a href="#" class="btn-pagination">02</a></li>
                                        <li><a href="#" class="btn-pagination">03</a></li>
                                        <li><a href="#" class="btn-pagination">...</a></li>
                                        <li><a href="#" aria-label="Next" class="btn-pagination next"><span aria-hidden="true" class="fa fa-angle-right"></span></a></li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="col-md-4 sidebar-widget">
                                <div class="col-2">
                                    <div class="find-widget find-flight-widget widget">
                                        <h4 class="title-widgets">Busca el teu viatge</h4>
                                        <form class="content-widget">
                                            <div class="ffw-radio-selection"><span class="ffw-radio-btn-wrapper"><input type="radio" name="flight type" value="one way" id="flight-type-1" checked="checked" class="ffw-radio-btn"><label for="flight-type-1" class="ffw-radio-label">Catalunya</label></span>                                                    <span class="ffw-radio-btn-wrapper"><input type="radio" name="flight type" value="round trip" id="flight-type-2" class="ffw-radio-btn"><label for="flight-type-2" class="ffw-radio-label">España</label></span>                                                    <span class="ffw-radio-btn-wrapper"><input type="radio" name="flight type" value="multiple cities" id="flight-type-3" class="ffw-radio-btn"><label for="flight-type-3" class="ffw-radio-label">Europa</label></span>
                                                <div
                                                    class="stretch">&nbsp;</div>
                                            </div>
                                            <div class="text-input small-margin-top">
                                                <div class="text-box-wrapper"><label class="tb-label">On vols anar?</label>
                                                    <div class="input-group"><input type="text" placeholder="Write the place" class="tb-input"></div>
                                                </div>
                                                <div class="input-daterange">
                                                    <div class="text-box-wrapper half left"><label class="tb-label">Sortida</label>
                                                        <div class="input-group"><input type="text" placeholder="MM/DD/YY" class="tb-input"><i class="tb-icon fa fa-calendar input-group-addon"></i></div>
                                                    </div>
                                                    <div class="text-box-wrapper half right"><label class="tb-label">Tornada</label>
                                                        <div class="input-group"><input type="text" placeholder="MM/DD/YY" class="tb-input"><i class="tb-icon fa fa-calendar input-group-addon"></i></div>
                                                    </div>
                                                </div>
                                                <div class="text-box-wrapper half left"><label class="tb-label">Professors</label>
                                                    <div class="input-group"><button disabled="disabled" data-type="minus" data-field="quant[1]" class="input-group-btn btn-minus"><span class="tb-icon fa fa-minus"></span></button><input type="number" name="quant[1]" min="1" max="9"
                                                                                                                                                                                                        value="1" class="tb-input count"><button data-type="plus" data-field="quant[1]" class="input-group-btn btn-plus"><span class="tb-icon fa fa-plus"></span></button></div>
                                                </div>
                                                <div class="text-box-wrapper half right"><label class="tb-label">Alumnes</label>
                                                    <div class="input-group"><button disabled="disabled" data-type="minus" data-field="quant[2]" class="input-group-btn btn-minus"><span class="tb-icon fa fa-minus"></span></button><input type="number" name="quant[2]" min="0" max="9"
                                                                                                                                                                                                        value="0" class="tb-input count"><button data-type="plus" data-field="quant[2]" class="input-group-btn btn-plus"><span class="tb-icon fa fa-plus"></span></button></div>
                                                </div>
                                            </div><button type="submit" data-hover="SEND NOW" class="btn btn-slide small-margin-top"><span class="text">Buscar ara</span><span class="icons fa fa-long-arrow-right"></span></button></form>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="col-1">
                                        <div class="price-widget widget">
                                            <div class="title-widget">
                                                <div class="title">preu</div>
                                            </div>
                                            <div class="content-widget">
                                                <div class="price-wrapper">
                                                    <div data-range_min="0" data-range_max="3000" data-cur_min="450" data-cur_max="1800" class="nstSlider">
                                                        <div class="leftGrip indicator">
                                                            <div class="number"></div>
                                                        </div>
                                                        <div class="rightGrip indicator">
                                                            <div class="number"></div>
                                                        </div>
                                                    </div>
                                                    <div class="leftLabel">0</div>
                                                    <div class="rightLabel">3000</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="turkey-cities-widget widget">
                                            <div class="title-widget">
                                                <div class="title">los mas vistos</div>
                                            </div>
                                            <div class="content-widget">
                                                <form class="radio-selection">
                                                    <div class="radio-btn-wrapper"><input type="radio" name="rating" value="1stars" id="1stars" class="radio-btn"><label for="1stars" class="radio-label stars stars5">1stars</label><span class="count">27</span></div>
                                                    <div class="radio-btn-wrapper"><input type="radio" name="rating" value="2stars" id="2stars" class="radio-btn"><label for="2stars" class="radio-label stars stars4">2stars</label><span class="count">75</span></div>
                                                    <div class="radio-btn-wrapper"><input type="radio" name="rating" value="3stars" id="3stars" class="radio-btn"><label for="3stars" class="radio-label stars stars3">3stars</label><span class="count">35</span></div>
                                                    <div class="radio-btn-wrapper"><input type="radio" name="rating" value="4stars" id="4stars" class="radio-btn"><label for="4stars" class="radio-label stars stars2">4stars</label><span class="count">34</span></div>
                                                    <div class="radio-btn-wrapper"><input type="radio" name="rating" value="5stars" id="5stars" class="radio-btn"><label for="5stars" class="radio-label stars stars1">5stars</label><span class="count">65</span></div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-1">
                                        <div class="car-type-widget widget">
                                            <div class="title-widget">
                                                <div class="title">Destins</div>
                                            </div>
                                            <div class="content-widget">
                                                <form class="radio-selection">
                                                    <div class="radio-btn-wrapper"><input type="radio" name="car-type" value="Economy" id="Economy" class="radio-btn"><label for="Economy" class="radio-label">Economy</label><span class="count">27</span></div>
                                                    <div class="radio-btn-wrapper"><input type="radio" name="car-type" value="Compact" id="Compact" class="radio-btn"><label for="Compact" class="radio-label">Compact</label><span class="count">75</span></div>
                                                    <div class="radio-btn-wrapper"><input type="radio" name="car-type" value="Midsize" id="Midsize" class="radio-btn"><label for="Midsize" class="radio-label">Midsize</label><span class="count">35</span></div>
                                                    <div class="radio-btn-wrapper"><input type="radio" name="car-type" value="Standard" id="Standard" class="radio-btn"><label for="Standard" class="radio-label">Standard</label><span class="count">34</span></div>
                                                    <div class="radio-btn-wrapper"><input type="radio" name="car-type" value="Premium" id="Premium" class="radio-btn"><label for="Premium" class="radio-label">Premium</label><span class="count">65</span></div>
                                                    <div class="radio-btn-wrapper"><input type="radio" name="car-type" value="Fullsize" id="Fullsize" class="radio-btn"><label for="Fullsize" class="radio-label">Fullsize</label><span class="count">65</span></div>
                                                    <div class="radio-btn-wrapper"><input type="radio" name="car-type" value="Convertible" id="Convertible" class="radio-btn"><label for="Convertible" class="radio-label">Convertible</label><span class="count">65</span></div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="special-equipment-widget widget">
                                        <div class="title-widget">
                                            <div class="title">Tipus de viatge</div>
                                        </div>
                                        <div class="content-widget">
                                            <form class="radio-selection">
                                                <div class="radio-btn-wrapper"><input type="radio" name="equipment" value="Infant-Seat" id="Infant-Seat" class="radio-btn"><label for="Infant-Seat" class="radio-label">Infant Seat</label><span class="count">27</span></div>
                                                <div class="radio-btn-wrapper"><input type="radio" name="equipment" value="Tooldler-Seat" id="Tooldler-Seat" class="radio-btn"><label for="Tooldler-Seat" class="radio-label">Tooldler Seat</label><span class="count">75</span></div>
                                                <div
                                                    class="radio-btn-wrapper"><input type="radio" name="equipment" value="Navigation-System" id="Navigation-System" class="radio-btn"><label for="Navigation-System" class="radio-label">Navigation System</label><span class="count">35</span></div>
                                                <div
                                                    class="radio-btn-wrapper"><input type="radio" name="equipment" value="Standard2" id="Standard2" class="radio-btn"><label for="Standard2" class="radio-label">Standard</label><span class="count">34</span></div>
                                                <div class="radio-btn-wrapper"><input type="radio" name="equipment" value="Ski-Rack" id="Ski-Rack" class="radio-btn"><label for="Ski-Rack" class="radio-label">Ski Rack</label><span class="count">65</span></div>
                                            </form>
                                        </div>
                                    </div>
                                    <!--<div class="suppliers-widget widget">
                                        <div class="title-widget">
                                            <div class="title">suppliers</div>
                                        </div>
                                        <div class="content-widget">
                                            <form class="radio-selection">
                                                <div class="radio-btn-wrapper"><input type="radio" name="suppliers" value="National" id="National" class="radio-btn"><label for="National" class="radio-label">National</label><span class="count">27</span></div>
                                                <div class="radio-btn-wrapper"><input type="radio" name="suppliers" value="Dollar" id="Dollar" class="radio-btn"><label for="Dollar" class="radio-label">Dollar</label><span class="count">75</span></div>
                                                <div class="radio-btn-wrapper"><input type="radio" name="suppliers" value="Alamo" id="Alamo" class="radio-btn"><label for="Alamo" class="radio-label">Alamo</label><span class="count">35</span></div>
                                            </form>
                                        </div>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="special-offer margin-top70">
                        <h3 class="title-style-2">ofertes especials</h3>
                        <div class="special-offer-list">
                            <div class="special-offer-layout">
                                <div class="image-wrapper">
                                    <a href="<?= base_url('destinos/1-test') ?>" class="link"><img src="<?= base_url() ?>img/footer/offer-13.jpg" alt="" class="img-responsive"></a>
                                    <div class="title-wrapper"><a href="<?= base_url('destinos/1-test') ?>" class="title">alpha</a><i class="icons flaticon-circle"></i></div>
                                </div>
                            </div>
                            <div class="special-offer-layout">
                                <div class="image-wrapper">
                                    <a href="<?= base_url('destinos/1-test') ?>" class="link"><img src="<?= base_url() ?>img/footer/offer-14.jpg" alt="" class="img-responsive"></a>
                                    <div class="title-wrapper"><a href="<?= base_url('destinos/1-test') ?>" class="title">otipus</a><i class="icons flaticon-circle"></i></div>
                                </div>
                            </div>
                            <div class="special-offer-layout">
                                <div class="image-wrapper">
                                    <a href="<?= base_url('destinos/1-test') ?>" class="link"><img src="<?= base_url() ?>img/footer/offer-15.jpg" alt="" class="img-responsive"></a>
                                    <div class="title-wrapper"><a href="<?= base_url('destinos/1-test') ?>" class="title">sunrise</a><i class="icons flaticon-circle"></i></div>
                                </div>
                            </div>
                            <div class="special-offer-layout">
                                <div class="image-wrapper">
                                    <a href="<?= base_url('destinos/1-test') ?>" class="link"><img src="<?= base_url() ?>img/footer/offer-16.jpg" alt="" class="img-responsive"></a>
                                    <div class="title-wrapper"><a href="<?= base_url('destinos/1-test') ?>" class="title">carisbean</a><i class="icons flaticon-circle"></i></div>
                                </div>
                            </div>
                            <div class="special-offer-layout">
                                <div class="image-wrapper">
                                    <a href="<?= base_url('destinos/1-test') ?>" class="link"><img src="<?= base_url() ?>img/footer/offer-13.jpg" alt="" class="img-responsive"></a>
                                    <div class="title-wrapper"><a href="<?= base_url('destinos/1-test') ?>" class="title">alpha</a><i class="icons flaticon-circle"></i></div>
                                </div>
                            </div>
                            <div class="special-offer-layout">
                                <div class="image-wrapper">
                                    <a href="<?= base_url('destinos/1-test') ?>" class="link"><img src="<?= base_url() ?>img/footer/offer-14.jpg" alt="" class="img-responsive"></a>
                                    <div class="title-wrapper"><a href="<?= base_url('destinos/1-test') ?>" class="title">otipus</a><i class="icons flaticon-circle"></i></div>
                                </div>
                            </div>
                            <div class="special-offer-layout">
                                <div class="image-wrapper">
                                    <a href="<?= base_url('destinos/1-test') ?>" class="link"><img src="<?= base_url() ?>img/footer/offer-15.jpg" alt="" class="img-responsive"></a>
                                    <div class="title-wrapper"><a href="<?= base_url('destinos/1-test') ?>" class="title">sunrise</a><i class="icons flaticon-circle"></i></div>
                                </div>
                            </div>
                            <div class="special-offer-layout">
                                <div class="image-wrapper">
                                    <a href="<?= base_url('destinos/1-test') ?>" class="link"><img src="<?= base_url() ?>img/footer/offer-16.jpg" alt="" class="img-responsive"></a>
                                    <div class="title-wrapper"><a href="<?= base_url('destinos/1-test') ?>" class="title">carisbean</a><i class="icons flaticon-circle"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top" class="link"><i class="fa fa-angle-double-up"></i></a></div>
</div>
<script src="<?= base_url() ?>js/template/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="<?= base_url() ?>js/template/libs/nst-slider/js/jquery.nstSlider.min.js"></script>
<script src="<?= base_url() ?>js/template/libs/plus-minus-input/plus-minus-input.js"></script>
<script src="<?= base_url() ?>js/template/pages/sidebar.js"></script>
