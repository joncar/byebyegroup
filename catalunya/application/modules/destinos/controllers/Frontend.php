<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();            
            $this->load->library('form_validation');
        }

        public function conserge(){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->set_table('conserge')
                 ->set_subject('Conserge')
                 ->set_theme('bootstrap2')
                 ->unset_delete()
                 ->unset_list()
                 ->unset_export()
                 ->unset_print()
                 ->unset_edit()
                 ->unset_read()
                 ->unset_back_to_list()
                 ->set_rules('email','Email','required|valid_email')
                 ->required_fields_array();
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function get_categorias(){
            $this->db->limit('8');
            $categorias = $this->db->get_where('categorias_destinos');
            foreach($categorias->result() as $n=>$c){
                $categorias->row($n)->cantidad = $this->db->get_where('destinos',array('categorias_destinos_id'=>$c->id))->num_rows();
            }
            return $categorias;
        }
        
        public function index($categoria = ''){
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_crud();
            $crud->unset_jquery();            
            if(is_numeric($categoria)){
                if($categoria!=7){
                    $crud->where('categorias_destinos_id',$categoria);
                }else{
                    redirect('destinos?lowcost=2');
                }
            }
            if(!empty($_POST['fecha_desde'])){
                $crud->where('fecha_desde >=',date("Y-m-d",strtotime(str_replace("/",'-',$_POST['fecha_desde']))));
            }
            if(!empty($_POST['fecha_hasta'])){
                $crud->where('fecha_hasta <=',date("Y-m-d",strtotime(str_replace("/",'-',$_POST['fecha_hasta']))));
            }
            if(!empty($_POST['profesores'])){
                $crud->where('profesores >=',$_POST['profesores']);
            }
            if(!empty($_POST['alumnos'])){
                $crud->where('alumnos >=',$_POST['alumnos']);
            }
            if(!empty($_POST['precio_min'])){
                $crud->where('precio >=',$_POST['precio_min']);
            }
            if(!empty($_POST['precio_max'])){
                $crud->where('precio <=',$_POST['precio_max']);
            }
            if(!empty($_POST['lowcost'])){
                $crud->where('lowcost',2);
            }
            $crud->set_table('destinos');
            $crud->set_theme('crud');
            $crud->set_subject('Destino');            
            $crud->set_url('destinos/frontend/index/'.$categoria.'/');
            $crud->field_type('destino','enum',array('Europa','Catalunya','España'));
            $crud->field_type('servicios','set',array(
                '<i class="icon-journey flaticon-interface"></i> <p class="text">Tots els transports</p>',
                '<i class="icon-journey flaticon-cup"></i> <p class="text">Destins de proximitat</p>',
                '<i class="icon-journey flaticon-food-2"></i> <p class="text">Bon clima</p>',
                '<i class="icon-journey flaticon-money-1"></i> <p class="text">Convinem oci i cultura</p>',
                '<i class="icon-journey flaticon-man"></i> <p class="text">Accessible x tothom</p>',
                '<i class="icon-journey flaticon-man"></i> <p class="text">Ens entendrem!</p>'
             ));
            
            $crud->field_type('transporte','set',array(
                '<i class="icons fa fa-bus"></i><span class="text number">AUTOCAR</span>',
                '<i class="icons fa fa-plane"></i><span class="text number">AVIÓ</span>',
                '<i class="icons fa fa-ship"></i><span class="text number">BAIXELL</span>',
             ));
            
            
            $crud->unset_delete()
                    ->unset_edit()
                    ->unset_add()
                    ->unset_export()
                    ->unset_print();            
            $crud = $crud->render('','application/modules/destinos/views/');
            $crud->title = 'Destí';
            $crud->view = $categoria==5?'viatges_per_centres':'mylist';            
            $crud->categoria = $categoria;
            $crud->myscripts = get_header_crud($crud->css_files,$crud->js_files,TRUE);
            $this->loadView($crud);
        }
        
        public function read($id){
            $url = explode('-',$id);
            $id = $url[0];
            if(is_numeric($id)){
                $destinos = new Bdsource();
                $destinos->where('destinos.id',$id);
                $destinos->init('destinos',TRUE);
                $destinos->visitas++;
                $destinos->save();
                $this->destinos->link = site_url('destinos/'.toURL($this->destinos->id.'-'.$this->destinos->destinos_nombre));
                $this->destinos->portada = base_url('img/destinos/'.$this->destinos->portada);
                $this->destinos->categoria = $this->db->get_where('categorias_destinos',array('id'=>$this->destinos->categorias_destinos_id))->row()->categorias_destinos_nombre;                
                $this->destinos->fecha_desde = date("Y-m-d",strtotime(str_replace("/",'-',$this->destinos->fecha_desde)));
                $this->destinos->fecha_hasta = date("Y-m-d",strtotime(str_replace("/",'-',$this->destinos->fecha_hasta)));
                $this->db->where('(fecha_inicio >='.date("Y-m-d").' OR fecha_final <= '.date("Y-m-d").")",'',FALSE);                
                $this->db->order_by('fecha_inicio','ASC');
                $this->destinos->precios = $this->db->get_where('destinos_precios',array('destinos_id'=>$id));
                foreach($this->destinos->precios->result() as $n=>$d){
                    $this->destinos->precios->row($n)->fecha_desde = date("Y-m-d",strtotime(str_replace("/",'-',$d->fecha_inicio)));
                    $this->destinos->precios->row($n)->fecha_hasta = date("Y-m-d",strtotime(str_replace("/",'-',$d->fecha_final)));
                    $this->destinos->precios->row($n)->detalle = $this->db->get_where('destinos_precios_detalles',array('destinos_precios_id'=>$d->id));
                }
                $this->db->order_by('orden','ASC');
                $this->destinos->itinerario = $this->db->get_where('destinos_itinerario',array('destinos_id'=>$id));
                $this->db->order_by('orden','ASC');
                $this->destinos->galeria = $this->db->get_where('destinos_galeria',array('destinos_id'=>$id));
                $this->destinos->actividades = $this->db->get_where('destinos_actividades',array('destinos_id'=>$id));
                $relacionados = new Bdsource();
                $relacionados->limit = array(4); 
                $relacionados->where('categorias_destinos_id',$this->destinos->categorias_destinos_id);
                $relacionados->where('id !=',$this->destinos->id);
                $relacionados->order_by = array('id','desc');
                $relacionados->init('destinos',FALSE,'relacionados');
                foreach($this->relacionados->result() as $n=>$b){
                    $this->relacionados->row($n)->link = site_url('destinos/'.toURL($b->id.'-'.$b->destinos_nombre));
                    $this->relacionados->row($n)->portada = base_url('img/destinos/'.$b->portada);                    
                }
                
                $testimonios = new Bdsource();
                $testimonios->where('destinos_id',$this->destinos->id);
                $testimonios->init('testimonios',FALSE,'testimonios');
                
                
                $editar = array();
                if(count($url)==3 && $url[1] == 'editar' && is_numeric($url[2])){
                    $solicitud = $this->db->get_where("solicitudes",array("id"=>$url[2]));
                    if($solicitud->num_rows()>0){
                        $editar = $solicitud->row();
                    }
                }
                $this->loadView(
                    array(
                        'view'=>'frontend/detail',
                        'detail'=>$this->destinos,
                        'title'=>$this->destinos->destinos_nombre,
                        'categorias'=>$this->get_categorias(),
                        'relacionados'=>$this->relacionados,
                        'testimonios'=>$this->testimonios,
                        'editar'=>$editar
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
        
        public function comentarios(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('autor','Autor','required')
                                  ->set_rules('texto','Comentario','required')
                                  ->set_rules('blog_id','','required|numeric');
            if($this->form_validation->run()){
                $data = array();
                foreach($_POST as $n=>$p){
                    $data[$n] = $p;
                }
                $data['fecha'] = date("Y-m-d");
                $this->db->insert('comentarios',$data);
                $_SESSION['mensaje'] = $this->success('Comentario añadido con éxito <script>document.reload();</script>');
                header("Location:".base_url('blog/frontend/read/'.$_POST['blog_id']));
            }else{
                $_SESSION['mensaje'] = $this->error('Comentario no enviado con éxito');
                header("Location:".base_url('blog/frontend/read/'.$_POST['blog_id']));                
            }
        }
        
        function solicitudes($x = "",$y = ""){           
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
            $crud = new ajax_grocery_CRUD();
            $crud->set_table('solicitudes');
            $crud->set_theme('bootstrap');
            $crud->set_subject('Pressupost');
            if($crud->getParameters(FALSE) == 'edit')
            {
                $destino = $this->db->get_where("solicitudes",array("id"=>$y));
                if($destino->num_rows()>0 && is_numeric($y)){
                    redirect("destinos/".$destino->row()->destinos_id."-editar-".$y."?solicitud=1#presupuesto");
                }else{
                    redirect("presupuesto");
                }
            }
            if(empty($_SESSION['user'])){
                $crud->where('sesion',$_SESSION['tmpsesion']);
            }else{
                $crud->where('sesion',$_SESSION['user']);
            }
            $crud->set_relation('destinos_id','destinos','destinos_nombre');
            $crud->display_as('destinos_id','Destí')                    
                     ->display_as('fecha','Data de sortida')
                     ->display_as('alumnos','Alumnes')
                     ->display_as('profesores','Professors')
                    ->display_as('precio','Preu x persona')
                    ->display_as('id','#Pressupost')
                     ->display_as('actividades','Activitats')
                     ->display_as('estado','Estat');
            $crud->field_type("estado","true_false",array("0"=>"Pendiente","1"=>"Enviado"));
            $crud->set_field_upload('pdf','pdfs');
            $crud->set_relation('destinos_id','destinos','{destinos_nombre}|{disponibilidad}');
            $crud->set_rules('nombre','Nombre','required|callback_validate_captcha');
            $crud->columns('fecha','id','destino','pdf','dias','alumnos','profesores','comentario','precio','estado');
            $crud->set_url('destinos/frontend/solicitudes/');
            $crud->callback_column('destino',function($val,$row){
                list($nombre,$dias) = explode("|",$row->sf00eabc1);
                return '<a href="'.base_url('destinos/'.  toURL($row->destinos_id.'-'.$nombre)).'" target="_new">'.$nombre.'</a>';                
            });
            $crud->callback_column('dias',function($val,$row){
                list($nombre,$dias) = explode("|",$row->sf00eabc1);
                return $dias;
            });
            $crud->callback_column('precio',function($val,$row){                
                return $val.' €';
            });
            $crud->callback_column('actividades',function($val,$row){
                    $ac = explode("//",$val);
                    $str = "";
                    foreach($ac as $a){
                        $str.= "<span>".$a.", </span>";
                    }
                    return $str;
                });
            $crud->callback_after_insert(function($post,$primary){
                unset($_SESSION['captcha']);
                get_instance()->crear_pdf($primary,'F');
                get_instance()->enviar_correo($primary);
                return true;
            });
            $crud->callback_before_update(function($post,$primary){
                $pdf = $this->db->get_where('solicitudes',array('id'=>$primary))->row()->pdf;
                if(file_exists('pdfs/'.$pdf)){
                    unlink('pdfs/'.$pdf);
                }
                return $post;
            });
            $crud->callback_after_update(function($post,$primary){
                get_instance()->crear_pdf($primary,'F');
                return true;
            });
            $crud->required_fields_array();
            $crud->unset_print()
                      ->unset_export()
                      ->unset_read();
            if(empty($_POST['destinos_id'])){
                $crud->unset_add();
            }
            $crud = $crud->render();
            $crud->view = 'presupuesto';
            $crud->myscripts = get_header_crud($crud->css_files, $crud->js_files,TRUE);
            $this->loadView($crud);
        }
        
        function validate_captcha(){            
            if(empty($_SESSION['captcha'])){
                $this->load->library('recaptcha');
                if(!$this->recaptcha->recaptcha_check_answer($_POST['g-recaptcha-response'])){
                        $this->form_validation->set_message("validate_captcha","No se ha validado el captcha correctamente.");
                        return false;
                }else{
                    $_SESSION['captcha'] = 1;
                }
            }
        }
        
        function reservar(){            
            if(!empty($_SESSION['user'])){
                $this->db->select('solicitudes.*, destinos.destinos_nombre');
                $this->db->join('destinos','destinos.id = solicitudes.destinos_id');
                $solicitudes = $this->db->get_where('solicitudes',array('sesion'=>$_SESSION['user'],'estado'=>0));
                if($solicitudes->num_rows()>0){
                    $data = array(
                        'nombre'=>$this->user->nombre,
                        'apellido'=>$this->user->apellido,
                        'telefono'=>$this->user->telefono,
                        'email'=>$this->user->email,
                        'estado'=>1
                    );
                    $this->db->update('solicitudes',$data,array('sesion'=>$_SESSION['user'],'estado'=>0));
                    $_SESSION['msj'] = $this->success('Su solicitud ha sido enviada con exito, en breve nos pondremos en contacto con usted');
                    $this->enviar_correo($solicitudes);
                }else{
                    $_SESSION['msj'] = $this->error('Su lista de presupuesto se encuentra vacia, antes de realizar esta accion indiquenos cual destino quiere presupuestar <a href="'.base_url('destinos').'" class="btn btn-gray">Ir a destinos</a>');   
                }
                redirect('presupuesto');
            }else{ 
                //Mostrar formulario
                $this->db->select('solicitudes.*, destinos.destinos_nombre');
                $this->db->join('destinos','destinos.id = solicitudes.destinos_id');
                $solicitudes = $this->db->get_where('solicitudes',array('sesion'=>$_SESSION['tmpsesion'],'estado'=>0));
                if($solicitudes->num_rows()>0){
                    if(empty($_POST)){                        
                        $this->loadView(array('view'=>'checkout','detail'=>$solicitudes));
                    }else{
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('nombre','Nombre','required');
                        $this->form_validation->set_rules('apellido','Apellido','required');
                        $this->form_validation->set_rules('telefono','Telefono','required');
                        $this->form_validation->set_rules('email','Email','required|valid_email');
                        if($this->form_validation->run()){                            
                            $data = array(
                                'nombre'=>$_POST['nombre'],
                                'apellido'=>$_POST['apellido'],
                                'telefono'=>$_POST['telefono'],
                                'email'=>$_POST['email'],
                                'estado'=>1
                            );
                            $this->db->update('solicitudes',$data,array('sesion'=>$_SESSION['tmpsesion'],'estado'=>0));
                            $_SESSION['msj'] = $this->success('Su solicitud ha sido enviada con exito, en breve nos pondremos en contacto con usted');                                                        
                            $this->enviar_correo($solicitudes);
                            redirect('presupuesto');
                        }else{
                            $_SESSION['msj'] = $this->error('Por favor complete todos los datos solicitados antes de continuar');   
                            redirect(base_url('destinos/frontend/reservar'));
                        }
                    }
                }else{
                    $_SESSION['msj'] = $this->error('Su lista de presupuesto se encuentra vacia, antes de realizar esta accion indiquenos cual destino quiere presupuestar <a href="'.base_url('destinos').'" class="btn btn-gray">Ir a destinos</a>');   
                    redirect('presupuesto');
                }
            }                        
        }
        
        function crear_tabla($solicitudes){
            $columns = array("id","destinos_nombre","fecha","alumnos","profesores","actividades","precio");
            $solicitudes->row()->destinos = '<table>';
            $solicitudes->row()->destinos.= '<tr style="background:#ffdd00">';
            foreach($solicitudes->row() as $n=>$v){
                if(in_array($n,$columns)){
                    $solicitudes->row()->destinos.= '<th style="padding: 10px 20px; text-align:center">'.ucfirst(str_replace("_"," ",$n)).'</th>';
                }
            }
            $solicitudes->row()->destinos.= '</tr>';
            foreach($solicitudes->result() as $s){
                $solicitudes->row()->destinos.= '<tr>';
                foreach($s as $n=>$v){
                    if($n=='fecha'){
                        $v = date("d/m/Y",strtotime($v));
                    }
                    if($n=='actividades'){
                        $v = str_replace("//",", ",$v);
                    }
                    if(in_array($n,$columns)){
                        $solicitudes->row()->destinos.= '<td style="padding: 5px; border-top:1px solid lightgray; text-align:center">'.$v.'</td>';
                    }
                }
                $solicitudes->row()->destinos.= '</tr>';
            }
            $solicitudes->row()->destinos .= '</table>';            
            return $solicitudes;
        }
        
        function enviar_correo($solicitud){
            $solicitudes = $this->db->get_where('solicitudes',array('id'=>$solicitud));
            if($solicitudes->num_rows()>0){
                switch($solicitudes->row()->asesor){
                    case 'Anna Riba':
                        $solicitudes->row()->asesor = 'ANNA RIBA: 93 803 06 94 (Ext. 305) / 693 806 249  a.riba@byebyegroup.com'; 
                        $email = "a.riba@byebyegroup.com";
                    break;
                    case 'Emma Soler':
                        $solicitudes->row()->asesor = 'EMMA SOLER: 93 803 06 94 (Ext. 305) administracio@byebyegroup.com'; 
                        $email = "administracio@byebyegroup.com";
                   break;
                }
                $solicitudes->row()->destino = $this->db->get_where('destinos',array('id'=>$solicitudes->row()->destinos_id))->row()->destinos_nombre;
                $solicitudes->row()->fecha = date("d/m/Y",strtotime($solicitudes->row()->fecha));
                get_instance()->load->library('mailer');
                get_instance()->mailer->mail->clearAttachments();
                get_instance()->mailer->mail->AddAttachment('pdfs/'.$solicitudes->row()->pdf,$solicitudes->row()->pdf);                           
                $this->enviarcorreo($solicitudes->row(),3,$solicitudes->row()->email);
                $this->enviarcorreo($solicitudes->row(),2,$email);
            }
        }
        
        function crear_pdf($solicitud, $type = ''){
            $reporte = $this->db->get_where('notificaciones',array('id'=>4))->row();            
            $this->db->select('solicitudes.*, destinos.destinos_nombre, destinos.servicios_pdf, destinos.disponibilidad, destinos.imagen_itinerario, destinos.servicios');
            $this->db->join('destinos','destinos.id = solicitudes.destinos_id');
            $solicitud = $this->db->get_where('solicitudes',array('solicitudes.id'=>$solicitud))->row();
            $solicitud->itinerario = $this->db->get_where('destinos_itinerario',array('destinos_id'=>$solicitud->destinos_id));
            
            $solicitud->destinos_nombre = $solicitud->destinos_nombre;
            $solicitud->fecha = date("d/m/Y",strtotime($solicitud->fecha));
            $solicitud->imagen_itinerario = base_url('img/destinos/'.$solicitud->imagen_itinerario);
            $solicitud->preci = $solicitud->precio;
            $solicitud->precio = number_format($solicitud->precio,2,',','.');
            
            switch($solicitud->asesor){
                case 'Anna Riba':$solicitud->asesorfoto = base_url('img/pdf/anna.png'); break;
                case 'Emma Soler':$solicitud->asesorfoto = base_url('img/pdf/emma.png'); break;
            }        
            $reporte->texto = $this->load->view('frontend/_pdf',array('solicitud'=>$solicitud),TRUE);
            //echo $reporte->texto;
            require_once APPPATH.'libraries/html2pdf/html2pdf.php';            
            $html2pdf = new HTML2PDF('P','A4','ca',true, 'UTF-8', array(0,0,0,0));            
            $html2pdf->writeHTML($reporte->texto);
            ob_clean();
            $name = 'Pressupost-'.$solicitud->id.date("dmY").'.pdf';
            $html2pdf->addFont("NunitoSansBold"); 
            $html2pdf->addFont("NunitoSans");
            $html2pdf->addFont("neomocon");
            $html2pdf->Output('pdfs/'.$name,$type);
            //echo $reporte->texto;
            $this->db->update('solicitudes',array('pdf'=>$name),array('id'=>$solicitud->id));            
        }
        
        function crear_pdf2($solicitud,$destinos_id = '', $type = ''){
            $reporte = $this->db->get_where('notificaciones',array('id'=>4))->row();            
            $this->db->select('solicitudes.*, destinos.destinos_nombre, destinos.servicios_pdf, destinos.disponibilidad, destinos.imagen_itinerario, destinos.servicios');
            $this->db->join('destinos','destinos.id = solicitudes.destinos_id');
            $solicitud = $this->db->get_where('solicitudes',array('solicitudes.id'=>$solicitud))->row();
            $solicitud->destinos_id = $destinos_id;
            $solicitud->itinerario = $this->db->get_where('destinos_itinerario',array('destinos_id'=>$solicitud->destinos_id));
            
            $solicitud->destinos_nombre = $solicitud->destinos_nombre;
            $solicitud->fecha = date("d/m/Y",strtotime($solicitud->fecha));
            $solicitud->imagen_itinerario = base_url('img/destinos/'.$solicitud->imagen_itinerario);
            $solicitud->preci = $solicitud->precio;
            $solicitud->precio = number_format($solicitud->precio,2,',','.');
            
            switch($solicitud->asesor){
                case 'Anna Riba':$solicitud->asesorfoto = base_url('img/pdf/anna.png'); break;
                case 'Emma Soler':$solicitud->asesorfoto = base_url('img/pdf/emma.png'); break;
            }        
            $reporte->texto = $this->load->view('frontend/_pdf',array('solicitud'=>$solicitud),TRUE);
            //echo $reporte->texto;
            require_once APPPATH.'libraries/html2pdf/html2pdf.php';            
            $html2pdf = new HTML2PDF('P','A4','ca',true, 'UTF-8', array(0,0,0,0));            
            $html2pdf->writeHTML($reporte->texto);
            ob_clean();
            $name = 'Pressupost-'.$solicitud->id.date("dmY").'.pdf';
            $html2pdf->addFont("NunitoSansBold"); 
            $html2pdf->addFont("NunitoSans");
            $html2pdf->addFont("neomocon");
            $html2pdf->Output('pdfs/'.$name,$type);
            //echo $reporte->texto;
            $this->db->update('solicitudes',array('pdf'=>$name),array('id'=>$solicitud->id));            
        }
        
        function sumarCorazon($id){
            $proyecto = $this->db->get_where('destinos',array('id'=>$id));
            if($proyecto->num_rows()>0){
                $likes = empty($proyecto->row()->likes)?array():json_decode($proyecto->row()->likes);
                $ip = getUserIP();
                if(!in_array($ip,$likes)){
                    $likes[] = $ip;
                }else{
                    foreach($likes as $n=>$v){
                        if($v==$ip){
                            unset($likes[$n]);
                        }
                    }
                }
                $count = count($likes);
                $this->db->update('destinos',array('likes'=>json_encode($likes)),array('id'=>$id));
                echo $count;
            }else{
                echo 'fail';
            }
        }        
    }
?>
