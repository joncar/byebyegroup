<div class="main-content">
    <section class="page-banner homepage-default">
        <div class="container">
            <div class="homepage-banner-warpper">
                <div class="homepage-banner-content">
                    <div class="group-title">
                        <h1 class="title titlemain">Experts</h1>
                        <p class="text">EN VIATGES DE FINAL DE CURS

                        </p>
                    </div>
                    <div class="group-btn">
                        <a href="<?= site_url('destinos') ?>" data-hover="FES CLICK" class="btn-click">
                            <span class="text">Explora i gaudeix</span>
                            <span class="icons fa fa-long-arrow-right"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    
    <section class="tours padding-top padding-bottom" style="">
        <div class="container">
            <div class="tours-wrapper">
                <div class="group-title">
                    <div class="sub-title">
                        <p class="text">Viatges</p><i class="icons flaticon-people" style=" color: #e7237e"></i></div>
                    <h2 class="main-title">nacionals</h2></div>
                <div class="tours-content margin-top70">
                    <?php $this->db->limit(3) ?>
                         <div class="tours-list">
                            <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>2))->result() as $d): ?>
                                <?php $this->load->view('_destino',array('d'=>$d)); ?>
                            <?php endforeach ?>
                        </div>
                    <div class="tours-list" style="margin-top:120px">
                        <?php $this->db->limit(3,3) ?>
                        <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>2))->result() as $d): ?>
                                <?php $this->load->view('_destino',array('d'=>$d)); ?>
                        <?php endforeach ?>
                    </div>
                    <a href="<?= site_url('destinos/pack/3') ?>" class="btn btn-maincolor margin-top70" style=" margin-top: 120px">més viatges</a>
                </div>
            </div>
        </div>
    </section>

    
    <section class="tours padding-top padding-bottom" style="background: url(<?= base_url() ?>img/destinos/a0c5e-mejores-destinos1.jpg) 0% 0% / cover; top: -127px; margin-bottom: -187px; margin-top:80px;">
        <div class="patterndestino"></div>
        <div class="container" style="transform: translate(1px, 1px);">
            <div class="tours-wrapper">
                <div class="group-title">
                    <div class="sub-title">
                        <p class="text"  style="color:#fff">viatges</p><i class="icons flaticon-transport-1"style=" color: #e7237e"></i></div>
                    <h2 class="main-title" style="color:#fff">internacionals</h2></div>
                <div class="tours-content margin-top70">
                    <div class="tours-list">
                        <?php $this->db->limit(3) ?>
                        <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>4))->result() as $d): ?>
                                <?php $this->load->view('_destino',array('d'=>$d)); ?>
                        <?php endforeach ?>                        
                    </div>                    
                    <div class="tours-list" style="margin-top:120px">
                        <?php $this->db->limit(3,3) ?>
                        <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>4))->result() as $d): ?>
                                <?php $this->load->view('_destino',array('d'=>$d)); ?>
                        <?php endforeach ?>
                    </div>
                    
                    <a href="<?= site_url('destinos/pack/4') ?>" class="btn btn-maincolor margin-top70">Demanar pressupost</a></div>
            </div>
        </div>
    </section>
    
    
    <section class="hotels padding-top padding-bottom">
        <div class="container">
            <div class="tours-wrapper">
                <div class="group-title">
                    <div class="sub-title">
                        <p class="text">VIATGES I PACKS </p><i class="icons flaticon-sport"style=" color: #e7237e"></i></div>
                    <h2 class="main-title">AVENTURA I ESPECIALS</h2></div>
                <div class="tours-content margin-top70">
                    <?php $this->db->limit(3) ?>
                         <div class="tours-list">
                            <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>3))->result() as $d): ?>
                                <?php $this->load->view('_destino',array('d'=>$d)); ?>
                            <?php endforeach ?>
                        </div>
                    <div class="tours-list" style="margin-top:120px">
                        <?php $this->db->limit(3,3) ?>
                        <?php foreach($this->db->get_where('destinos',array('categorias_destinos_id'=>3))->result() as $d): ?>
                                <?php $this->load->view('_destino',array('d'=>$d)); ?>
                        <?php endforeach ?>
                    </div>
                    <a href="<?= site_url('destinos/pack/2') ?>" class="btn btn-maincolor margin-top70">més viatges</a>
                </div>
            </div>
        </div>
    </section>
    <section class="travelers">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="traveler-wrapper padding-top padding-bottom">
                        <div class="group-title white">
                            <div class="sub-title">
                                <p class="text">Recomanacions dels</p><i class="icons flaticon-people-2"style=" color: #e7237e"></i></div>
                            <h2 class="main-title">Professors</h2></div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="traveler-list">
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/homepage/cover-image-1.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/homepage/avatar-1.jpg" alt="" class="img-responsive"></div>
                                <p class="name">Tutors de 4t d'ESO</p>
                                <p class="address">INS Castellbisbal</p>
                                <p class="description">"Hem realitzat un viatge d'estudis de final de 4t d'ESO a San Sebastián amb Finalia. Ha estat una boníssima experiència, les activitats programades, l'hotel on ens hem allotjat  molt ben dotat i amb una atenció impecable, fins i tot ens ha acompanyat el bon temps. Moltes gràcies a la gent de Finalia per la seva professionalitat tant en allò que estava programat com en la resolució d'imprevistos i problemes"</p>
                            </div>
                        </div>
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/homepage/cover-image-2.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/homepage/avatar-2.jpg" alt="" class="img-responsive"></div>
                                <p class="name">Tutors de 4t d'ESO</p>
                                <p class="address">Escola L'Oreig</p>
                                <p class="description">"Aquest curs i per segon any amb  els alumnes de 4 d'ESO hem  anat a Cantàbria amb FINALIA, una organització molt professional  que cuida tots els detalls, desde el primer moment  fins l´últim . Ha sigut una molt bona experiència. Els monitors de primera , han sapigut tractar als alumnes perque gaudissin i s'emportessin un bon record, tot això lligat amb l'estada en  un hotel amb instal.lacions adecuades i un personal encantador"</p>
                            </div>
                        </div>
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/homepage/cover-image-3.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/homepage/avatar-3.jpg" alt="" class="img-responsive"></div>
                                <p class="name">Tutors de 4t d'ESO</p>
                                <p class="address">Inst. La Serreta</p>
                                <p class="description">"L'experiència de viatjar a Mallorca amb Finalia ha estat fantàstica! Sense entrebancs ni ensurts, cosa que s'agraeix quan es viatja amb un grup nombrós d'adolescents. Molt contents amb la professionalitat i servei, tant del personal l'hotel,  dels transfers, del vaixell i dels  monitors de les activitats. L'estança ha estat divertida amb activitats culturals i esportives inclusives i engrescadores. Tothom el tornaríem a repetir!""</p>
                            </div>
                        </div>
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/homepage/cover-image-4.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/homepage/avatar-4.jpg" alt="" class="img-responsive"></div>
                                <p class="name">Tutors de 4t d'ESO</p>
                                <p class="address">FEDAC Guissona</p>
                                <p class="description">"Cantàbria en 4 dies és possible? la resposta és rotundament sí.
                                    Han estat 4 dies de llibertat, natura, mar, sol, esport,
                                    muntanya, felicitat i bon humor. Tan els alumnes com nosaltres hem gaudit moltíssim, també del menjar i del dormir. No us perdeu el far de Santander,
                                    els pics d'Europa, el surf i les canoes, la playa de los locos, passejar a la nit per Suances i matinar molt per veure sortir el sol. Un viatge de 10 al que afegiríem, això sí, un parell de dies més."</p>
                            </div>
                        </div>
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/homepage/cover-image-5.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/homepage/avatar-5.jpg" alt="" class="img-responsive"></div>
                                <p class="name">Tutors de Batxillerat</p>
                                <p class="address">Escola Diocesana de Navàs</p>
                                <p class="description">"Roma en 5 dies, amb 10 estudiants de Batxillerat i 2 professores. Una petita aventura que gràcies a l’atenció de Finalia vam dur a terme molt feliçment.
                                    Vam sortir de bon matí, però això ens va permetre aprofitar molt bé el primer dia de viatge. A les 8 ja aterràvem a Roma i començava un dia ple d’aventures i reconeixement de la ciutat.
                                    Roma queda en el nostre record com una destinació on ens hem pogut conèixer millor, però també com una ciutat interessant.
                                    Gràcies per fer-ho possible."</p>
                            </div>
                        </div>
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/homepage/cover-image-6.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/homepage/avatar-6.jpg" alt="" class="img-responsive"></div>
                                <p class="name">Tutors de 4t d’ESO</p>
                                <p class="address">INS Fax</p>
                                <p class="description">"El nostres alumnes van gaudir molt de totes les activitats d’aventura (tir amb arc, escalada, kayac). Una organització impecable  i uns monitors molt engrescadors. Repetirem segur!
                                    "</p>
                            </div>
                        </div>
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/homepage/cover-image-7.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/homepage/avatar-7.jpg" alt="" class="img-responsive"></div>
                                <p class="name">Tutors de l’escola </p>
                                <p class="address">La Gaspar</p>
                                <p class="description">"L'Escola Municipal d'Art i Disseny d'Igualada "La Gaspar" ha anat de viatge de fi de cicle a Berlín i a la Bauhaus.
                                    Hem triat aquestes destinacions perquè pels nostres estudis en els cicles formatius són un referent cultural imprescindible.
                                    Ha sigut una experiència inoblidable poder passejar-nos per l'escola de la Bauhaus i descobrir llocs emblemàtics de Berlín.
                                    Agraïm a Finalia-ByeByeGroup totes les gestions fetes per a poder fer realitat aquest viatge.
                                    "</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="news padding-top padding-bottom">
        <div class="container">
            <div class="news-wrapper">
                <div class="group-title">
                    <div class="sub-title">
                        <p class="text">EXPLORA LES NOSTRES HISTORIES</p><i class="icons flaticon-summer"style=" color: #e7237e"></i></div>
                    <h2 class="main-title">BLOG</h2></div>
                        <div class="news-content margin-top70">
                            <div class="news-list">
                                <?php foreach($blog->result() as $b): ?>
                                        <div class="new-layout">
                                            <div class="image-wrapper">
                                                <a href="<?= $b->link ?>" class="link">
                                                    <img src="<?= $b->foto ?>" alt="" class="img-responsive">
                                                </a>
                                            </div>
                                            <div class="content-wrapper">
                                                <a href="<?= $b->link ?>" class="title"><?= $b->titulo ?></a>
                                                <ul class="info list-inline list-unstyled">
                                                    <li><a href="#" class="link"><?= strftime("%B %d, %Y",strtotime($b->fecha)) ?> </a></li>
                                                    <li><a href="#" class="link"><?= $b->user ?></a></li>
                                                </ul>
                                                <p class="text"><?= substr(strip_tags($b->texto),0,500) ?></p>
                                                <?php substr(strip_tags($b->texto),0,300) ?>
                                                <a href="<?= $b->link ?>" class="btn btn-maincolor">Lleguir més</a>
                                                <div class="tags">
                                                    <div class="title-tag">tags:</div>
                                                    <ul class="list-inline list-unstyled list-tags">
                                                        <?php foreach(explode(',',$b->tags) as $t): ?>
                                                            <li><a href="<?= site_url('blog') ?>?direccion=<?= $t ?>" class="tag"><?= $t ?></a></li>
                                                        <?php endforeach ?>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                <?php endforeach ?>
                            </div>
                        </div>
            </div>
        </div>
    </section>
    <section class="videos layout-1">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="video-wrapper padding-top padding-bottom">
                        <h5 class="sub-title">Els millors <strong>finals </strong> de curs</h5>
                        <h2 class="title">explora ara</h2>
                        <div class="text">
                            Mira el nostre video i veuràs que apostem per destins que combinen una part cultural amb visites als llocs més emblemàtics de cada regió, i  una part lúdica amb divertides activitats d’aventura que encantaran als alumnes.
                        </div>
                        <a href="<?= site_url('p/nosotros') ?>" class="btn btn-maincolor">llegir més</a>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="video-thumbnail">
                        <div class="video-bg"><img src="<?= base_url() ?>img/homepage/video-bg.jpg" alt="" class="img-responsive"></div>
                        <div class="video-button-play"> <i class="icons fa fa-play"></i></div>
                        <div class="video-button-close"></div>
                        <iframe src="https://www.youtube.com/embed/7Iiw_NqzxN4?rel=0" allowfullscreen="allowfullscreen" class="video-embed"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php $this->load->view('_contacto'); ?>
</div>
<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
    window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
    _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="https://v2.zopim.com/?560oMeEimgaeT4NAgVG2vZSj9buasjpu";z.t=+new Date;$.
            type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->