        <!-- LOADING JS FOR PAGE-->
        <script src="<?= base_url() ?>js/template/pages/home-page.js"></script>
        <script src="<?= base_url() ?>js/template/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script>
    function sumarCorazon(id){
        $.post('<?= base_url('destinos/frontend/sumarCorazon') ?>/'+id,{},function(data){
            $('#likes'+id).html('<i class="icons fa fa-heart"></i><span class="text number">'+data+'</span>');
        });
    }
    
    
    function addToWish(id){
        <?php if(empty($_SESSION['user'])): ?>
                document.location.href="<?= base_url('registro/index/add') ?>?redirect=main/index";
        <?php else: ?>
                $.post('<?= base_url() ?>destinos/backend/wishlist/insert',{destinos_id:id,user_id:<?= $this->user->id ?>},function(data){
                    $("#wish"+id).addClass('active');
                });
        <?php endif ?>
    }
    
    $(document).on("ready",function(){        
        $('.select2').select2();
    });
</script>
<script>
    function subscribir(form){
      var email = $(form).find('input[type="email"]').val();
      $.post('<?= base_url('paginas/frontend/subscribir') ?>',{email:email},function(data){
          $("#subscralert,#footsubscralert").html(data);
      });
      return false;
    }
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119821433-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-119821433-1');
</script>
