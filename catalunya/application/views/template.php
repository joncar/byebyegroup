<!DOCTYPE html>
<html lang="en">

    <head>
        <title><?= empty($title)?'ByeByeGroup':$title ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Agencia de Viajes elegida por excelencia entre los Centros Educativos y Asociaciones de Madres
        y Padres de Alumnos de su localidad. Destinos que combinan una parte cultural">
        <meta name="keywords" content="Viajes de fin de curso, ESO, colegios, viajes, fin, curso, cultura">
        <!-- FONT CSS-->
        <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,900">
        <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Montserrat:400,700">
        <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,900" rel="stylesheet">
        
        <link type="text/css" rel="stylesheet" href="<?= base_url() ?>css/template/scripts.css">

        <script src="<?= base_url() ?>js/template/libs/jquery/jquery-2.2.3.min.js"></script>
        <link rel="shortcut icon" href="<?= base_url('img/favicon.png') ?>">
        
        
        <!-- LIBRARY JS-->                                                                                        
        <script src='<?= base_url() ?>js/template/scripts.js'></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <link type="text/css" rel="stylesheet" href="<?= base_url() ?>js/template/libs/fancybox/css/jquery.fancybox.css">
        <link type="text/css" rel="stylesheet" href="<?= base_url() ?>js/template/libs/fancybox/css/jquery.fancybox-buttons.css">
        <link type="text/css" rel="stylesheet" href="<?= base_url() ?>js/template/libs/fancybox/css/jquery.fancybox-thumbs.css">
        <!-- MAIN JS-->
        <script src="<?= base_url() ?>js/template/main.js"></script>
        <?php if(!empty($myscripts)) echo $myscripts ?>
    </head>

    <body>
        <div class="body-wrapper">
            <!-- MENU MOBILE-->
            <div class="wrapper-mobile-nav">                
                <div class="header-main">
                    <?php $this->load->view('includes/template/menu_mobile'); ?>
                </div>
            </div>
            <!-- WRAPPER CONTENT-->
            <div class="wrapper-content">
            <!-- HEADER-->
            <?php $this->load->view('includes/template/header'); ?>
            <!-- WRAPPER-->
            <div id="wrapper-content">
                <!-- MAIN CONTENT-->
                <?php $this->load->view($view); ?>
            <!-- BUTTON BACK TO TOP-->
                <div id="back-top"><a href="#top" class="link"><i class="fa fa-angle-double-up"></i></a></div>
            </div>
            <!-- FOOTER-->
            <?php $this->load->view('includes/template/footer'); ?>
            </div>
        </div>        
        <?php $this->load->view('includes/template/scripts'); ?>
    </body>

</html>
