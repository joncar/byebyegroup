<!DOCTYPE html>
<html lang="en">

    <head>
        <title><?= empty($title)?'ByeByeGroup':$title ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Agencia de Viajes elegida por excelencia entre los Centros Educativos y Asociaciones de Madres
        y Padres de Alumnos de su localidad. Destinos que combinan una parte cultural">
        <meta name="keywords" content="Viajes de fin de curso, ESO, colegios, viajes, fin, curso, cultura">

        <!-- FONT CSS-->
        <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700,900">
        <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Montserrat:400,700">
        <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,900" rel="stylesheet">
        
        <link type="text/css" rel="stylesheet" href="<?= base_url() ?>css/template/scripts.css">

        <script src="<?= base_url() ?>js/template/libs/jquery/jquery-2.2.3.min.js"></script>
        <link rel="shortcut icon" href="<?= base_url('img/favicon.png') ?>">
        
        
        <!-- LIBRARY JS-->                                                                                        
        <script src='<?= base_url() ?>js/template/scripts.js'></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <!-- MAIN JS-->
        <script src="<?= base_url() ?>js/template/main.js"></script>
        <?php if(!empty($myscripts)) echo $myscripts ?>
    </head>

    <body>
        <div class="body-wrapper" style="background: url(../../img/intro.jpg); background-size:cover;">            
            <div class="wrapper-content">            
            <?php $this->load->view('includes/template/header'); ?>            
            <div id="wrapper-content">                
                <?php $this->load->view($view); ?>
            </div>
            </div>
        </div>        
        <?php $this->load->view('includes/template/scripts'); ?>
    </body>
    <div id="barracookies">
        Usamos cookies propias y de terceros que recogen datos de sus hábitos de navegación y realizar análisis de uso de nuestro sitio.
        <br/>
        Si continúa navegando consideramos que acepta su uso.<br/>
        <a href="javascript:void(0);" onclick="var expiration = new Date(); expiration.setTime(expiration.getTime() + (60000*60*24*365)); setCookie('avisocookies','1',expiration,'/');document.getElementById('barracookies').style.display='none';">
            <b style="font-family: 'Montserrat', sans-serif">
                OK
            </b>
        </a>
    </div>
    <!-- Estilo barra CSS -->
    <style>#barracookies {display: none;z-index: 99999;position:fixed;left:0px;right:0px;bottom:0px;width:100%;min-height:40px;font-weight: normal;padding:5px;background: #333333;color:white;line-height:20px;font-family: 'Montserrat', sans-serif; font-weight: lighter;font-size:12px;text-align:center;box-sizing:border-box;} #barracookies a:nth-child(3) {padding:4px;padding-left10px; padding-right10px; background:#e22013;border-radius:2px;text-decoration:none;} #barracookies a {color: #fff;text-decoration: none;}</style>
    <!-- Gestión de cookies-->
    <script type='text/javascript'>function setCookie(name,value,expires,path,domain,secure){document.cookie=name+"="+escape(value)+((expires==null)?"":"; expires="+expires.toGMTString())+((path==null)?"":"; path="+path)+((domain==null)?"":"; domain="+domain)+((secure==null)?"":"; secure")}function getCookie(name){var cname=name+"=";var dc=document.cookie;if(dc.length>0){begin=dc.indexOf(cname);if(begin!=-1){begin+=cname.length;end=dc.indexOf(";",begin);if(end==-1)end=dc.length;return unescape(dc.substring(begin,end))}}return null}function delCookie(name,path,domain){if(getCookie(name)){document.cookie=name+"="+((path==null)?"":"; path="+path)+((domain==null)?"":"; domain="+domain)+"; expires=Thu, 01-Jan-70 00:00:01 GMT"}}</script>
    <!-- Gestión barra aviso cookies -->
    <script type='text/javascript'>
        var comprobar = getCookie("avisocookies");
        if (comprobar != null) {}
        else {
            var expiration = new Date();
            expiration.setTime(expiration.getTime() + (60000*60*24*365));
            setCookie("avisocookies","1",expiration);
            document.getElementById("barracookies").style.display="block";

            window.addEventListener('load',function(){
                setTimeout(function(){
                    $(".edgtf-login-register-holder").toggle('modal');
                },3000);
            });
        }
    </script>
    </div> <!-- close div.edgtf-wrapper-inner  -->
    </div> <!-- close div.edgtf-


</html>
