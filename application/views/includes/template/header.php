<header>
    <div class="bg-transparent header-01">
        <div class="header-topbar">
            <div class="container">
                <ul class="topbar-left list-unstyled list-inline pull-left">
                    <li>
                        <a href="void(0)" class="monney dropdown-text">
                            <i class="topbar-icon fa fa fa-phone "></i>
                            <span> 902 002 068</span>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/pg/byebyegroup/photos/?ref=page_internal" class="link  facebook" target="_blank">
                            <i class="fa fa-facebook"></i>
                        </a> 
                    </li>
                    <li>
                        <a href="https://twitter.com/byebyegroup" class="link  twitter" target="_blank">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="http://stalkture.com/p/byebyegroup/3680527830/" class="link instagram" target="_blank">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="header-main">
            <div class="container">
                <div class="navbar-header">
                        <div class="logo">
                            <a href="<?= site_url() ?>" class="header-logo" style="color:white">
                                <img src="<?= base_url() ?>img/logo/logo.svg" alt=""  < />
                                <h2 class="main-title">Selecciona la zona donde resides</h2>
                            </a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</header>
