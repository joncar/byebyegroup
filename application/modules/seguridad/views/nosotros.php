<div id="wrapper-content">
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <section class="page-banner about-us-page">
            <div class="container">
                <div class="page-title-wrapper">
                    <div class="page-title-content">
                        <ol class="breadcrumb">
                            <li><a href="index.html" class="link home">Home</a></li>
                            <li class="active"><a href="#" class="link">nosaltres</a></li>
                        </ol>
                        <div class="clearfix"></div>
                        <h2 class="captions">Qui som?</h2></div>
                </div>
            </div>
        </section>
        <section class="about-us layout-2 padding-top padding-bottom about-us-4">
            <div class="container">
                <div class="row">
                    <div class="wrapper-contact-style">
                        <div class="col-lg-6 col-md-8">
                            <h3 class="title-style-2">Per què viatjar amb nosaltres?</h3>
                            <div class="about-us-wrapper">
                                <p class="text">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                                    duis aute irure dolor quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute.</p>
                                <p class="text">Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
                                    duis aute irure dolor.</p>
                                <div class="group-list">
                                    <ul class="list-unstyled about-us-list">
                                        <li>
                                            <p class="text">First Class Flights</p>
                                        </li>
                                        <li>
                                            <p class="text">5 Star Accommodations</p>
                                        </li>
                                        <li>
                                            <p class="text">Inclusive Packages</p>
                                        </li>
                                        <li>
                                            <p class="text">Latest Model Vehicles</p>
                                        </li>
                                    </ul>
                                    <ul class="list-unstyled about-us-list">
                                        <li>
                                            <p class="text">Handpicked Hotels</p>
                                        </li>
                                        <li>
                                            <p class="text">Accesibility managment</p>
                                        </li>
                                        <li>
                                            <p class="text">10 Languages available</p>
                                        </li>
                                        <li>
                                            <p class="text">+120 Premium city tours</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div data-wow-delay="0.4s" class="about-us-image wow zoomInRight"><img src="<?= base_url() ?>img/homepage/about-us-4.png" alt="" class="img-responsive"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="videos padding-top padding-bottom page-our-values">
            <div class="container">
                <h3 class="title-style-2 white style=" color:="" black"="" style="color: white;">Per què viatjar amb nosaltres?</h3>
                <div class="row">
                    <div class="our-wrapper">
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/1.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">GARANTIA FINALIA </p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur elit.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/2.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">AGENT PERSONALITZAT</p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur elit.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/3.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">TRANSPORTS </p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur elit.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/4.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">ALLOTJAMENTS</p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur elit.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="our-wrapper">
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/10.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">SUPORT TELEFÒNIC</p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur elit.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/6.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title"> VIATGE AMB MONITORS </p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur elit.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/7.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">PROMOCIONS </p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur elit.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/8.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">RESERVA ANTICIPADA</p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur eli</p>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/9.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">ASSEGURANCES</p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur eli</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/11.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">TRACTE DIRECTE</p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur eli</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="our-expert padding-top padding-bottom-50">
            <div class="container">
                <h3 class="title-style-2">El nostres experts</h3>
                <div class="wrapper-expert">
                    <div class="item content-expert">
                        <a href="#" class="img-expert"><img src="<?= base_url() ?>img/homepage/about-5.jpg" alt="" class="img-responsive img"></a>
                        <div class="caption-expert"><a href="#" class="title">Mark letto</a>
                            <p class="text">Manager Tour Guide</p>
                            <ul class="social list-inline">
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-facebook"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-twitter"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item content-expert">
                        <a href="#" class="img-expert"><img src="<?= base_url() ?>img/homepage/about-6.jpg" alt="" class="img-responsive img"></a>
                        <div class="caption-expert"><a href="#" class="title">Mark letto</a>
                            <p class="text">Manager Tour Guide</p>
                            <ul class="social list-inline">
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-facebook"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-twitter"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item content-expert">
                        <a href="#" class="img-expert"><img src="<?= base_url() ?>img/homepage/about-7.jpg" alt="" class="img-responsive img"></a>
                        <div class="caption-expert"><a href="#" class="title">Mark letto</a>
                            <p class="text">Manager Tour Guide</p>
                            <ul class="social list-inline">
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-facebook"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-twitter"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item content-expert">
                        <a href="#" class="img-expert"><img src="<?= base_url() ?>img/homepage/about-8.jpg" alt="" class="img-responsive img"></a>
                        <div class="caption-expert"><a href="#" class="title">Mark letto</a>
                            <p class="text">Manager Tour Guide</p>
                            <ul class="social list-inline">
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-facebook"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-twitter"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item content-expert">
                        <a href="#" class="img-expert"><img src="<?= base_url() ?>img/homepage/about-7.jpg" alt="" class="img-responsive img"></a>
                        <div class="caption-expert"><a href="#" class="title">Mark letto</a>
                            <p class="text">Manager Tour Guide</p>
                            <ul class="social list-inline">
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-facebook"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-twitter"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item content-expert">
                        <a href="#" class="img-expert"><img src="<?= base_url() ?>img/homepage/about-8.jpg" alt="" class="img-responsive img"></a>
                        <div class="caption-expert"><a href="#" class="title">Mark letto</a>
                            <p class="text">Manager Tour Guide</p>
                            <ul class="social list-inline">
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-facebook"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-twitter"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="about-tours padding-top padding-bottom">
            <div class="container">
                <div class="wrapper-tours">
                    <div class="content-icon-tours">
                        <div class="content-tours"><i class="icon flaticon-people"></i>
                            <div class="wrapper-thin"><span class="wrapper-icon-thin"><i class="icon-thin fa fa-circle-thin"></i></span>
                                <div class="tours-title">3750</div>
                            </div>
                            <div class="text">Alumnes Feliços</div>
                        </div>
                        <div class="content-tours"><i class="icon flaticon-suitcase"></i>
                            <div class="wrapper-thin"><span class="wrapper-icon-thin"><i class="icon-thin fa fa-circle-thin"></i></span>
                                <div class="tours-title">7740</div>
                            </div>
                            <div class="text">Destins</div>
                        </div>
                        <div class="content-tours"><i class="icon flaticon-two"></i>
                            <div class="wrapper-thin"><span class="wrapper-icon-thin"><i class="icon-thin fa fa-circle-thin"></i></span>
                                <div class="tours-title">850</div>
                            </div>
                            <div class="text">Hotels</div>
                        </div>
                        <div class="content-tours"><i class="icon flaticon-transport"></i>
                            <div class="wrapper-thin"><span class="wrapper-icon-thin"><i class="icon-thin fa fa-circle-thin"></i></span>
                                <div class="tours-title">140</div>
                            </div>
                            <div class="text">Centres escolars</div>
                        </div>
                        <div class="content-tours"><i class="icon flaticon-drink"></i>
                            <div class="wrapper-thin"><span class="wrapper-icon-thin"><i class="icon-thin fa fa-circle-thin"></i></span>
                                <div class="tours-title">8960</div>
                            </div>
                            <div class="text">Viatges fets</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="wrapper-open-position padding-top padding-bottom">
            <div class="container">
                <div class="wrapper-position">
                    <h3 class="title-style-2">Prestacions de serveis</h3>
                    <div class="content-position">
                        <div class="row">
                            <div class="col-md-6 col-sm-10 col-xs-10 main-right">
                                <div class="content-open">
                                    <div class="main-position">
                                        <div class="img-position">
                                            <a href="#" class="img-open"><img src="<?= base_url() ?>img/background/bg-team-open.jpg" alt="" class="img-responsive"></a>
                                        </div>
                                        <ul class="list-info list-unstyled">
                                            <li><a class="link"><i class="icon fa fa-facebook"></i></a></li>
                                            <li><a class="link"><i class="icon fa fa-twitter"></i></a></li>
                                            <li><a class="link"><i class="icon fa fa-tumblr"></i></a></li>
                                            <li><a class="link"><i class="icon fa fa-linkedin"></i></a></li>
                                            <li><a class="link"><i class="icon fa fa-behance"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="wrapper-text-excel">
                                        <div class="text-excel"> Observacions generals</div>
                                        <ul class="list-text list-unstyled">
                                            <li><a class="link-text"></i><span class="text-title">RÈGIMS</span></a>
                                                <p class="text">S.A. Només allotjament – A.D. Allotjament i esmorzar – M.P Mitja pensió – P.C Pensió completa – S.P Segons programa.</p>
                                            </li>
                                            <li><a class="link-text"></i><span class="text-title">PREU</span></a>
                                                <p class="text">El preu del viatge combinat ha estat calculat segons els tipus de canvi, tarifes de transport, costos del carburant i taxes d'impostos aplicables al moment de la consulta i confirmació de la reserva. Qualsevol variació del preu dels citats elements, podrà donar lloc a la revisió del preu final del viatge.</p>
                                            </li>
                                               <!-- <ul>
                                                    <li><span>Create assets for the Zendesk website</span></li>
                                                    <li><span>Concept ideas around the Zendesk brand</span></li>
                                                    <li><span>Design and e xecute Zendesk online advertising</span></li>
                                                    <li><span>Design presentations and printed materials</span></li>
                                                </ul>-->
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-10 col-xs-10">
                                <div class="group-list group-number">
                                    <ul class="list-unstyled about-us-list">
                                        <li><span class="text">Assistència mèdica i Espanya</span><span class="text-number">3</span></li>
                                        <li><span class="text">Repatriació o transport de ferits i/o malalts</span><span class="text-number">1</span></li>
                                        <li><span class="text">Repatriació d’acompanyant</span><span class="text-number">4</span></li>
                                        <li><span class="text">Repatriació o transport de menors</span><span class="text-number">6</span></li>
                                        <li><span class="text">Desplaçament d’un familiar en cas d’hospitalizació superior a cinc dies</span><span class="text-number">2</span></li>
                                        <li><span class="text">Gastos d’allotjament a l’estranger (amb un màxim de 10 dies)</span><span class="text-number">1</span></li>
                                        <li><span class="text">Convalencia en hotel (amb un màxim de 10 dies)</span><span class="text-number">12</span></li>
                                        <li><span class="text">Repatriació o transport de l’assegurat mort</span><span class="text-number">3</span></li>
                                        <li><span class="text">Búsqueda, localització i enviament d’equipatge extraviat</span><span class="text-number">1</span></li>
                                        <li><span class="text">Robatori i danys materials de l’equipatge</span><span class="text-number">4</span></li>
                                        <li><span class="text">	Transmissió de missatges urgents</span><span class="text-number">6</span></li>
                                        <li><span class="text">Retorn anticipat per mort o hospitalització superior a dos dies d’un familiar de fins a segon grau</span><span class="text-number">2</span></li>
                                        <li><span class="text">Assegurança d’Equipatges AXA</span><span class="text-number">1</span></li>
                                        <li><span class="text">Assegurança de Responsabilitat Civil AXA</span><span class="text-number">12</span></li>
                                    </ul>
                                </div>
                                <div class="wrapper-llc">
                                    <div class="llc-title">On estem?</div>
                                    <div class="text">Create your travel agency, lodge, club, blog or destination website the quick and easy way.</div>
                                    <ul class="list-llc list-unstyled">
                                        <li><i class="icon fa fa-map-marker"></i><a href="#" class="item">132, My Street, Kingston, New York </a></li>
                                        <li><i class="icon fa fa-phone"></i>
                                            <a href="#" class="item">
                                                <p class="ph-number">(+1) 369-258-147</p>
                                                <p class="ph-number">(+1) 369-268-157</p>
                                            </a>
                                        </li>
                                        <li><i class="icon fa fa-envelope-o"></i><a href="#" class="item">recruit@explooer.com</a></li>
                                    </ul>
<!--                                </div><a href="#" class="view-more"><span class="more">View our company page</span></a></div>-->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--<section class="about-banner">
            <div class="container">
                <h3 class="title-style-2">OUR INVESTORS RELATIONS</h3>
                <div class="wrapper-banner">
                    <div class="content-banner">
                        <a href="#" class="img-banner"><img src="<?/*= base_url() */?>img/logo/about-banner-1.png" alt="" class="img-responsive"></a>
                        <a href="#" class="img-banner"><img src="<?/*= base_url() */?>img/logo/about-banner-4.png" alt="" class="img-responsive"></a>
                    </div>
                    <div class="content-banner">
                        <a href="#" class="img-banner"><img src="<?/*= base_url() */?>img/logo/about-banner-2.png" alt="" class="img-responsive"></a>
                        <a href="#" class="img-banner"><img src="<?/*= base_url() */?>img/logo/about-banner-5.png" alt="" class="img-responsive"></a>
                    </div>
                    <div class="content-banner">
                        <a href="#" class="img-banner"><img src="<?/*= base_url() */?>img/logo/about-banner-3.png" alt="" class="img-responsive"></a>
                        <a href="#" class="img-banner"><img src="<?/*= base_url() */?>img/logo/about-banner-6.png" alt="" class="img-responsive"></a>
                    </div>
                    <div class="content-banner">
                        <a href="#" class="img-banner"><img src="<?/*= base_url() */?>img/logo/about-banner-4.png" alt="" class="img-responsive"></a>
                        <a href="#" class="img-banner"><img src="<?/*= base_url() */?>img/logo/about-banner-1.png" alt="" class="img-responsive"></a>
                    </div>
                    <div class="content-banner">
                        <a href="#" class="img-banner"><img src="<?/*= base_url() */?>img/logo/about-banner-5.png" alt="" class="img-responsive"></a>
                        <a href="#" class="img-banner"><img src="<?/*= base_url() */?>img/logo/about-banner-2.png" alt="" class="img-responsive"></a>
                    </div>
                    <div class="content-banner">
                        <a href="#" class="img-banner"><img src="<?/*= base_url() */?>img/logo/about-banner-6.png" alt="" class="img-responsive"></a>
                        <a href="#" class="img-banner"><img src="<?/*= base_url() */?>img/logo/about-banner-3.png" alt="" class="img-responsive"></a>
                    </div>
                </div>
            </div>
        </section>-->
        <?php $this->load->view('_contacto'); ?>
    </div>
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top" class="link"><i class="fa fa-angle-double-up"></i></a></div>
</div>
<!--- About page ---->
<script src="<?= base_url() ?>js/template/pages/about-us.js"></script>
<script src="<?= base_url() ?>js/template/libs/nst-slider/js/jquery.nstSlider.min.js"></script>
<script src="<?= base_url() ?>js/template/libs/plus-minus-input/plus-minus-input.js"></script>
