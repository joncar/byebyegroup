<page id="pagina1">
    <div style="background:url(<?= base_url('img/pdf/bg1.jpg') ?>); width:797px; height:1122px;">
        <div style="position:absolute; top:640px; width:795px; text-align: center; font-size:30px; font-family: nunitosansbold; font-weight:bold;">VIATGE DE FI DE CURS A <?= strtoupper($solicitud->destinos_nombre) ?></div>
        <div style="position:absolute; top:720px; width:795px; text-align: center; font-size:14px; font-family: nunitosans">REFERÈNCIA DEL PRESSUPOST: <?= $solicitud->referencia ?></div>
        <div style="position: absolute; rotate:45; text-align: center; font-size: 28px; font-family: neomocon; top: 970px; font-weight: bold; color: white; width: 170px; left: 590px;">
             Beneficia't <br>de la Reserva Anticipada
        </div>
    </div>
</page>
<page id="pagina2">
    <div style="background:url(<?= base_url('img/pdf/bg44.jpg') ?>); width:795px; height:1120px; font-family: nunitosans">
        <div style="width:795px; height:300px;">            
            <div style="width:795px; height:190px; background:url(<?= $solicitud->imagen_itinerario ?>); background-position:center"></div>
            <div style="position:absolute; top: 150px; left:45px; font-family: nunitosans; color:white;">                
                <span style="font-family:neomocon; font-weight: bold; font-size: 32px; margin-top: 6px;"><?= $solicitud->destinos_nombre ?></span>
                <span style="font-size: 12px;  margin-left: 5px">DES DE</span>
                <span style="font-family:nunitosansbold; font-weight: bold; font-size: 32px;  color: #eb2c72;"><?= str_replace(',00','',$solicitud->precio) ?>&euro;</span>
            </div>

            <div style="idth:795px; position: absolute; top:220px; left:30px; font-size:12px;">

                <?php foreach ($solicitud->itinerario->result() as $p): ?>
                    <div style="position:relative; height:150px;">
                        <div style="position: absolute; top: 0px; background: #d4d4d4 none repeat scroll 0% 0%; width: 1px; left: 20px; height: 180px;"></div>
                        <div style="position:absolute; top:0px; left:10px;  border: 2px solid #ffdd00;"><span style="background-color: #ffdd00;"><?= $p->etiqueta ?></span></div>
                        <div style="width: 400px; position: absolute; left: 70px; top:20px;">
                            <span style="font-family:nunitosansbold; font-weight: bold"><?= $p->lugar ?></span><br><br/>
                            <span><?= $p->descripcion ?></span>                        
                        </div>
                        <div style="width: 300px; position: absolute; left: 510px; top: 0px;">
                            <img style="width:220px;" alt="" src="<?= base_url() ?>img/destinos/<?= $p->foto ?>">
                        </div>
                    </div>
                <?php endforeach ?>

            </div>
        </div>
    </div>
</page>
<page id="pagina3">
    <div style="background:url(http://byebyegroup.com/new/img/pdf/bg3.jpg); width:797px; height:1122px; font-family: nunitosans">
        <div style="position: absolute; width: 795px; text-align: left; font-size: 68px; top: 30px; left: 45px; font-family: neomocon;"><?= $solicitud->destinos_nombre ?></div>
        <div style="position: absolute; width: 795px; text-align: left; font-size: 18px; left: 45px; top: 90px;"><?= $solicitud->disponibilidad ?></div>
        <div style="width: 795px; position: absolute; top: 180px; left: 45px;">DATA DEL PRESSUPOST:<span style="font-family:nunitosansbold; font-size: 14px"> <?= date("d/m/Y") ?></span></div>

        <div style="position: absolute; width: 225px; top: 165px; left: 537px; text-decoration: underline; font-size: 15px; font-family: nunitosansbold;">DADES DEL TEU ASSESOR</div>
        
        
        <div style="top: 190px; width: 410px; left: 525px; position: relative;">

            <img style="width: 60px; position: absolute; top: 10px; left: 0px;" src="<?= $solicitud->asesorfoto ?>">
            <div style="position: absolute; left: 65px; top: 10px; font-size: 12px;">
                <?php 
                    switch($solicitud->asesor):
                    case 'Anna Riba': ?>
                    <span style="font-family:nunitosansbold;">Anna Riba</span><br>
                    Catalunya Occidental<br>
                    693806249<br>
                    a.riba@byebyegroup.com<br>
                <?php break; ?>
                <?php 
                    case 'Emma Soler': ?>
                    <span style="font-family:nunitosansbold;">Emma Soler</span><br>
                    Catalunya Oriental<br>
                    93 8030694(Ext. 305)<br>
                    administracio@byebyegroup.com<br>
                <?php break; ?>
                <?php endswitch; ?>
            </div>
        </div>
        

        <div style="width: 795px; position: absolute; top: 160px; left: 45px;">REFERÈNCIA DEL PRESSUPOST: <span style="font-family:nunitosansbold; font-size: 14px"> <?= $solicitud->referencia ?></span></div>
        
        <div style="width: 795px; position: absolute; font-size: 15px; top: 246px; left: 45px; font-family: nunitosansbold; text-decoration: underline;">DADES DEL CLIENT</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 270px; left: 45px;">Institut</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 270px; left: 190px; font-family: nunitosansbold"><?= $solicitud->instituto ?></div>
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 292px;">Localitat</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 295px; left: 190px; font-family: nunitosansbold"><?= $solicitud->localidad ?></div>
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 316px;">Nivell del curs</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 319px; left: 190px; font-family: nunitosansbold"><?= $solicitud->curso ?></div>
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 340px;">Nom</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 343px; left: 190px; font-family: nunitosansbold"><?= $solicitud->nombre ?></div>
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 366px;">Email</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 369px; left: 190px; font-family: nunitosansbold"><?= $solicitud->email ?></div>
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 392px;">Telèfon</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 395px; left: 190px; font-family: nunitosansbold"><?= $solicitud->telefono ?></div>
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 420px;">Càrrec</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 423px; left: 190px; font-family: nunitosansbold"><?= $solicitud->cargo ?></div>
        
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 450px;">Comentari</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 453px; left: 190px; font-family: nunitosansbold"><?= $solicitud->comentario ?></div>
        
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 590px; font-family: nunitosansbold; text-decoration: underline;">DADES DEL TEU VIATGE</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 612px; left: 45px;">Ciutat de sortida</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 615px; left: 190px; font-family: nunitosansbold"><?= $solicitud->ciudad ?></div>

        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 638px;">Circuit de destí</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 641px; left: 190px; font-family: nunitosansbold"><?= $solicitud->destinos_nombre ?></div>
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 666px;">Data de sortida</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 669px; left: 190px; font-family: nunitosansbold"><?= $solicitud->fecha ?></div>
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 691px;">Nº Estudiants</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 694px; left: 190px; font-family: nunitosansbold"><?= $solicitud->alumnos ?></div>
        <div style="width: 795px; position: absolute; font-size: 15px; left: 45px; top: 719px;">Nª Professors</div>
        <div style="width: 795px; position: absolute; font-size: 15px; top: 721px; left: 190px; font-family: nunitosansbold"><?= $solicitud->profesores ?></div>
        <div style="position:absolute; left:31px; top:670px; width:498px; height:422px">
            <img src="<?= base_url() ?>img/pdf/icon1.png" style="position: absolute; top: 130px; width: 52px;">
            <img src="<?= base_url() ?>img/pdf/icon2.png" style="position: absolute; width: 48px; top: 176px;">
            <img src="<?= base_url() ?>img/pdf/icon3.png" style="position: absolute; width: 52px; top: 220px;">
            <img src="<?= base_url() ?>img/pdf/icon4.png" style="position: absolute; width: 52px; top: 268px;">
            <img src="<?= base_url() ?>img/pdf/icon5.png" style="position: absolute; width: 52px; top: 320px;">
            <img src="<?= base_url() ?>img/pdf/icon6.png" style="position: absolute; width: 52px; top: 365px;">
        </div>
        <div style="position: absolute; left: 31px; top: 765px; width: 500px; height: 422px;">
            <div style="margin: 15px 30px 30px 80px;">
                <div style="font-family:nunitosansbold; text-decoration: underline;font-size: 18px">SERVEIS INCLOSOS</div><br>
                <div style="font-family:nunitosans;  font-size: 11px">També li recordem tots els serveis que estan inclosos en el pressupost:<br><br>
                    • Autocar durant tot el recorregut amb recollida i retorn..<br><br>

                    • Allotjament en la categoria i règim seleccionat durant la vostra estada.<br><br>

                    • Distribució en habitacions múltiples, abans de la sortida farem el rooming-list.<br><br>

                    • Assegurança d'assistència en viatge i responsabilitat civil (RC).<br><br>

                    • Gratuïtats per als professors acompanyants segons ràtio.<br><br><br>

                    • Visites i entrades incloses inidicadas en cada itinerari.<br><br>

                    • Monitors per a la coordinació i realització d'activitats detallades.<br><br>

                    • Obsequis per a tots els estudiants participants, es lliurarà a l'inici <br>del viatge. <br><br>

                    • Assessorament i gestió del circuit per part de la nostra Agència de Viatges.<br><br>

                    • 1 Talonari gratis per cada estudiant per finançar-200 € del viatge.
                    <?php foreach(explode(',',$solicitud->servicios_pdf) as $s): ?>
                        <div>
                            <?= $s ?>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</page>





<page id="pagina4">
    <div style="background:url(http://byebyegroup.com/new/img/pdf/bg9.jpg); width:795px; height:1120px; font-family: nunitosans">
        <div style="position: absolute; width: 795px; text-align: left; font-size: 48px; top: 30px; left: 45px; font-family: neomocon;"><?= $solicitud->destinos_nombre ?></div>
        <div style="position: absolute; width: 795px; text-align: left; font-size: 18px; left: 45px; top: 90px;"><?= $solicitud->disponibilidad ?></div>
        <div style="padding-left: 45px; margin-top: 250px; margin-bottom: 10px; font-family: nunitosansbold; font-size: 24px;"><u>ACTIVITATS INCLOSES</u></div>
        <div style="padding-left:45px; ">
            <table style="font-size:13px">
                <tbody>
                    <tr style="background:#ffdd00">
                        <th style="padding: 5px; text-align:center; width:200px">Activitat</th>
                        <th style="padding: 5px; text-align:center">Preu</th>
                        <th style="padding: 5px; text-align:center">Transport</th>
                        <th style="padding: 5px; text-align:center">Durada</th>
                        <th style="padding: 5px; text-align:center">Assegurança</th>
                        <th style="padding: 5px; text-align:center">Guia</th>
                        <th style="padding: 5px; text-align:center">Menú</th>
                        <th style="padding: 5px; text-align:center">Monitors</th>
                    </tr>
                    <?php
                    $extras = 0;
                    foreach (explode('//', $solicitud->actividades)as $e):
                        foreach ($this->db->get_where('destinos_actividades', array('destinos_id',$solicitud->destinos_id,'actividad' => $e, 'precio <' => 0))->result() as $a):
                            $a->transporte = $a->transporte ? 'SI' : 'NO';
                            $a->duracion = $a->duracion ? 'SI' : 'NO';
                            $a->seguro = $a->seguro ? 'SI' : 'NO';
                            $a->guia = $a->guia ? 'SI' : 'NO';
                            $a->menu = $a->menu ? 'SI' : 'NO';
                            $a->monitores = $a->monitores ? 'SI' : 'NO';
                            ?>
                            <tr>
                                <td style="padding: 5px; border-top:1px solid lightgray; word-wrap:break-word;"><?= $a->actividad ?></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"> <span>Inclosa</span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->transporte ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->duracion ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->seguro ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->guia ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->menu ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->monitores ?><span class="hide"></span></td>
                            </tr>
                        <?php endforeach ?>
                        <?php 
                            $actividad = $this->db->get_where('destinos_actividades', array('destinos_id',$solicitud->destinos_id,'actividad' => $e, 'precio >=' => 0));
                            if($actividad->num_rows()>0){
                                $extras+= $actividad->row()->precio;
                            }
                            
                        ?>
                            
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>        
        <div style="padding-left:45px; ">
            <span style="font-family:nunitosansbold">PREU DE L'ITINERARI BASE:</span>
            <span style="width: 795px; text-align: left; font-size: 28px"><?= (double)$solicitud->precio-$extras ?> &euro;</span>
        </div>
        <div style="padding-left: 45px; margin-top: 51px; margin-bottom: 10px; font-family: nunitosansbold; font-size: 24px;"><u>ACTIVITATS EXTRES SEL.LECCIONADES</u></div>
        <div style="padding-left:45px; ">
            <table style="font-size:13px">
                <tbody>
                    <tr style="background:#ffdd00">
                        <th style="padding: 5px; text-align:center; width:200px">Activitat</th>
                        <th style="padding: 5px; text-align:center">Preu</th>
                        <th style="padding: 5px; text-align:center">Transport</th>
                        <th style="padding: 5px; text-align:center">Durada</th>
                        <th style="padding: 5px; text-align:center">Assegurança</th>
                        <th style="padding: 5px; text-align:center">Guia</th>
                        <th style="padding: 5px; text-align:center">Menú</th>
                        <th style="padding: 5px; text-align:center">Monitors</th>
                    </tr>
<?php
$total = 0;
foreach (explode('//', $solicitud->actividades)as $e):
    foreach ($this->db->get_where('destinos_actividades', array('destinos_id',$solicitud->destinos_id,'actividad' => $e, 'precio >=' => 0))->result() as $a):
        $a->transporte = $a->transporte ? 'SI' : 'NO';
        $a->duracion = $a->duracion ? 'SI' : 'NO';
        $a->seguro = $a->seguro ? 'SI' : 'NO';
        $a->guia = $a->guia ? 'SI' : 'NO';
        $a->menu = $a->menu ? 'SI' : 'NO';
        $a->monitores = $a->monitores ? 'SI' : 'NO';
        $total += (double) $a->precio;
        ?>
                            <tr>
                                <td style="padding: 5px; border-top:1px solid lightgray;"><?= $a->actividad ?></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"> <span><?= str_replace(',00','',number_format($a->precio, 2, ',', '.')) ?></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->transporte ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->duracion ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->seguro ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->guia ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->menu ?><span class="hide"></span></td>
                                <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->monitores ?><span class="hide"></span></td>
                            </tr>
    <?php endforeach ?>
<?php endforeach ?>
                </tbody>
            </table>
        </div>
        <div style="padding-left:45px; ">
            <span style="font-family:nunitosansbold">PREU FINAL AMB ACTIVITATS EXTRAS:</span>
            <span style="width: 795px; text-align: left; font-size: 28px"><?= str_replace(',00','',$solicitud->precio) ?> &euro;</span>
        </div>
        
        <div style="top: 70px; width: 410px; left: 525px; position: absolute;">

            <img style="width: 60px; position: absolute; top: 10px; left: 0px;" src="<?= $solicitud->asesorfoto ?>">
            <div style="position: absolute; left: 65px; top: 10px; font-size: 12px;">
                <?php 
                    switch($solicitud->asesor):
                    case 'Anna Riba': ?>
                    <span style="font-family:nunitosansbold;">Anna Riba</span><br>
                    Catalunya Occidental<br>
                    693806249<br>
                    a.riba@byebyegroup.com<br>
                <?php break; ?>
                <?php 
                    case 'Emma Soler': ?>
                    <span style="font-family:nunitosansbold;">Emma Soler</span><br>
                    Catalunya Oriental<br>
                    93 8030694(Ext. 305)<br>
                    administracio@byebyegroup.com<br>
                <?php break; ?>
                <?php endswitch; ?>
            </div>
        </div>
    </div>
</page>


<page id="pagina5">
    <div style="background:url(http://byebyegroup.com/new/img/pdf/bg8.jpg); width:795px; height:1120px; font-family: nunitosans">
        <div style="position: absolute; width: 795px; text-align: left; font-size: 48px; top: 30px; left: 45px; font-family: neomocon;"><?= $solicitud->destinos_nombre ?></div>
        <div style="position: absolute; width: 795px; text-align: left; font-size: 18px; left: 45px; top: 90px;"><?= $solicitud->disponibilidad ?></div>
        
        
        <div style="margin-top: 290px; margin-left:45px; margin-right: 45px; font-family: nunitosansbold; font-size: 24px;">
            <u>PREU FINAL DEL VIATGE I MÈTODE DE RESERVA</u>
        </div>
        <div style="margin-top: 20px; margin-left:45px; margin-right: 45px;">
<?php $total = $total + (double) $solicitud->precio ?>
            <div><span style="font-family:nunitosans">Total Nº de alumnes:</span> <span style="font-family:nunitosansbold"> <?= $solicitud->alumnos ?></span></div>
            <div><span style="font-family:nunitosans">Total Nº de professors/acompanyants:</span> <span style="font-family:nunitosansbold"> <?= $solicitud->profesores ?></span></div><br>
            <div><span style="font-family:nunitosans">Data 1r pagament:</span> <span style="font-family:nunitosansbold">Reserva de plaçes i bloqueig del circuit</span></div>
            <div><span style="font-family:nunitosans">Data 2n pagament: </span> <span style="font-family:nunitosansbold">30 díes abans de la sortida </span></div><br/>
            <div style="background:#ffdd00;  border: 10px solid #ffdd00; width: auto"><span style="font-family:nunitosansbold; font-size:24px;">PVP TOTAL per alumne <?= str_replace(',00','',$solicitud->precio) ?> &euro;</span></div>
            <br/>
            <div>
                <span style="font-family:nunitosans">Primer pagament (10%): </span><span style="font-family:nunitosansbold"><?= str_replace(',00','',number_format(((double)$solicitud->precio * 10) / 100, 2, ',', '.')) ?> &euro;</span><br/>
                <span style="font-family:nunitosans">Segon pagament (90%):</span>  <span style="font-family:nunitosansbold"><?= str_replace(',00','',number_format(((double)$solicitud->precio * 90) / 100, 2, ',', '.')) ?> &euro;</span>
            </div>
            <br/>
<?php $total = $total * $solicitud->alumnos ?>
            <div><span style="font-family:nunitosans;text-decoration: underline;">PVP TOTAL per grup</span><span style="font-family:nunitosansbold; text-decoration: underline;"> <?= number_format((double)$solicitud->precio*$solicitud->alumnos, 2, ',', '.') ?> &euro;</span></div><br/>
            <div>
                <span style="font-family:nunitosans">Primer pagament (10%):</span> <span style="font-family:nunitosansbold"><?= str_replace(',00','',number_format(((double)$solicitud->precio*$solicitud->alumnos * 10) / 100, 2, ',', '.')) ?> &euro;</span><br/>
                <span style="font-family:nunitosans">Segon pagament (90%):</span> <span style="font-family:nunitosansbold"><?= str_replace(',00','',number_format(((double)$solicitud->precio*$solicitud->alumnos * 90) / 100, 2, ',', '.')) ?> &euro;</span></div>
        </div>
        <div style="margin-top: 60px; margin-left:45px; margin-right: 45px;">
            Els centres poden fer el pagament mitjançant transferècia bancaria o a les nostres oficines,<br>només és necesari indicar en el concepte el número de pressupost y el nom del centre.
        </div>
        <div style="top: 70px; width: 410px; left: 525px; position: absolute;">

            <img style="width: 60px; position: absolute; top: 10px; left: 0px;" src="<?= $solicitud->asesorfoto ?>">
            <div style="position: absolute; left: 65px; top: 10px; font-size: 12px;">
                <?php 
                    switch($solicitud->asesor):
                    case 'Anna Riba': ?>
                    <span style="font-family:nunitosansbold;">Anna Riba</span><br>
                    Catalunya Occidental<br>
                    693806249<br>
                    a.riba@byebyegroup.com<br>
                <?php break; ?>
                <?php 
                    case 'Emma Soler': ?>
                    <span style="font-family:nunitosansbold;">Emma Soler</span><br>
                    Catalunya Oriental<br>
                    93 8030694(Ext. 305)<br>
                    administracio@byebyegroup.com<br>
                <?php break; ?>
                <?php endswitch; ?>
            </div>
        </div>
    </div>
</page>


<page id="pagina6">
    <div style="background:url(http://byebyegroup.com/new/img/pdf/bg9.jpg); width:795px; height:1120px; font-family: nunitosans">
        <div style="position: absolute; width: 795px; text-align: left; font-size: 48px; top: 30px; left: 45px; font-family: neomocon;"><?= $solicitud->destinos_nombre ?></div>
        <div style="position: absolute; width: 795px; text-align: left; font-size: 18px; left: 45px; top: 90px;"><?= $solicitud->disponibilidad ?></div>
        <div style="padding-left:45px;  margin-top:161px; margin-bottom: 40px; font-size: 18px"><u>ACTIVIDADES EXTRES</u></div>
        <div style="padding-left:45px; ">
            <table style="font-size:13px">
                <tbody>
                    <tr style="background:#ffdd00">
                        <th style="padding: 5px; text-align:center">Activitat</th>
                        <th style="padding: 5px; text-align:center">Preu</th>
                        <th style="padding: 5px; text-align:center">Transport</th>
                        <th style="padding: 5px; text-align:center">Durada</th>
                        <th style="padding: 5px; text-align:center">Assegurança</th>
                        <th style="padding: 5px; text-align:center">Guia</th>
                        <th style="padding: 5px; text-align:center">Menú</th>
                        <th style="padding: 5px; text-align:center">Monitors</th>
                    </tr>
<?php
foreach ($this->db->get_where('destinos_actividades', array('destinos_id' => $solicitud->destinos_id, 'precio >=' => 0))->result() as $a):
    $a->transporte = $a->transporte ? 'SI' : 'NO';
    $a->duracion = $a->duracion ? 'SI' : 'NO';
    $a->seguro = $a->seguro ? 'SI' : 'NO';
    $a->guia = $a->guia ? 'SI' : 'NO';
    $a->menu = $a->menu ? 'SI' : 'NO';
    $a->monitores = $a->monitores ? 'SI' : 'NO';
    ?>
                        <tr>
                            <td><?= $a->actividad ?></td>
                            <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"> <span><?= str_replace(',00','',number_format($a->precio, 2, ',', '.')) ?></span></td>
                            <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->transporte ?><span class="hide"></span></td>
                            <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->duracion ?><span class="hide"></span></td>
                            <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->seguro ?><span class="hide"></span></td>
                            <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->guia ?><span class="hide"></span></td>
                            <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->menu ?><span class="hide"></span></td>
                            <td style="padding: 5px; border-top:1px solid lightgray; text-align:center"><?= $a->monitores ?><span class="hide"></span></td>
                        </tr>
<?php endforeach ?>
                </tbody>
            </table>
        </div>
        <div style="top: 70px; width: 410px; left: 525px; position: absolute;">

            <img style="width: 60px; position: absolute; top: 10px; left: 0px;" src="<?= $solicitud->asesorfoto ?>">
            <div style="position: absolute; left: 75px; top: 10px; font-size: 12px;">
                <?php 
                    switch($solicitud->asesor):
                    case 'Anna Riba': ?>
                    <span style="font-family:nunitosansbold;">Anna Riba</span><br>
                    Catalunya Occidental<br>
                    693806249<br>
                    a.riba@byebyegroup.com<br>
                <?php break; ?>
                <?php 
                    case 'Emma Soler': ?>
                    <span style="font-family:nunitosansbold;">Emma Soler</span><br>
                    Catalunya Oriental<br>
                    93 8030694(Ext. 305)<br>
                    administracio@byebyegroup.com<br>
                <?php break; ?>
                <?php endswitch; ?>
            </div>
        </div>
    </div>
</page>

<page id="pagina7">
    <div style="background:url(http://byebyegroup.com/new/img/pdf/bg6.jpg); width:795px; height:1120px;">
        <div style="position: absolute; width: 795px; text-align: left; font-size: 48px; top: 30px; left: 45px; font-family: neomocon;"><?= $solicitud->destinos_nombre ?></div>
        <div style="position: absolute; width: 795px; text-align: left; font-size: 18px; left: 45px; top: 90px;"><?= $solicitud->disponibilidad ?></div>
        <div style="padding-left:45px; margin-top:161px; margin-bottom: 40px; font-size: 18px; ">CONDICIONS GENERALS</div>
        <div style="margin-left:45px; margin-right:240px;font-size: 11px">
            <span style="font-family:nunitosansbold; text-decoration: underline;"><b>ORGANITZACIÓ</b></span><br/><br/>
            L’organització tècnica dels viatges ha sigut realitzada per Finalia Viajes S.L. (ByeBye Group), agència de viatges minorista majorista amb CIF B65847170, domicili al C/ Girona, 34, 08700 d’Igualada (Barcelona) i amb títol-llicència número GC-002578 atorgat per la Generalitat de Catalunya.<br/><br/>
            <span style="font-family:nunitosansbold; text-decoration: underline;"><b>PAGAMENT I RESERVA </b></span><br/><br/>
            En realitzar la reserva s’abonarà un 10% del pressupost final i la resta s’abonarà 30 dies abans de la sortida del grup.  La confirmació de la reserva s’efectuarà en el moment de l’entrega dels bonus i documentació final del viatge que conformen el contracte. En cas que el viatge sigui internacional la reserva
            serà d’un 40%, ja que ja que s'ha de procedir a la reserva i pagament dels vols.<br/><br/>
            <span style="font-family:nunitosansbold; text-decoration: underline;"><b>PREU</b></span><br/><br/>
            El preu del viatge combinat ha estat calculat segona els tipus de canvi, tarifes de transport, costos del carburant i taxes d’impostos aplicables al moment de la consulta i confirmació de la reserva.  Qualsevol variació del preus dels citats elements, podrà donar lloc a la revisió del preu final del viatge.<br><br>
            ELS NOSTRES PREUS INCLOUEN<br>
            1. El transport d’anada i tornada (quan aquest servei està inclòs en el contracte i segons les seves especificacions).<br>
            2. Allotjament en habitacions múltiples pels estudiants (habitacions individuals pels acompanyants, sempre que hi hagi disponibilitat i en mesura del possible) i pensió alimentària segons el règim contractat (S.A. Només allotjament – A.D. Allotjament i esmorzar – M.P. Mitja pensió – P.C. Pensió completa – S.P. Segons programa).<br>
            3 Assitència durant el viatge.<br>
            4. Tots aquells serveis i complements especificats concretament als itineraris corresponents.<br>
            5. Els impostos indirectes (IVA, I.G.I.C.) quan aquests siguin aplicables.  Els preus establerts a l’itinerari s’han calculat en base als impostos e IVA/I.G.I. C. vigents a 15/8/2012, i qualsevol modificació dels mateixos serà repercutida al preu final del viatge.<br><br>
            ELS NOSTRES PREUS NO INCLOUEN<br>
            Vols,  taxes d’aeroports, taxes d’hotels, certificats de vacunació, extres als hotels, règims alimentaris especials, serveis de bugaderia, serveis opcionals dels allotjaments (lloguer de tv...) i, en general, qualsevol altre servei que no s’especifiqui expressament com a inclòs.<br/><br/>
            <span style="font-family:nunitosansbold; text-decoration: underline;"><b>NOTES IMPORTANTS SOBRE ELS ALLOTJAMENTS </b></span><br/><br/>
            Al realitzar la reserva és imprescindible fer la declaració correcta del nombre de persones que s’allotjaran a l’establiment, sent aquesta responsabilitat del client.  A l’arribada s’abonarà la corresponent fiança per respondre als desperfectes que es puguin ocasionar. L’horari habitual d’entrada i sortida dels allotjaments és en funció del
            primer i últim servei que l’usuari utilitzi.  Com a norma general, les habitacions podran ser utilitzades a partir de les 12 h del dia d’arribada i hauran de quedar lliures abans de les 12 h. del dia de sortida.<br/><br/>
            <span style="font-family:nunitosansbold; text-decoration: underline;"><b>TRANSPORTS</b></span><br/><br/>
            El mitjà de transport durant el nostre trajecte haurem de mantenir-lo cuidat.
            <br/><br/>
            <span style="font-family:nunitosansbold; text-decoration: underline;"><b>DOCUMENTACIÓ</b></span><br/><br/>
            La documentació que és necessària portar pels viatges d’estudiants és el DNI en vigor o PASSAPORT en vigor, i recomanable la Targeta Sanitària.  Pel que fa als viatges internacionals, serà necessària també l’autorització paternal en cas que l’estudiant sigui menor d’edat.<br/><br/>
            <span style="font-family:nunitosansbold; text-decoration: underline;"><b>INFORMACIÓ MÈDICA</b></span><br/><br/>
            Si l’estudiant que va a realitzar el viatge de fi de curs necessita cures de salut especials, hauran de ser notificades amb antel.lació a la nostra empresa per poder gestionar-les.  Han de comunicar-ho a professors i/o pares acompanyants, responsables de l’allotjament triat i coordinador d’activitats de la destinació del viatge de fi de curs.<br/><br/>
            <span style="font-family:nunitosansbold; text-decoration: underline;"><b>PROTECCIÓ DE DADES DE CARÀCTER PERSONAL</b></span><br/><br/>
            Conforme a la Llei Orgànica  15/1999, del 13 de desembre, sobre la Protecció de Dades de Caràcter Personal, les dades facilitades per l’usuari quedaran incorporades automàticament en un fitxer, propietat de FINALIA VIAJES S.L. (ByeBye Group) el qual serà processat amb la finalitat de poder proporcionar els serveis sol.licitats.  L’usuari autoritza a ByeBye Group per incloure les seves dades personals als respectius fitxers, així com la seva utilització per a la gestió i registre de les operacions subscrites entre ambdues parts.
            En cumpliment de la Llei Orgànica 15/1999, els usuaris podran exercir el seu dret d’accés, rectificació, cancel.lació i oposició mitjançant un escrit dirigit a Finalia Viajes S.L. – ByeBye Group, C/ Girona, 34, 08700 Igualada (Barcelona).<br/><br/>


        </div>
    </div>
</page>
<page id="pagina8">
    <div style="background:url(http://byebyegroup.com/new/img/pdf/bg7.jpg); width:797px; height:1122px;">
      <!--  <div style="position: absolute; left: 520px; top: 304px;">@byebyegroup</div>
        <div style="position: absolute; left: 520px; top: 354px;">@byebyegroup</div>
        <div style="position: absolute; left: 520px; top: 404px;">@byebyegroup</div>-->
    </div>
</page>
