<div class="timeline-book-block book-tour <?= !empty($_GET['solicitud']) ? 'show-book-block' : '' ?>">
    <div class="find-widget find-hotel-widget widget new-style" >
        <h4 class="title-widgets"><?= $detail->destinos_nombre ?></h4>
        <div class="content-widget tour-booking slz-booking-wrapper">

            <div class="extra-item">
                <h4 class="capt"> 
                    <span class="btn btn-maincolor btn-book-tour" style=" font-weight: 600; border: 1px solid #fff; background-color: #fff;font-size: 18px;border-radius: 0px; margin-left: -32px; margin-right: 10px;">1</span>
                    HOLA! TRIA LA TEVA OPCIÓ DE VIATGE
                </h4>
                <div class="text-box-wrapper">
                    <label class="tb-label">
                        Ciutat de sortida
                    </label>
                    <div class="input-group">
                        <input type="text" id='ciudad' name="ciudad" placeholder="Exem: Igualada" class="tb-input" value="<?= !empty($editar) ?$editar->ciudad : '' ?>">
                        <i class="tb-icon fa fa-home input-group-addon"></i>
                    </div>
                </div>
                <div class="text-box-wrapper">
                    <label class="tb-label">
                        Circuit de destí
                    </label>
                    <div class="input-group">
                        <input type="text" id='destino' name="destino" placeholder="Circuit de destí" class="tb-input" value="<?= $detail->destinos_nombre ?>">
                        <i class="tb-icon fa fa-suitcase input-group-addon"></i>
                    </div>
                </div>
                <div class="text-box-wrapper">
                    <label class="tb-label">
                        Data de sortida
                    </label>
                    <div class="input-group">
                        <input type="text" id='fecha' name="fecha" placeholder="DD/MM/YY" class="tb-input" value="<?= !empty($editar) ? date("d/m/Y", strtotime($editar->fecha)) : '' ?>">
                        <i class="tb-icon fa fa-calendar input-group-addon"></i>
                    </div>
                </div>
                <div class="count text-box-wrapper">
                    <label class="tb-label">
                        Alumnes
                    </label>
                    <div class="input-group">
                        <button type="button" disabled="disabled" data-type="minus" data-field="alumnos" class="input-group-btn btn-minus">
                            <span class="tb-icon fa fa-minus"></span>
                        </button>
                        <input type="number" id="alumnos" name="alumnos" min="0" max="9000" value="<?= empty($editar) ? 0 : $editar->alumnos ?>" class="tb-input count">
                        <button type="button" data-type="plus" data-field="alumnos" class="input-group-btn btn-plus">
                            <span class="tb-icon fa fa-plus"></span>
                        </button>
                    </div>
                </div>
                <div class="text-box-wrapper">
                    <label class="tb-label">
                        Professors
                    </label>
                    <div class="input-group">
                        <button type="button" disabled="disabled" data-type="minus" data-field="profesores" class="input-group-btn btn-minus">
                            <span class="tb-icon fa fa-minus"></span>
                        </button>
                        <input type="number" name="profesores" min="1" max="900" value="<?= empty($editar) ? 1 : $editar->profesores ?>" class="tb-input count">
                        <button type="button" data-type="plus" data-field="profesores" class="input-group-btn btn-plus">
                            <span class="tb-icon fa fa-plus"></span>
                        </button>
                    </div>
                </div>
            </div><!-- End extra items -->

            <div class="extra-item">

                <h4  class="capt">
                    <span class="btn btn-maincolor btn-book-tour" style=" font-weight: 600; border: 1px solid #fff; background-color: #fff;font-size: 18px;border-radius: 0px; margin-left: -32px; margin-right: 10px;">2</span>OMPLE AQUEST PETIT FORMULARI PER REBRE EL TEU PRESSUPOST</h4>
                <div class="text-box-wrapper">
                    <label class="tb-label">
                        El teu nom
                    </label>
                    <div class="input-group">
                        <input type="text" id='nombre' name="nombre" placeholder="Escriu el teu nom" class="tb-input" value="<?= !empty($editar) ? $editar->nombre : '' ?>">
                        <i class="tb-icon fa fa-pencil input-group-addon"></i>
                    </div>
                </div>
                <div class="text-box-wrapper">
                    <label class="tb-label">
                        El teu telèfon
                    </label>
                    <div class="input-group">
                        <input type="text" id='telefono' name="telefono" placeholder="Escriu el teu telèfon" class="tb-input" value="<?= !empty($editar) ? $editar->telefono : '' ?>">
                        <i class="tb-icon fa fa-phone input-group-addon"></i>
                    </div>
                </div>
                <div class="text-box-wrapper">
                    <label class="tb-label">
                       Ref. del Pressupost
                    </label>
                    <div class="input-group">
                        <input type="text" id='referencia' name="referencia" placeholder="Fical el nom que mès t'agradi al teu pressupost" class="tb-input" value="<?= !empty($editar) ? $editar->referencia : '' ?>">
                        <!--                        <i class="tb-icon fa fa-calendar input-group-addon"></i>-->
                    </div>
                </div>
                <div class="text-box-wrapper">
                    <label class="tb-label">
                        Nom del institut
                    </label>
                    <div class="input-group">
                        <input type="text" id='instituto' name="instituto" placeholder="Nom del institut" class="tb-input" value="<?= !empty($editar) ? $editar->instituto : '' ?>">
                        <i class="tb-icon fa fa-graduation-cap input-group-addon"></i>
                    </div>
                </div>
                <div class="text-box-wrapper">
                    <label class="tb-label">
                        Localitat
                    </label>
                    <div class="input-group">
                        <input type="text" id='localidad' name="localidad" placeholder="Localitat del institut" class="tb-input" value="<?= !empty($editar) ? $editar->localidad : '' ?>">
                        <i class="tb-icon fa fa-home input-group-addon"></i>
                    </div>
                </div>
            </div><!-- End extra items -->

            <div class="extra-item" style=" margin-left: 15px">
                <!--                <h4 style="font-weight: 600;font-size: 16.1px;    text-decoration: underline;">Completa este pequeño formulario para recibir tu presupuesto</h4>               -->
                <div>
                    <label class="tb-label " style=" text-decoration: underline;">
                        Nivell del curs
                    </label>
                    <div>
                        <label><input type="radio" id='curso' name="curso" value="<?= "1er ESO" ?>">   1r ESO</label>
                        <label><input type="radio" id='curso' name="curso" value="<?= "2er ESO" ?>"> 2n ESO</label>
                        <label><input type="radio" id='curso' name="curso" value="<?= "3er ESO" ?>"> 3r ESO</label>
                        <label><input type="radio" id='curso' name="curso" value="<?= "4r ESO" ?>"> 4r ESO</label>
                        <label><input type="radio" id='curso' name="curso" value="<?= "1er Batxillerat" ?>"> 1r Batxillerat</label>
                        <label><input type="radio" id='curso' name="curso" value="<?= "2er Batxillerat" ?>"> 2n Batxillerat</label>
                        <label><input type="radio" id='curso' name="curso" value="<?= "Cicle Formatiu" ?>"> Cicle Formatiu</label>
                    </div>
                </div>
            </div><!-- End extra items -->

            <div class="extra-item" style=" margin-left: 15px">
                <div style=" margin-top: 14px">
                    <label class="tb-label" style=" text-decoration: underline;">
                        Càrrec
                    </label>
                    <div>
                        <label><input type="radio" id='cargo' name="cargo" value="<?= 'Professor' ?>"> Professor</label>
                        <label><input type="radio" id='cargo' name="cargo" value="<?= 'Director' ?>"> Director</label>
                        <label><input type="radio" id='cargo' name="cargo" value="<?= 'Coordinador' ?>"> Coordinador</label>
                        <label><input type="radio" id='cargo' name="cargo" value="<?= 'AMPA' ?>"> AMPA</label>
                        <label><input type="radio" id='cargo' name="cargo" value="<?= 'Pare/Mare' ?>"> Pare/Mare</label>
                    </div>
                </div>
            </div><!-- End extra items -->

            <!-- Extra Items -->
            <div class="extra-item">
                <h4  class="capt">
                    <span class="btn btn-maincolor btn-book-tour" style=" font-weight: 600; border: 1px solid #fff; background-color: #fff;font-size: 18px;border-radius: 0px; margin-left: -32px; margin-right: 10px;">3</span>ACTIVITATS INCLOSES EN EL TEU DESTÍ</h4>
                <p class="des" style=" margin-left: 15px">Si algunes d’aquestes activitats no et satisfan les pots deseleccionar i no s’inclouran al teu viatge.</p>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th>Activitat</th>
                            <th>Preu</th>
                            <th>Transport</th>
                            <th>Durada</th>
                            <th>Assegurança</th>
                            <th>Guia</th>
                            <th>Menú</th>
                            <th>Monitors</th>
                            <th>Afegir</th>
                        </tr>
                        <?php if (!empty($editar)) $actividades = explode("//", $editar->actividades); ?>
                        <?php foreach ($detail->actividades->result() as
                                       $a): ?>
                            <?php if ($a->precio == -1): ?>
                                <tr>
                                    <td class="item-info">
                                        <h5><?= $a->actividad ?></h5>
                                    </td>
                                    <td class="item-price"> <span>Inclosa</span></td>
                                    <td class="is-person"><?= $a->transporte ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->duracion ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->seguro ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->guia ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->menu ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->monitores ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="number-item">
                                        <input type="checkbox" class="actividad" data-precio='<?= $a->precio >= 0 ? $a->precio : 0 ?>' data-name="<?= $a->actividad ?>"
                                            <?php
                                            if (empty($editar) || !empty($actividades) && in_array($a->actividad, $actividades))
                                                echo "checked"
                                            ?>>
                                    </td>
                                </tr>
                            <?php endif ?>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
                <div class="booking-des hide"></div>
            </div><!-- End extra items -->



            <!-- Extra Items -->
            <div class="extra-item">
                <h4  class="capt">
                    <span class="btn btn-maincolor btn-book-tour" style=" font-weight: 600; border: 1px solid #fff; background-color: #fff;font-size: 18px;border-radius: 0px; margin-left: -32px; margin-right: 10px;">4</span>VOLS ALGUNA ACTIVITAT EXTRA?</h4>
                <p class="des" style=" margin-left: 15px">Selecciona les activitas que més s’adaptin al vostre viatge.</p>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th>Activitat</th>
                            <th>Preu</th>
                            <th>Transport</th>
                            <th>Durada</th>
                            <th>Assegurança</th>
                            <th>Guia</th>
                            <th>Menú</th>
                            <th>Monitors</th>
                            <th>Afegir</th>
                        </tr>
                        <?php if (!empty($editar)) $actividades = explode("//", $editar->actividades); ?>
                        <?php foreach ($detail->actividades->result() as
                                       $a): ?>
                            <?php if ($a->precio >= 0): ?>
                                <tr>
                                    <td class="item-info">
                                        <h5><?= $a->actividad ?></h5>
                                    </td>
                                    <td class="item-price"> <span><?= number_format($a->precio, 2, ',', '.') ?></span> €</td>
                                    <td class="is-person"><?= $a->transporte ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->duracion ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->seguro ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->guia ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->menu ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="is-person"><?= $a->monitores ? 'SI' : 'NO' ?><span class="hide"></span></td>
                                    <td class="number-item">
                                        <input type="checkbox" class="actividad" data-precio='<?= $a->precio >= 0 ? $a->precio : 0 ?>' data-name="<?= $a->actividad ?>"
                                            <?php if (!empty($actividades) && in_array($a->actividad, $actividades)) echo "checked" ?>>
                                    </td>
                                </tr>
                            <?php endif ?>
                        <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
                <div class="booking-des hide"></div>
            </div><!-- End extra items -->

            <div class="extra-item">
                <h4  class="capt">
                    <span class="btn btn-maincolor btn-book-tour" style=" font-weight: 600; border: 1px solid #fff; background-color: #fff;font-size: 18px;border-radius: 0px; margin-left: -32px; margin-right: 10px;">5</span>SELECCIONA UNA ASSESORA I SI VOLS FES-LI UN COMENTARI</h4>
                <div class="row" style="margin-left:0px; margin-right:0px;">
                    <div class="col-xs-12 col-sm-6" style=" margin-top: 28px">
                        <div class="col-xs-12" align='left'>
                            <!--                            <p> Seleccioni el comercial que vol que l'antegui</p>-->
                        </div>
                        <div class="col-xs-6" style="margin-bottom:20px;">
                            <div class="avatar" style="float:left;  text-align: center;">
                                <img class="img-responsive" style="border-radius:100%; width:60px;" alt="" src="<?= base_url('img/homepage/avatar-contact-1.jpg') ?>">
                            </div>
                            <div style="float:left; margin-left:10px; width:60%">
                                <label style="width:100%;"><input type='radio' name='asesor' value='Anna Riba'> <b>Anna Riba</b></label>
                                <label style="width:100%;" for="asesor"><span>Catalunya Occiental</span></label>
                            </div>
                        </div>
                        <div class="col-xs-6" style="margin-bottom:20px;">
                            <div class="avatar" style="float:left; text-align: center;">
                                <img class="img-responsive" style="border-radius:100%; width:60px;" alt="" src="<?= base_url('img/homepage/avatar-contact-5.jpg') ?>">
                            </div>
                            <div style="float:left; margin-left:10px; width:60%">
                                <label style="width:100%;"><input type='radio' name='asesor' value='Emma Soler'> <b>Emma Soler</b></label>
                                <label style="width:100%;" for="asesor"><span>Catalunya Oriental</span></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="input-group" style="border: 1px solid rgb(204, 204, 204); padding: 20px; width:100%; margin:15px 0; width:100%; text-align:left;">
                            <label for="comentario">Deixa un comentari</label>
                            <textarea class="form-control" name="comentario" id="comentario" placeholder="Deixa un comentari" style="border: 1px solid rgb(204, 204, 204); min-height: 110px;" placeholder='Deixa un comentari'><?= empty($editar) ? '' : $editar->comentario ?></textarea>
                        </div>
                    </div>
                </div>
            </div><!-- End extra items -->

            <div class="extra-item">
                <h4  class="capt">
                    <span class="btn btn-maincolor btn-book-tour" style=" font-weight: 600; border: 1px solid #fff; background-color: #fff;font-size: 18px;border-radius: 0px; margin-left: -32px; margin-right: 10px;">6</span>ENHORABONA JA HO TENIM TOT</h4>
                <p class="des" style=" margin-left: 15px" >Fes click al botó de consultar i rebràs el pressupost per email.</p>
                
                
                <div  class="premail text-box-wrapper">
                    <label class="tb-label">
                        Email
                    </label>
                    <div class="input-group">
                        <input type="text" id='email' name="email" placeholder="email" class="tb-input" value="<?= !empty($editar) ? $editar->email : '' ?>">
                        <i class="tb-icon fa fa-envelope input-group-addon"></i>
                    </div>
                </div>
                <div class="precaptcha text-box-wrapper">
                    <div>
                        <div class="g-recaptcha" data-sitekey="6LfG5i0UAAAAABfvs7-lS8DDkZPMlwEXOcs1TMWi"></div>
                    </div>
                </div>
            
            </div><!-- End extra items -->

        </div>
        <input type="hidden" name="actividades" id="actividades" value="<?= !empty($editar) ? $editar->actividades : '' ?>">
        <input type="hidden" name="destinos_id" value="<?= $detail->id ?>">
        <input type="hidden" name="estado" value="0">
        <input type="hidden" id="precioinput" name="precio" value="<?= $detail->precios->num_rows() > 0 && $detail->precios->row(0)->detalle->num_rows() ? number_format($detail->precios->row(0)->detalle->row(0)->precio, 2, ',', '.') : number_format($detail->precio, 2, ',', '.') ?>">
        <input type="hidden" name="sesion" value="<?= empty($_SESSION['user']) ? $_SESSION['tmpsesion'] : $_SESSION['user'] ?>">
        <div class="message"></div>
        <div class="clearfix"></div>
        <div class="slz-btn-group">
                <button type="submit" id="guardar" data-hover="Enviar" class="btn btn-slide btn-check slz-add-to-cart" data-id="588">
                <span class="text">Consultar</span>
                <span class="icons fa fa-long-arrow-right"></span>
            </button>
        </div>

    </div>
</div>

<script type='text/javascript'>
/* <![CDATA[ */
var ajaxurl = "http:\/\/wp.swlabs.co\/exploore\/wp-admin\/admin-ajax.php";
/* ]]> */

var precios = <?php
        $precios = array();
        foreach($detail->precios->result() as $p){
            $pre = array();
            $pre['desde'] = $p->fecha_desde;
            $pre['hasta'] = $p->fecha_hasta;
            $detalle = array();
            foreach($p->detalle->result() as $d){
                $d->alumnos = (int)$d->alumnos;
                $d->precio = $d->precio<0?0:$d->precio;
                $detalle[] = $d;                
            }
            $pre['detalle'] = $detalle;
            $precios[] = $pre;
        }
        echo json_encode($precios);
    ?>;
        
<?php if(!empty($editar)): ?>
    $(document).on("ready",function(){
       total(); 
    });
<?php endif ?>
    
    function iraform(){
        $(".book-tour").addClass("show-book-block");
        document.location.href="#presupuesto";
    }
</script>

<script>
    function in_date_range(value,desde,hasta) {
        // format: dd/mm/yyyy;
        value = value.split("/");
        desde = desde.split("-");
        hasta = hasta.split("-");
        var startDate = new Date(desde[0], desde[1]-1, desde[2]),
            endDate = new Date(hasta[0], hasta[1]-1, hasta[2]),
            date = new Date(value[2], value[1]-1, value[0]);
        return startDate <= date && date <= endDate;
    }

    function total(){
        var fecha = $("#fecha").val(),
            alumnos = parseInt($("#alumnos").val()),
            subtotal = $("#subtotals"),
            plazas = $("#plazas"),
            fecha_salida = $("#fechasalida"),
            total = $("#totals,#precioalumno");
        var encontrado = false;
        var precio = 0;
        for(var i in precios){
            if(in_date_range(fecha,precios[i].desde,precios[i].hasta)){
                for(var k in precios[i].detalle){
                    if(alumnos<= precios[i].detalle[k].alumnos && !encontrado){
                        encontrado = true;
                        subtotal.html(precios[i].detalle[k].precio);
                        precio = precios[i].detalle[k].precio;
                    }
                }
                if(!encontrado && alumnos>precios[i].detalle[precios[i].detalle.length-1].alumnos){
                    subtotal.html(precios[i].detalle[precios[i].detalle.length-1].precio);
                    precio = precios[i].detalle[precios[i].detalle.length-1].precio;
                }
                total.html(precio);
                $("#precioinput").val(precio);
                plazas.html(alumnos);
                fecha_salida.html(fecha);
            }
        }
        var extras = 0;
        $("#actividades").val("");
        $("#extraslabel").html("0");
        $(".actividad").each(function(){
            var tot = parseFloat($("#precioinput").val());
            if(!isNaN(tot) && $(this).prop("checked")){
                tot+= parseFloat($(this).data("precio"));
                extras+= parseFloat($(this).data("precio"));
                $("#extraslabel").html(extras);
                $("#precioinput").val(tot);
                total.html(tot);
                var actividades = $("#actividades").val();
                actividades = actividades+$(this).data("name")+"//";
                $("#actividades").val(actividades);
            }
        });
    }

    function send(form){
        var url_validation = "<?php
            if(empty($editar)){
                echo base_url('destinos/frontend/solicitudes/insert_validation/');
            }else{
                echo base_url('destinos/frontend/solicitudes/update_validation/'.$editar->id);
            }
            ?>";
        var url = "<?php
            if(empty($editar)){
                echo base_url('destinos/frontend/solicitudes/insert');
            }else{
                echo base_url('destinos/frontend/solicitudes/update/'.$editar->id);
            }
            ?>";
        var f = new FormData(form);
        $(".message").html("Si us plau sigues pacient, estem generant el pressupost immediatament te l'enviarem al teu e-mail.");
        $("#guardar").attr('disabled',true);
        $.ajax({
            url:url_validation,
            data: f,
            context: document.body,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success:function(data){
                $("#guardar").attr('disabled',false);
                data = data.replace(/<[^p].*?>/g,"",data);
                data = JSON.parse(data);
                if(data.success){
                    $("#guardar").attr('disabled',true);
                    $.ajax({
                        url:url,
                        data: f,
                        context: document.body,
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'POST',
                        success:function(data){
                            $("#guardar").attr('disabled',false);
                            data = data.replace(/<[^p].*?>/g,"",data);
                            data = JSON.parse(data);
                            if(data.success){
                                $(".message").html("¡Perfecte! El vostre pressupost s'ha enviat correctament, consulta el teu correu i sorprèn-te del viatge que tens planificat. Qualsevol consulta o modificació de l'itinerari en el pressupost posseeix les dades de l'assessor per canviar el que necessiteu.");
                            }else{
                                grecaptcha.reset();
                                $(".message").html("Ha ocorregut un error intern, sis plau comunica't amb ByeBye Group");
                        }
                        }
                    });
                }else{
                    grecaptcha.reset();
                    $(".message").html(data.error_message);
                }
            }
        });
        return false;
    }
    $(document).ready(function(){
        var mindate = "<?= $detail->precios->num_rows()>0?date("d/m/Y",strtotime($detail->precios->row(0)->fecha_desde)):date("d/m/Y") ?>";
        mindate = mindate.split("/");
        mindate = new Date(mindate[2], mindate[1]-1, mindate[0]);

        var maxdate = "<?= $detail->precios->num_rows()>0?date("d/m/Y",strtotime($detail->precios->row($detail->precios->num_rows()-1)->fecha_hasta)):date("d/m/Y") ?>";
        maxdate = maxdate.split("/");
        maxdate = new Date(maxdate[2], maxdate[1]-1, maxdate[0]);

        $('#fecha').datepicker({
            format: 'dd/mm/yyyy',
            maxViewMode: 0,
            startDate:mindate,
            endDate:maxdate
        });
        $("#fecha, #alumnos, .actividad").on('change',function(){
            total();
        });
        total();
    });
</script>