<div class="page-title parallax parallax4 projectes"  style=' background-size: inherit;'>           
        <div class="container">
            <div class="row">
                <div class="col-md-12">                    
                    <div class="page-title-heading">
                        <h2 class="title">Projectes</h2>
                    </div><!-- /.page-title-heading -->
                    <div class="breadcrumbs">
                        <ul>
                            <li class="home"><a href="#">Home</a></li>
                            <li>Projectes</li>
                        </ul>                   
                    </div><!-- /.breadcrumbs --> 
                </div><!-- /.col-md-12 -->  
            </div><!-- /.row -->  
        </div><!-- /.container -->                      
    </div><!-- /page-title parallax -->

    <section id="projectess" class="flat-row flat-recent-causes recent-mag-top pad-bottom50px">
        <div class="container">
            <div class="row">
                <?= $output ?>
            </div><!-- /.row -->
        </div><!-- /.container -->   
    </section><!-- /main-content blog-post -->
<script>
    function changePage(id){
        $("#page").val(id);
        $("#searchform").submit();
    }
    
    function changeCategoria(id){
        $("#blog_categorias_id").val(id);
        $("#searchform").submit();
    }
</script>
