<?php
$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/jquery.noty.js');
$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js');
$this->set_js_lib('assets/grocery_crud/js/common/list.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/cookies.js');
$this->set_js('js/crud/main.js');
$this->set_js('js/crud/destinos.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/jquery.form.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.numeric.min.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/jquery.printElement.min.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/pagination.js');
/** Fancybox */
$this->set_css('assets/grocery_crud/css/jquery_plugins/fancybox/jquery.fancybox.css');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.fancybox-1.3.4.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.easing-1.3.pack.js');

/** Jquery UI */
$this->load_js_jqueryui();
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url(); ?>';
    var subject = '<?php echo $subject ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var crud_pagin = 1;
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
</script>
<div class="flexigrid">
    <form action="<?= $ajax_list_url ?>" method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="<?= $ajax_list_info_url ?>" onsubmit="return filterSearchClick(this)">
    <div class="result-meta row">
        <div class="col-lg-4 col-md-12">
            <div class="result-count-wrapper">Viatges trobats: <span class="result-count"><?= $total_results ?></span></div> 
        </div>

        <div class="col-lg-8 col-md-12">
            <div class="result-filter-wrapper">
                <div class="result-meta row">
                    <div class="col-md-7">
                        <div class="result-count-wrapper"><span class="search-result-title"><?php $paging_starts_from = "<span id='page-starts-from' class='page-starts-from'>1</span>"; ?>
                    <?php $paging_ends_to = "". ($total_results < $default_per_page ? $total_results : $default_per_page) .""; ?>
                    <?php $paging_total_results = "$total_results"?>
                    <?php echo str_replace( array('{start}','{end}','{results}'),
                            array($paging_starts_from, $paging_ends_to, $paging_total_results),
                            $this->l('list_displaying')
                       ); ?>  </span> 
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="result-filter-wrapper">
                            <div class="result-filter">
                                <div class="hide slz-sort-type" data-type="tour"></div>
                                <label class="result-filter-label">Ordenar por:	</label>
                                <div class="selection-bar">
                                    <div class="select-wrapper" style="z-index: 100;">
                                        <select id="ordenamiento" class="custom-select selectbox" style="display: none;">
                                            <option value="desc" selected="selected" data-ordering="id">Darrer afegit</option>
                                            <option value="asc" data-ordering="destinos_nombre">Nom - Ascendent</option>
                                            <option value="desc" data-ordering="destinos_nombre">Nom - Descendent</option>
                                            <option value="desc" data-ordering="visitas">Els més vistos</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="hide slz-archive-column" data-col="2"></div>
                            </div>
                        </div>	
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="result-body">
        <div class="row">
            <div class="col-md-8 main-right">
                <div id='ajax_list' class="car-rent-list ajax_list">                    
                    <?php echo $list_view ?>
                </div>
                <nav class="pagination-list margin-top70">
                    <ul class="pagination">                        
                    </ul>
                </nav>
            </div>
            
            <div class="col-md-4 sidebar-widget">
                <div class="col-2" style="display:none">
                    <div class="find-widget find-flight-widget widget">
                        <h4 class="title-widgets">Busca el teu viatge</h4>
                            <div class="ffw-radio-selection">
                                <input type="hidden" name="search_field[]" value="destino">
                                <span class="ffw-radio-btn-wrapper">                                    
                                    <input type="radio" name="search_text[]" value="" id="flight-type-0" <?php if(empty($_GET['destino']))echo 'checked="checked"' ?> class="ffw-radio-btn">
                                    <label for="flight-type-0" class="ffw-radio-label">Tots</label>
                                </span>
                                <span class="ffw-radio-btn-wrapper">                                    
                                    <input type="radio" name="search_text[]" <?php if(!empty($_GET['destino']) && $_GET['destino']=='Catalunya')echo 'checked="checked"' ?> value="Catalunya" id="flight-type-1" class="ffw-radio-btn">
                                    <label for="flight-type-1" class="ffw-radio-label">Catalunya</label>
                                </span>
                                <span class="ffw-radio-btn-wrapper">
                                    <input type="radio" name="search_text[]"  <?php if(!empty($_GET['destino']) && $_GET['destino']=='España')echo 'checked="checked"' ?>value="España" id="flight-type-2" class="ffw-radio-btn">
                                    <label for="flight-type-2" class="ffw-radio-label">España</label>
                                </span>
                                <span class="ffw-radio-btn-wrapper">
                                    <input type="radio" name="search_text[]" <?php if(!empty($_GET['destino']) && $_GET['destino']=='Europa')echo 'checked="checked"' ?> value="Europa" id="flight-type-3" class="ffw-radio-btn">
                                    <label for="flight-type-3" class="ffw-radio-label">Europa</label>
                                </span>
                                <div class="stretch">&nbsp;</div>
                            </div>
                            <div class="text-input small-margin-top">
                                <div class="text-box-wrapper">
                                    <label class="tb-label">On vols anar?</label>
                                    <div class="input-group">
                                        <input type="hidden" name="search_field[]" value="destinos_nombre">                                        
                                        <select class="tb-input select2" name="search_text[]" tabindex="-1" aria-hidden="true">
                                            <option value="" selected="selected">Tria un destí</option>
                                            <?php get_instance()->db->order_by("destinos_nombre","ASC"); ?>
                                            <?php foreach(get_instance()->db->get('destinos')->result() as $d): ?>
                                            <option value="<?= $d->destinos_nombre ?>" <?php if(!empty($_GET['destinos_nombre']) && $_GET['destinos_nombre'] == $d->destinos_nombre)echo "selected" ?>><?=  $d->destinos_nombre ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div> 
                                <div class="input-daterange">
                                    <div class="text-box-wrapper half left">
                                        <label class="tb-label">Sortida</label>
                                        <div class="input-group">                                            
                                            <input type="text" name="fecha_desde" placeholder="DD/MM/YY" class="tb-input" value="<?php if(!empty($_GET['fecha_desde']))echo $_GET['fecha_desde'] ?>">
                                            <i class="tb-icon fa fa-calendar input-group-addon"></i>
                                        </div>
                                    </div>
                                    <div class="text-box-wrapper half right">
                                        <label class="tb-label">Tornada</label> 
                                        <div class="input-group">                                            
                                            <input type="text" name="fecha_hasta" placeholder="DD/MM/YY" class="tb-input" value="<?php if(!empty($_GET['fecha_hasta']))echo $_GET['fecha_hasta'] ?>">
                                            <i class="tb-icon fa fa-calendar input-group-addon"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-box-wrapper half left">
                                    <label class="tb-label">Professors</label>
                                    <div class="input-group">
                                        <button disabled="disabled" data-type="minus" data-field="profesores" class="input-group-btn btn-minus">
                                            <span class="tb-icon fa fa-minus"></span>
                                        </button>                                        
                                        <input type="number" name="profesores" min="0" max="9" value="<?php if(!empty($_GET['profesores']))echo $_GET['profesores']; else echo "0"; ?>" class="tb-input count">
                                        <button data-type="plus" data-field="profesores" class="input-group-btn btn-plus">
                                            <span class="tb-icon fa fa-plus"></span>
                                        </button>
                                    </div>
                                </div>
                                <div class="text-box-wrapper half right">
                                    <label class="tb-label">Alumnes</label>
                                    <div class="input-group">
                                        <button disabled="disabled" data-type="minus" data-field="alumnos" class="input-group-btn btn-minus">
                                            <span class="tb-icon fa fa-minus"></span>
                                        </button>                                        
                                        <input type="number" name="alumnos" min="0" max="9" value="<?php if(!empty($_GET['alumnos']))echo $_GET['alumnos']; else echo "0"; ?>" class="tb-input count">
                                        <button data-type="plus" data-field="alumnos" class="input-group-btn btn-plus">
                                            <span class="tb-icon fa fa-plus"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        <?php if(!empty($_GET['lowcost'])): ?>
                        <input type="hidden" name="search_field[]" value="lowcost">
                        <input type="hidden" name="search_text[]" value="2">
                        <?php endif ?>
                        <?php if(!empty($hidden)): ?>
                            <input type="hidden" name="search_field[]" value="<?= $hidden[0] ?>">
                            <input type="hidden" name="search_text[]" value="<?= $hidden[1] ?>">
                        <?php endif ?>
                        <button type="submit" data-hover="Enviar Ahora" class="btn btn-slide small-margin-top">
                            <span class="text">Buscar ara</span>
                            <span class="icons fa fa-long-arrow-right"></span>
                        </button>
                    </div>
                </div>
                <div class="col-2">
                    <div class="col-1" style="display:none">
                        <div class="price-widget widget">
                            <div class="title-widget">
                                <div class="title">preu</div>
                            </div>
                            <div class="content-widget">
                                <div class="price-wrapper">
                                    <div data-range_min="0" data-range_max="800" data-cur_min="0" data-cur_max="3000" class="nstSlider">
                                        <div class="leftGrip indicator">
                                            <div class="number"></div>
                                        </div>
                                        <div class="rightGrip indicator">
                                            <div class="number"></div>
                                        </div>
                                    </div>
                                    <div class="leftLabel">0</div>
                                    <div class="rightLabel">800</div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <div class="col-1">
                        <div class="car-type-widget widget">
                            <div class="title-widget">
                                <div class="title">Destins</div>
                            </div>
                            <div class="content-widget">
                                <?php if(count($list)>0): ?>
                                    <?php get_instance()->db->where('categorias_destinos_id',$list[0]->categorias_destinos_id); ?>
                                    <?php foreach(get_instance()->db->get_where('destinos')->result() as $c): ?>                                        
                                            <div class="radio-btn-wrapper">
                                                <label class="radio-label">
                                                    <a href="<?= site_url('destinos/'.toURL($c->id.'-'.$c->destinos_nombre)) ?>" style="color:#555e69">
                                                        <?= $c->destinos_nombre ?>
                                                    </a>
                                                </label>
                                                <span class="count"><?= $c->disponibilidad ?></span>
                                            </div>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-2">
                    <div class="special-equipment-widget widget">
                        <div class="title-widget">
                            <div class="title">Tipus de viatge</div>
                        </div>
                        <div class="content-widget">           
                            <?php 
                                get_instance()->db->order_by('orden','ASC');
                                foreach(get_instance()->db->get_where('categorias_destinos',array('id !='=>5))->result() as $c): 
                            ?>
                            <div class="radio-btn-wrapper">
                                <label class="radio-label">
                                    <a href="<?= site_url('destinos/pack/'.$c->id) ?>#list" style="color:#555e69"><?= $c->categorias_destinos_nombre ?></a>
                                </label>
                                <span class="count">
                                    <?php if($c->id!=7): ?>
                                    <?= get_instance()->db->get_where('destinos',array('categorias_destinos_id'=>$c->id))->num_rows() ?>
                                    <?php else: ?>
                                    <?= get_instance()->db->get_where('destinos',array('lowcost'=>2))->num_rows() ?>
                                    <?php endif ?>
                                </span>
                            </div>
                             <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="special-offer margin-top70">
        <h3 class="title-style-2">Més vistos</h3>
        <div class="special-offer-list" style=" margin-bottom: 0px;">
            <?php get_instance()->db->order_by("visitas","DESC"); ?>
            <?php get_instance()->db->limit(8); ?>
            <?php foreach(get_instance()->db->get_where("destinos")->result() as $p): ?>
                        <div class="special-offer-layout">
                            <div class="image-wrapper">
                                <a href="<?= site_url("destinos/".toURL($p->id."-".$p->destinos_nombre)) ?>" class="link"><img src="<?= base_url() ?>img/destinos/<?= $p->portada ?>" alt="" class="img-responsive"></a>
                                <div class="title-wrapper"><a href="<?= site_url("destinos/".toURL($p->id."-".$p->destinos_nombre)) ?>" class="title"><?= $p->destinos_nombre ?></a><i class="icons flaticon-circle"></i></div>
                            </div>
                        </div>
            <?php endforeach ?>
        </div>
    </div>
    <div style="text-align:center; font-size:40px;"><i class="fa fa-spinner ajax_refresh_and_loading" style="visibility: hidden"></i></div>
    <input name='page' type="hidden" value="1" size="4" id='page'>
    <input name='per_page' class="per_page" type="hidden" value="10" size="4">
    <input type='hidden' name='order_by[0]' class='hidden-sorting' value='<?php if(!empty($order_by[0])){?><?php echo $order_by[0]?><?php }?>' />
    <input type='hidden' name='order_by[1]' class='hidden-ordering'  value='<?php if(!empty($order_by[1])){?><?php echo $order_by[1]?><?php }?>'/>
    </form>
</div>
