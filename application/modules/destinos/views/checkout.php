<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?= base_url('destinos/frontend/reservar') ?>" enctype="multipart/form-data">
<div id="wrapper-content">
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <!-- Page Title -->
        <section class="page-title page-banner" style="top: -143px; margin-bottom: -143px;">
            <div class="container">
                <div class="page-title-wrapper">
                    <div class="page-title-content">
                        <ol class="breadcrumb">
                            <li>
                                <a href="<?= site_url() ?>" class="link home">Home</a>
                            </li>
                            <li class="active">
                                <a href="#" class="link">Consultar</a>
                            </li>
                        </ol>				
                        <div class="clearfix"></div>
                        <h2 class="captions">
                            Consultar					
                        </h2>
                    </div>
                </div>
            </div>
        </section>

        <!-- Content section -->
        <div class="section section-padding page-detail padding-top padding-bottom">
            <div class="container">
                <div class="row">
                    <div id="page-content" class="col-md-12 col-xs-12">
                        <div id="post-662" class="post-662 page type-page status-publish hentry">
                            <div class="section-page-content clearfix ">
                                <div class="entry-content">
                                    <div class="woocommerce">
                                        <div id="yith-wcwl-messages">
                                            <?= @$_SESSION['msj'] ?>
                                            <?php unset($_SESSION['msj']); ?>
                                        </div>
                                            <div class="col-xs-12 col-sm-6" id="customer_details">
                                                <div class="col-1">
                                                    <div class="woocommerce-billing-fields">
                                                        <h3>Datos personales</h3>
                                                        <p class="form-row form-row form-row-first validate-required" id="billing_first_name_field">
                                                            <label for="billing_first_name" class="">
                                                                Nombre <abbr class="required" title="required">*</abbr>
                                                            </label>
                                                            <input class="input-text " name="nombre" id="nombre" placeholder="Escribe tu nombre" value="" type="text">
                                                        </p>
                                                        <p class="form-row form-row form-row-first validate-required" id="billing_first_name_field">
                                                            <label for="billing_first_name" class="">
                                                                Apellido <abbr class="required" title="required">*</abbr>
                                                            </label>
                                                            <input class="input-text " name="apellido" id="apellido" placeholder="Escribe tu apellido" value="" type="text">
                                                        </p>
                                                        <div class="clear"></div>                                                        
                                                        <p class="form-row form-row form-row-first validate-required validate-email" id="billing_email_field">
                                                            <label for="billing_email" class="">
                                                                Email Address <abbr class="required" title="required">*</abbr>
                                                            </label>
                                                            <input class="input-text " name="email" id="email" placeholder="Escribe tu email" value="" type="email">
                                                        </p>
                                                        <p class="form-row form-row form-row-last validate-required validate-phone" id="billing_phone_field">
                                                            <label for="billing_phone" class="">Telefono <abbr class="required" title="required">*</abbr></label>
                                                            <input class="input-text " name="telefono" id="telefono" placeholder="Escribe tu telefono" value="" type="tel">
                                                        </p>
                                                        <div class="clear"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <h3 id="order_review_heading">Tu solicitud</h3>
                                                <div id="order_review" class="woocommerce-checkout-review-order">
                                                    <table class="shop_table woocommerce-checkout-review-order-table" style="width:100%">
                                                        <thead>
                                                            <tr>
                                                                <th class="product-name">Destino</th>
                                                                <th class="product-total">Precio</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $total = 0 ?>
                                                            <?php foreach($detail->result() as $d): ?>
                                                                <tr class="cart_item">
                                                                    <td class="product-name">
                                                                        <?= $d->destinos_nombre ?>
                                                                        <strong class="product-quantity">× 1</strong>
                                                                    </td>
                                                                    <td class="product-total">
                                                                        <span class="woocommerce-Price-amount amount">
                                                                            <?=  $d->precio ?> <span class="woocommerce-Price-currencySymbol">€</span>
                                                                        </span>						
                                                                    </td>
                                                                </tr>
                                                                <?php $total+= $d->precio ?>
                                                            <?php endforeach ?>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr class="cart-subtotal">
                                                                <th>Subtotal</th>
                                                                <td>
                                                                    <span class="woocommerce-Price-amount amount">
                                                                        <?= $total ?> <span class="woocommerce-Price-currencySymbol">€</span>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr class="order-total">
                                                                <th>Total</th>
                                                                <td>
                                                                    <span class="woocommerce-Price-amount amount">
                                                                        <?= $total ?> <span class="woocommerce-Price-currencySymbol">€</span>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            <div class="group-button">
                                                <button type="submit" class="btn btn-maincolor">Consultar</button>
                                                <a href="<?= site_url('destinos') ?>" class="btn">Seguir comprando</a>                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #section -->
        <!--WPFC_FOOTER_START-->					
    </div>
    <!-- MAIN CONTENT-->
</div>
</form>