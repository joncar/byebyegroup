<form class="content-widget" action="<?= base_url('destinos') ?>" method="get">
    <div class="ffw-radio-selection">
        <span class="ffw-radio-btn-wrapper"><input type="radio" name="destino" value="" id="flight-type-0" checked="checked" class="ffw-radio-btn"><label for="flight-type-0" class="ffw-radio-label">Tots</label></span>
        <span class="ffw-radio-btn-wrapper"><input type="radio" name="destino" value="Catalunya" id="flight-type-1" class="ffw-radio-btn"><label for="flight-type-1" class="ffw-radio-label">Catalunya</label></span>
        <span class="ffw-radio-btn-wrapper"><input type="radio" name="destino" value="España" id="flight-type-2" class="ffw-radio-btn"><label for="flight-type-2" class="ffw-radio-label">Espanya</label></span>
        <span class="ffw-radio-btn-wrapper"><input type="radio" name="destino" value="Europa" id="flight-type-3" class="ffw-radio-btn"><label for="flight-type-3" class="ffw-radio-label">Europa</label></span>
        <div class="stretch">&nbsp;</div>
    </div>
    <div class="text-input small-margin-top">
        <div class="place text-box-wrapper">
            <label class="tb-label">On vols anar?</label>
            <div class="input-group">
                <select class="tb-input select2" name="destinos_nombre" tabindex="-1" aria-hidden="true">
                    <option value="" selected="selected">Tria un destí</option>
                    <?php $this->db->order_by("destinos_nombre","ASC"); ?>
                    <?php foreach($this->db->get('destinos')->result() as $d): ?>
                    <option value="<?= $d->destinos_nombre ?>"><?=  $d->destinos_nombre ?></option>
                    <?php endforeach ?>
                </select>
            </div>
        </div>
        <div class="input-daterange">
            <div class="text-box-wrapper half">
                <label class="tb-label">Sortida</label>
                <div class="input-group">
                    <input type="text" name="fecha_desde" placeholder="D/M/A" class="tb-input">
                    <i class="tb-icon fa fa-calendar input-group-addon"></i>
                </div>
            </div>
            <div class="text-box-wrapper half">
                <label class="tb-label">Tornada</label>
                <div class="input-group">
                    <input type="text" name="fecha_hasta" placeholder="D/M/A" class="tb-input">
                    <i class="tb-icon fa fa-calendar input-group-addon"></i>
                </div>
            </div>
        </div>
        <div class="count adult-count text-box-wrapper">
            <label class="tb-label">Professors</label>
            <div class="select-wrapper">
                <!--i.fa.fa-chevron-down-->
                <select name="profesores" class="form-control custom-select selectbox">
                    <option selected="selected">1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                    <option>7</option>
                    <option>8</option>
                    <option>9</option>
                    <option>10</option>
                </select>
            </div>
        </div>
        <div class="count child-count text-box-wrapper">
            <label class="tb-label">Alumnos</label>
            <div class="select-wrapper">
                <!--i.fa.fa-chevron-down-->
                <select name="alumnos" class="form-control custom-select selectbox">
                    <option selected="selected">0</option>
                    <option>10</option>
                    <option>20</option>
                    <option>30</option>
                    <option>40</option>
                    <option>50</option>
                    <option>60</option>
                    <option>70</option>
                    <option>80</option>
                    <option>90</option>
                    <option>100</option>
                </select>
            </div>
        </div>
        <input type="hidden" name="categorias_destinos_id" value="2">
        <button type="submit" data-hover="ENVIAR" class="btn btn-slide">
            <span class="text">BUSCAR ARA</span>
            <span class="icons fa fa-long-arrow-right"></span>
        </button>
    </div>
</form>