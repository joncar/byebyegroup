<div id="wrapper-content">
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <section class="page-banner about-us-page">
            <div class="patterndestino"></div>
            <div class="container">
                <div class="page-title-wrapper">
                    <div class="page-title-content">
                        <ol class="breadcrumb">
                            <li><a href="index.html" class="link home">Home</a></li>
                            <li class="active"><a href="#" class="link">nosaltres</a></li>
                        </ol>
                        <div class="clearfix"></div>
                        <h2 class="captions">Qui som?</h2></div>
                </div>
            </div>
        </section>
        <section class="about-us layout-2 padding-top padding-bottom about-us-4">
            <div class="container">
                <div class="row">
                    <div class="wrapper-contact-style">
                        <div class="col-lg-8 col-md-8">
                            <h3 class="title-style-2">Qui es Bye Bye Group?</h3>
                            <div class="about-us-wrapper">
                                <p class="text">FINALIA és l’agència de viatges escollida per excel·lència entre els centres educatius.
                                    Treballem des de 1999 amb l’objectiu d’oferir els millors viatges de final de curs, ajustats a les necessitats de cada centre. Els 15 anys d’experiència organitzant viatges ens ha permès comptar amb els destins més atractius i més competitius amb la millor relació qualitat / preu.
                                    Estem convençuts que l’educació que reben els estudiants a les aules és complementària a la formació pedagògica en la qual s’orienten els nostres viatges. Per això, apostem per destins que combinen una part cultural amb visites als llocs més emblemàtics de cada regió, i  una part lúdica amb divertides activitats d’aventura que encantaran als alumnes.


                                </p>
                                <div class="group-list">
                                    <ul class="list-unstyled about-us-list">
                                        <li>
                                            <p class="text">Descobreix nous indrets</p>
                                        </li>
                                        <li>
                                            <p class="text">Destins de proximitat
                                                </p>
                                        </li>
                                        <li>
                                            <p class="text">Viatja per mar o aire</p>
                                        </li>
                                        <li>
                                            <p class="text">Combinem oci i cultura</p>
                                        </li>
                                    </ul>
                                    <ul class="list-unstyled about-us-list">
                                        <li>
                                            <p class="text">  Aprèn idiomes</p>
                                        </li>
                                        <li>
                                            <p class="text">Accessible per a tothom</p>
                                        </li>
                                        <li>
                                            <p class="text">Descobreix nous indrets</p>
                                        </li>
                                        <li>
                                            <p class="text"> Ens entendrem segur!</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div data-wow-delay="0.4s" class="about-us-image wow zoomInRight"><img src="<?= base_url() ?>img/homepage/about-us-4.png" alt="" class="img-responsive"></div>
                    </div>
                </div>
            </div>
        </section>
        <section class="videos padding-top padding-bottom page-our-values" style=" padding-bottom: 87px; height: 920px;">
            <div class="container">
                <h3 class="title-style-2" style="color: white;">Per què viatjar amb nosaltres?</h3>
                <div class="row">
                    <div class="our-wrapper">
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/1.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">GARANTIA FINALIA </p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur elit.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/2.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">AGENT PERSONALITZAT</p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur elit.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/3.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">TRANSPORTS </p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur elit.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/4.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">ALLOTJAMENTS</p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur elit.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="our-wrapper">
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/10.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">SUPORT TELEFÒNIC</p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur elit.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/6.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title"> VIATGE AMB MONITORS </p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur elit.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/7.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">PROMOCIONS </p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur elit.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/8.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">RESERVA ANTICIPADA</p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur eli</p>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/9.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">ASSEGURANCES</p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur eli</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <div class="our-content"><img src="<?= base_url() ?>img/homepage/11.svg" alt="" class="img-responsive" style="width: 30%;">
                                <div class="main-our">
                                    <p class="our-title">TRACTE DIRECTE</p>
                                    <p class="text">Lorem ipsum dolor sit amet, consectetur elitea commodo consequat duis aute irure dolor consectetur eli</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>            
        </section>
    <section class="our-expert padding-top padding-bottom-50" style="padding-bottom: 160px; background:url(<?= base_url() ?>img/background/bg-section-about_1.jpg)">
        <div class="container">
        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1466761151287"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="slz-shortcode block-title-141176022559947adb20239 ">
                        <h3 class="title-style-2">Com ens organitzem?</h3></div>
                    <div id="accordion-slzexploore_faq-94869926059947adb21205" class="slz-shortcode  wrapper-accordion panel-group ">
                        <div class="panel">
                            <div class="panel-heading">
                                <p class="text">


                                    Organitzar un viatge de final de curs pot resultar un tràmit complicat i costós. Per això, Finalia està a la vostra disposició, per facilitar totes les gestions. El nostre agent de viatges us assessorarà durant tot el procés.</p>       <br>                         <h5 class="panel-title">
                                    <a data-toggle="collapse" href="#slzexploore_faq-94869926059947adb21205-collapse-1" aria-expanded="false" class="accordion-toggle collapsed">
                                        CALENDARI DE PREPARACIÓ
                                    </a>
                                </h5>
                            </div>
                            <div id="slzexploore_faq-94869926059947adb21205-collapse-1" aria-expanded="false" class="panel-collapse collapse" role="tabpanel" style="height: 0px;">
                                <div class="panel-body"><p>El calendari de preparació del vostre viatge és l'origen d'una correcta planificació. Per la nostra experiència, proposem notificar l' interès de fer el  viatge amb la màxima antelació possible per treballar amb un marge de temps adequat.
                                        Truca'ns i us assignarem un gestor de projectes perquè estableixi contacte personal amb l'equip de professorat per assessorar-vos i tancar un pressupost segons les vostres necessitats.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" href="#slzexploore_faq-94869926059947adb21205-collapse-2" aria-expanded="false" class="accordion-toggle collapsed">PRESSUPOST I CONTRACTE</a>
                                </h5>
                            </div>
                            <div id="slzexploore_faq-94869926059947adb21205-collapse-2" aria-expanded="false" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body"><p>Quan us visitem al vostre centre, us portarem els pressupostos dels viatges que estigueu valorant acompanyat de la següent documentació:<br>
                                        • Itinerari detallat<br>
                                        • Mapes<br>
                                        • Informació turística i activitats<br>
                                        • Contracte genèric de viatge combinat</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" href="#slzexploore_faq-94869926059947adb21205-collapse-3" aria-expanded="false" class="accordion-toggle collapsed">CONFIRMACIÓ DE NOMBRE D’ALUMNES I PRIMER PAGAMENT</a>
                                </h5>
                            </div>
                            <div id="slzexploore_faq-94869926059947adb21205-collapse-3" aria-expanded="false" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body"><p>Una vegada seleccionat el destí del vostre viatge, és important  demanar una paga i senyal per assegurar-vos del nombre d’alumnes que vindran al viatge.
                                        <br>Si voleu donar un preu final del viatge als alumnes tenint com a referència el pressupost, que sigui aproximat, mai tancat, perquè aquest pot variar en funció del nombre d’alumnes que finalment hi participin.
                                        <br>Recomanem que fixeu els següents imports de paga i senyal així seran els mateixos que nosaltres necessitarem per fer el bloqueig de les places dels estudiants pel viatge:<br><br>
                                        <b>Viatges nacionals e internacionals  → Bloqueig gratuït del grup, 20% al realitzar la reserva i la resta 30 Dies abans.</b><br><br>
                                        En cas d’utilitzar avió, s’ha de demanar aproximadament una bestreta d’uns 150€ per alumne per poder comprar els bitllets. Abans de comprar-los, els nostres agents sempre confirmen els preus dels bitllets i si han pujat i no us interessa o voleu canviar de viatge us tornem els diners sense cap inconvenient.
                                        <br>Quan finalitzeu aquest procés, sabreu exactament quants alumnes faran el viatge i li podreu confirmar al nostre agent de viatge per poder fer el pagament de garantia de reserva.
                                        <br>30 dies abans de la sortida, s’haurà de realitzar el pagament final del grup i enviar-nos el llistat dels alumnes i professors amb els seus noms, cognoms i DNI.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h5 class="panel-title">
                                    <a data-toggle="collapse" href="#slzexploore_faq-94869926059947adb21205-collapse-4" aria-expanded="false" class="accordion-toggle collapsed">DOCUMENTACIÓ FINAL</a>
                                </h5>
                            </div>
                            <div id="slzexploore_faq-94869926059947adb21205-collapse-4" aria-expanded="false" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body"><p>Abans de l’inici del viatge el nostre agent us farà entrega de la documentació important de tot el viatge, així com el llistat telefònic per a qualsevol imprevist.<br><br>
                                        Oferim el servei de facturació electrònica de la Generalitat de Catalunya.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
</div>

</section>
        <section class="our-expert padding-top padding-bottom-50">
            <div class="container">
                <h3 class="title-style-2">El nostres experts</h3>
                <div class="wrapper-expert">
                    <div class="item content-expert">
                        <a href="#" class="img-expert"><img src="<?= base_url() ?>img/homepage/about-5.jpg" alt="" class="img-responsive img"></a>
                        <div class="caption-expert"><a href="#" class="title">Anabel Mateos Mijares</a>
                            <p class="text">Controller administrativa de expansión Espanya</p>
                            <ul class="social list-inline">
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-facebook"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-twitter"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item content-expert">
                        <a href="#" class="img-expert"><img src="<?= base_url() ?>img/homepage/about-6.jpg" alt="" class="img-responsive img"></a>
                        <div class="caption-expert"><a href="#" class="title">Anna Riba</a>
                            <p class="text">Controller administrativa</p>
                            <ul class="social list-inline">
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-facebook"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-twitter"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item content-expert">
                        <a href="#" class="img-expert"><img src="<?= base_url() ?>img/homepage/about-7.jpg" alt="" class="img-responsive img"></a>
                        <div class="caption-expert"><a href="#" class="title">Monica Martinez</a>
                            <p class="text">Directora comercial de ventas</p>
                            <ul class="social list-inline">
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-facebook"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-twitter"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item content-expert">
                        <a href="#" class="img-expert"><img src="<?= base_url() ?>img/homepage/about-8.jpg" alt="" class="img-responsive img"></a>
                        <div class="caption-expert"><a href="#" class="title">Emma Soler</a>
                            <p class="text">Controller administrativa</p>
                            <ul class="social list-inline">
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-facebook"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-twitter"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="item content-expert">
                        <a href="#" class="img-expert"><img src="<?= base_url() ?>img/homepage/about-9.jpg" alt="" class="img-responsive img"></a>
                        <div class="caption-expert"><a href="#" class="title">Montse Raja</a>
                            <p class="text">Comercial de ventas</p>
                            <ul class="social list-inline">
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-facebook"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-twitter"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>
                   <!-- <div class="item content-expert">
                        <a href="#" class="img-expert"><img src="<?/*= base_url() */?>img/homepage/about-8.jpg" alt="" class="img-responsive img"></a>
                        <div class="caption-expert"><a href="#" class="title">Mark letto</a>
                            <p class="text">Manager Tour Guide</p>
                            <ul class="social list-inline">
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-facebook"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-twitter"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-pinterest-p"></i></a></li>
                                <li><a href="#" class="social-expert"><i class="expert-icon fa fa-google"></i></a></li>
                            </ul>
                        </div>
                    </div>-->
                </div>
            </div>
        </section>
        <div class="about-tours padding-top padding-bottom">
            <div class="container">
                <div class="wrapper-tours">
                    <div class="content-icon-tours">
                        <div class="content-tours"><i class="icon flaticon-people"></i>
                            <div class="wrapper-thin"><span class="wrapper-icon-thin"><i class="icon-thin fa fa-circle-thin"></i></span>
                                <div class="tours-title">3750</div>
                            </div>
                            <div class="text">Alumnes Feliços</div>
                        </div>
                        <div class="content-tours"><i class="icon flaticon-suitcase"></i>
                            <div class="wrapper-thin"><span class="wrapper-icon-thin"><i class="icon-thin fa fa-circle-thin"></i></span>
                                <div class="tours-title">7740</div>
                            </div>
                            <div class="text">Destins</div>
                        </div>
                        <div class="content-tours"><i class="icon flaticon-two"></i>
                            <div class="wrapper-thin"><span class="wrapper-icon-thin"><i class="icon-thin fa fa-circle-thin"></i></span>
                                <div class="tours-title">850</div>
                            </div>
                            <div class="text">Hotels</div>
                        </div>
                        <div class="content-tours"><i class="icon flaticon-people-1"></i>
                            <div class="wrapper-thin"><span class="wrapper-icon-thin"><i class="icon-thin fa fa-circle-thin"></i></span>
                                <div class="tours-title">140</div>
                            </div>
                            <div class="text">Centres escolars</div>
                        </div>
                        <div class="content-tours"><i class="icon flaticon-drink"></i>
                            <div class="wrapper-thin"><span class="wrapper-icon-thin"><i class="icon-thin fa fa-circle-thin"></i></span>
                                <div class="tours-title">8960</div>
                            </div>
                            <div class="text">Viatges fets</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section class="wrapper-open-position padding-top padding-bottom">
            <div class="container">
                <div class="wrapper-position">
                    <h3 class="title-style-2">Prestacions de serveis <img src="<?= base_url() ?>img/INNOVAC.jpg" alt="" class="img-responsive" style="position: absolute; z-index: 10; width: 168px; left:390px; top: -24px"></h3>

                    <div class="content-position"
                        <div class="row">
                            <div class="col-md-6 col-sm-10 col-xs-10 main-right">
                                <div class="content-open">
                                    <div class="main-position">
                                        <div class="img-position">
                                            <a href="#" class="img-open"><img src="<?= base_url() ?>img/background/bg-team-open.jpg" alt="" class="img-responsive"></a>
                                        </div>
                                        <ul class="list-info list-unstyled">
                                            <li><a href="https://www.facebook.com/pg/byebyegroup/photos/?ref=page_internal" class="link"><i class="icon fa fa-facebook"></i></a></li>
                                            <li><a href="https://twitter.com/byebyegroup" class="link"><i class="icon fa fa-twitter"></i></a></li>
                                            <li><a href="http://stalkture.com/p/byebyegroup/3680527830/" class="link"><i class="icon fa fa-instagram"></i></a></li>
                                         <!--   <li><a class="link"><i class="icon fa fa-linkedin"></i></a></li>
                                            <li><a class="link"><i class="icon fa fa-behance"></i></a></li>-->
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="wrapper-text-excel">
                                        <div class="text-excel"> Observacions generals</div>
                                        <ul class="list-text list-unstyled">
                                            <li><a class="link-text"></i><span class="text-title">RÈGIMS</span></a>
                                                <p class="text">S.A. Només allotjament – A.D. Allotjament i esmorzar – M.P Mitja pensió – P.C Pensió completa – S.P Segons programa.</p>
                                            </li>
                                            <li><a class="link-text"></i><span class="text-title">PREU</span></a>
                                                <p class="text">El preu del viatge combinat ha estat calculat segons els tipus de canvi, tarifes de transport, costos del carburant i taxes d'impostos aplicables al moment de la consulta i confirmació de la reserva. Qualsevol variació del preu dels citats elements, podrà donar lloc a la revisió del preu final del viatge.</p>
                                            </li>
                                            <li><a class="link-text"></i><span class="text-title">FIANÇA</span></a>
                                                <p class="text">Els hotels podran exigir una fiança, 100% reemborsable, si les instal·lacions no han sofert desperfectes a la sortida del grup. L'agència no assumirà les despeses que es puguin ocasionar per aquests motius. L'import de la fiança ho designa cada establiment.</p>
                                            </li>

                                            <li><a class="link-text"></i><span class="text-title">PAGAMENT I RESERVA</span></a>
                                                <p class="text">En realitzar la reserva s'abonarà un 20% del pressupost final i la resta s'abonarà 30 dies abans de la sortida del grup.</p>
                                            </li>
                                            <li><a class="link-text"></i><span class="text-title">DOCUMENTACIÓ</span></a>
                                                <p class="text">La documentació que és necessària portar pels viatges d'estudiants és el DNI en vigor o PASSAPORT en vigor, i recomanable la Targeta Sanitària.</p>
                                            </li>
                                            <li><a class="link-text"></i><span class="text-title">INFORMACIÓ MÉDICA</span></a>
                                                <p class="text">Si l'estudiant que va a realitzar el viatge de fi de curs necessita cures de salut especials, hauran de ser notificades amb antel·lació a la nostra empresa per poder gestionar-les. Han de comunicar-ho a professors i/o pares acompanyants, responsables de l'allotjament triat i coordinador d'activitats de la destinació del viatgi fi de curs.</p>
                                            </li>
                                            <li><a class="link-text"></i><span class="text-title">AUTOCAR</span></a>
                                                <p class="text">L'autocar serà el nostre mitjà de transport durant el nostre trajecte; haurem, per tant, de mantenir-lo cuidat.</p>
                                            </li>


                                               <!-- <ul>
                                                    <li><span>Create assets for the Zendesk website</span></li>
                                                    <li><span>Concept ideas around the Zendesk brand</span></li>
                                                    <li><span>Design and e xecute Zendesk online advertising</span></li>
                                                    <li><span>Design presentations and printed materials</span></li>
                                                </ul>-->
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-10 col-xs-10">
                                <div class="group-list group-number">
                                    <ul class="list-unstyled about-us-list">
                                        <li><span class="" style="font-weight: bold">SERVEIS</span><span class="text-number"  style="font-weight: bold">LÍMITS</span></span></li>
                                        <li><span class="text">Assistència mèdica a Espanya</span><span class="text-number">601,01€</span></li>
                                        <li><span class="text">Repatriació o transport de ferits i/o malalts</span><span class="text-number">IL.LIMITAT</span></li>
                                        <li><span class="text">Repatriació d’acompanyant</span><span class="text-number">INCLÒS</span></li>
                                        <li><span class="text">Repatriació o transport de menors</span><span class="text-number">IL.LIMITAT</span></li>
                                        <li><span class="text">Desplaçament d’un familiar en cas d’hospitalizació superior <br>a cinc dies</span><span class="text-number">IL.LIMITAT</span></li>
                                        <li><span class="text">Despeses d’allotjament a l’estranger (amb un màxim de 10 dies)</span><span class="text-number">30,05€/dia</span></li>
                                        <li><span class="text">Convalecencia en hotel (amb un màxim de 10 dies)</span><span class="text-number">30,05€/dia</span></li>
                                        <li><span class="text">Repatriació o transport de l’assegurat mort</span><span class="text-number">IL.LIMITAT</span></li>
                                        <li><span class="text">Búsqueda, localització i enviament d’equipatge extraviat</span><span class="text-number">IL.LIMITAT</span></li>
                                        <li><span class="text">Robatori i danys materials de l’equipatge</span><span class="text-number">150,25€</span></li>
                                        <li><span class="text">Transmissió de missatges urgents</span><span class="text-number">IL.LIMITAT</span></li>
                                        <li><span class="text">Retorn anticipat per mort o hospitalització superior a <br> dos dies d’un familiar de fins a segon grau</span><span class="text-number">IL.LIMITAT</span></li>
                                        <li><span class="text">Assegurança d’Equipatges AXA</span><span class="text-number">150,25€</span></li>
                                        <li><span class="text">Assegurança de Responsabilitat Civil AXA</span><span class="text-number">60,101€</span></li>
                                    </ul>
                                </div>
                                <div class="wrapper-llc">
                                    <h3 class="title-style-2">On estem?</h3>
                                    <div class="text">Les activitats que es troben en aquest portal web estan organitzades per la nostra agència de viatges especialitzada en viatges d’ estudiants FINALIA VIAJES, S.L, Agencia de Viajes Minorista, Títol-Llicència Activitat otorgat per la Generalitat de Catalunya GC-002578, CIF: B65847170.</div>
                                    <ul class="list-llc list-unstyled">
                                        <li><i class="icon fa fa-map-marker"></i><a href="#" class="item">Girona, 34. 08700 IGUALADA (Barcelona) </a></li>
                                        <li><i class="icon fa fa-phone"></i>
                                            <a href="#" class="item">
                                                <p class="ph-number">902 002 068</p>

                                            </a>

                                        </li>
                                        <li><i class="icon fa fa-phone"></i>
                                            <a href="#" class="item">

                                                <p class="ph-number">93 803 06 94</p>
                                            </a>

                                        </li>
                                        <li><i class="icon fa fa-envelope-o"></i><a href="mailto:info@byebyegroup.com" class="item">info@byebyegroup.com</a></li>
                                        <li><i class="icon fa fa-envelope-o"></i><a href="mailto:reservas@byebyegroup.com" class="item">reservas@byebyegroup.com</a></li>
                                    </ul>
<!--                                </div><a href="#" class="view-more"><span class="more">View our company page</span></a></div>-->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="travelers"  style="background:url(<?= base_url() ?>img/background/bg-section-traveler_1.jpg); background-position: center bottom; background-repeat: repeat; background-size: cover;">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="traveler-wrapper padding-top padding-bottom">
                        <div class="group-title white">
                            <div class="sub-title">
                                <p class="text">INFORMACIÓ PER A</p><i class="icons flaticon-security"style=" color: #e7237e"></i></div>
                            <h2 class="main-title">AMPES I Professors</h2></div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="traveler-list">
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/pares-cover.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/pares.jpg" alt="" class="img-responsive"></div>
                                <p class="name" style="
font-weight: 900;">AMPES</p>
                                <p class="address">NECESSITEU PREPARAR UN VIATGE DE FINAL DE CURS SENSE MORIR EN L’INTENT?</p>
                                <p class="description">Aquest és un veritable repte al que us afronteu els pares cada curs.
                                    Nosaltres, que ja portem 15 anys d’experiència en organització i gestió de viatges, us oferim la nostra ajuda, suport i entusiasme per poder-ho aconseguir.
                                    No us preocupeu de res, ens reunim amb vosaltres, us aconsellem i us planifiquem tot el viatge, sempre ajustant-nos a les necessitats de cada curs.
                                    No te la juguis, deixa-ho en mans de BYE BYE GROUP</p>
                            </div>
                        </div>
                        <div class="traveler">
                            <div class="cover-image"><img src="<?= base_url() ?>img/pizarra.jpg" alt=""></div>
                            <div class="wrapper-content">
                                <div class="avatar"><img src="<?= base_url() ?>img/profe.jpg" alt="" class="img-responsive"></div>
                                <p class="name" style="
font-weight: 900;">PROFESSORS</p>
                                <p class="address">DESPRÈS DE TOT L’ANY TREBALLANT NO US VE DE GUST DESCONNECTAR?</p>
                                <p class="description">No hi penseu més i  gaudiu. Nosaltres ens n’encarreguem! Us organitzem un viatge arreu, tant amb companys, per gaudir de bons moments i experiències úniques fora de la feina, com en família.

                                    A BYE BYE GROUP tenim un gran equip professional, que us ajudarà en el procés i us proporcionarà tots els recursos necessaris, com per exemple transport, allotjament, visites…adaptats a les vostres demandes.
                                    Si us cal viatjar per altres motius, com seminaris, ponències, reunions, congregacions…també us podem ajudar.
                                    ENGRESQUEU-VOS!! </p>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
    </section>
        <?php $this->load->view('_contacto'); ?>
    </div>
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top" class="link"><i class="fa fa-angle-double-up"></i></a></div>
</div>
<!--- About page ---->
<script src="<?= base_url() ?>js/template/pages/about-us.js"></script>
<script src="<?= base_url() ?>js/template/libs/nst-slider/js/jquery.nstSlider.min.js"></script>
<script src="<?= base_url() ?>js/template/libs/plus-minus-input/plus-minus-input.js"></script>
