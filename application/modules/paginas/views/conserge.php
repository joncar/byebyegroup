<?php 
	redirect('http://byebyegroup.com/catalunya/conserge');
	die();
?>
<script>
	var URL = 'http://byebyegroup.com/catalunya/';
	var URI = 'http://byebyegroup.com/catalunya/';
</script>
<script src="http://byebyegroup.com/js/frame.js"></script>
<style>
	.page-banner{
		margin-bottom: -368px !important;
	}
</style>
<div id="wrapper-content">
    <!-- MAIN CONTENT-->
    <div class="main-content">
        <section class="page-banner about-us-page">
            <div class="patterndestino"></div>
            <div class="container">
                <div class="page-title-wrapper">
                    <div class="page-title-content">
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>" class="link home">Home</a></li>                            
                        </ol>
                        <div class="clearfix"></div>
                        <h2 class="captions">Conserge</h2></div>
                </div>
            </div>
        </section>        
        
        <section class="contact style-1" id='contactenosform' style=" margin-top: 120px;">
		    <div class="container">
		        <div class="row">
		            <div class="wrapper-contact-style">
		                <div data-wow-delay="0.5s" class="contact-wrapper-images wow fadeInLeft">
		                    <img src="<?= base_url() ?>img/homepage/contact-people.png" alt="" class="img-responsive">
		                </div>
		                <div class="col-lg-6 col-sm-7 col-lg-offset-4 col-sm-offset-5">
		                    <div data-wow-delay="0.4s" class="contact-wrapper padding-top padding-bottom wow fadeInRight">
		                        <div class="contact-box">		                            
		                            <h5 class="title">FORMULARI DE CONTACTE</h5>
		                            <p class="text" style="color: #222;">Si no has trobat el viatge que t’agradaria fer, explica’ns-ho!!</p>
		                            <form class="contact-form" method="post" action="" onsubmit="insertar('destinos/frontend/conserge/insert',this,'.response'); return false;">
		                            	<div class="response"></div>
		                                <input name="conserge" type="text" placeholder="Nom del conserge" class="form-control form-input">
		                                <input name='email' type="email" placeholder="Mail de contacte" class="form-control form-input">
		                                <input name="centro" type="text" placeholder="Nom del centre" class="form-control form-input">
		                                <input name="localidad" type="text" placeholder="Localitat" class="form-control form-input">
		                                <input name="coordinador" type="text" placeholder="Nom del coordinador del viatge" class="form-control form-input">
		                                <input name="destino" type="text" placeholder="Destí del viatge" class="form-control form-input">
		                                <input name="telefono" type="text" placeholder="Telèfon" class="form-control form-input">		                                
		                                <div class="contact-submit">		                                    
		                                    <button type="submit" data-hover="SEND NOW" class="btn btn-slide">
		                                        <span class="text">enviar ara</span>
		                                        <span class="icons fa fa-long-arrow-right"></span>
		                                    </button>
		                                </div>
		                            </form>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>

    </div>
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top" class="link"><i class="fa fa-angle-double-up"></i></a></div>
</div>
<!--- About page ---->
<script src="<?= base_url() ?>js/template/pages/about-us.js"></script>
<script src="<?= base_url() ?>js/template/libs/nst-slider/js/jquery.nstSlider.min.js"></script>
<script src="<?= base_url() ?>js/template/libs/plus-minus-input/plus-minus-input.js"></script>
