<section class="contact style-1" id='contactenosform' style=" margin-top: 120px;">
    <div class="container">
        <div class="row">
            <div class="wrapper-contact-style">
                <div data-wow-delay="0.5s" class="contact-wrapper-images wow fadeInLeft">
                    <img src="<?= base_url() ?>img/homepage/contact-people.png" alt="" class="img-responsive">
                </div>
                <div class="col-lg-6 col-sm-7 col-lg-offset-4 col-sm-offset-5">
                    <div data-wow-delay="0.4s" class="contact-wrapper padding-top padding-bottom wow fadeInRight">
                        <div class="contact-box">
                            <?= @$_SESSION['msj'] ?>
                            <?php $_SESSION['msj'] = ''; ?>
                            <h5 class="title">FORMULARI DE CONTACTE</h5>
                            <p class="text" style="color: #222;">Si vols contactar amb nosaltres, no dubtis en fer-ho.</p>
                            <form class="contact-form" method="post" action="<?= base_url('paginas/frontend/contacto') ?>?redirect=<?= base_url() ?>#contactenosform">
                                <input name="name" type="text" placeholder="El teu nom" class="form-control form-input">
                                <input name='email' type="email" placeholder="El teu Email" class="form-control form-input">
                                <textarea name="message" placeholder="Missatge" class="form-control form-input"></textarea>
                                <div class="contact-submit">
                                    <button type="submit" data-hover="SEND NOW" class="btn btn-slide">
                                        <span class="text">enviar ara</span>
                                        <span class="icons fa fa-long-arrow-right"></span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
